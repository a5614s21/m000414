$(document).ready(function() {

	$(".check_pay_ment").on('click', function() {
		$(this).parent().parent().parent().toggleClass("pay_on_check");
	});
	$(".over_pay_ment").on('click', function() {
		$(this).parent().parent().parent().parent().toggleClass("pay_on_check");
	});

	if ($("#index").length==1) {
	
		$("header").addClass("hasPrivacy");
	}


  if (!/Android/i.test(window.navigator.userAgent)) {	
		$(".scrollbarX").each(function() {
	    	$(this).mCustomScrollbar({
				axis: "x",
				mouseWheel:{ enable: false }
			});
		});
	  if ($('.ios').length==0 || $(window).outerWidth()>575) {	
		  $('.scrollbarY').mCustomScrollbar({
			  axis: "y"
		  });		  
	  }

	}

	$('.jqimgFill').imgLiquid();
	$('.jqimgFill, .main').css("opacity","1");
	$('.selectpicker').select2();
	$(".sortTable").tablesorter({
		theme: "blue",
		widgets: ['zebra'],
		headers: {
			// 2: {
			// 	sorter: false
			// },
			// 3: {
			// 	sorter: false
			// },
			// 4: {
			// 	sorter: false
			// },
			// 5: {
			// 	sorter: false
			// },
			6: {
				sorter: false
			},		
		}
	});
	$(".datepicker").datepicker({
		"dateFormat": "yy / mm / dd"
	});

  	resizeCss();
	//因架構變化，resize trigger reload
	var wW = $(window).width();
	var trigger_size = [576,768,992,1200,1400];

	$(window).resize(function () {
		trigger_size.forEach(function (ele) {
			if (wW > ele) {
				$(window).width() <= ele ? location.reload() : "";
			} else {
				$(window).width() > ele ? location.reload() : "";
			}
		});
	});
	/* ==========================================================================
		[header]
 	==========================================================================*/
	$('#header').each(function() {
		$(this).css("visibility","visible");
		$(".menu-toggle").click(function() {
			$(this).toggleClass('active');
			$(this).parents("#header").find(".bg_phone").toggleClass('active');
			$(".gray_block").toggleClass('active');

			$(".menu .menutitle").toggleClass("slideIn");
			
			$(".menu>.menutitle.hasChild>ul").slideUp();
			$("html").toggleClass("noscroll");
		});
		$(".menu-toggle").keypress(function (e) {
			if(e.keyCode == 13){
				$(this).toggleClass('active');
				$(this).parents("#header").find(".bg_phone").toggleClass('active');
				$(".gray_block").toggleClass('active');

				$(".menu .menutitle").toggleClass("slideIn");

				$(".menu>.menutitle.hasChild>ul").slideUp();
				$("html").toggleClass("noscroll");
			}
		});
		$(".gray_block").click(function () {
			$(".menu-toggle").removeClass('active');
			$(this).parent().find(".bg_phone").toggleClass('active');
			$(".gray_block").removeClass('active');
			$(".menu .menutitle").toggleClass("slideIn");
			$("html").removeClass("noscroll");
		});
		if($(window).outerWidth()<992){
			$(".profile>.menutitle>a").click(function () {
				$(".menu-toggle").removeClass('active');
				$(".menu-toggle").parent().find(".bg_phone").removeClass('active');
				$(".gray_block").removeClass('active');

				$(".menu .menutitle").toggleClass("slideIn");
				$("html").removeClass("noscroll");
			});
		}

		$("#header .search-toggle").click(function () {
			$("#header .search").toggleClass("actived");
			$("#header .search  .search-toggle .icon-cancel").toggleClass("hidden");
			$("#header .search  .search-toggle .icon-search").toggleClass("hidden");
		});
		if($(window).width()>991){
			$("#header .lang").click(function () {
				$(this).children("ul").slideToggle();
			});
		}
		if ($(window).width()>1400) {

			$(window).scroll(function () {
				if ($(window).scrollTop() > 210) {
				// if ($(window).scrollTop() > 65) {
					$("#header").addClass("scroll");
				}
				else {
					$("#header").removeClass("scroll");
				}
			});
		}
		
	});
	$("#top").on("click", function () {
		$('html, body').animate({ scrollTop: 0 }, 800);

	});


	/* ==========================================================================
			IE 9 不支援 placeholder
	==========================================================================*/
	(function ($) {
   	$.support.placeholder = ('placeholder' in document.createElement('input'));
	})(jQuery);

	//fix for IE7 and IE8
	$(function () {
    if (!$.support.placeholder) {
	    $("[placeholder]").focus(function () {
	      if ($(this).val() == $(this).attr("placeholder")) $(this).val("");
	     }).blur(function () {
	  	  if ($(this).val() == "") $(this).val($(this).attr("placeholder"));
		}).blur();

		$("[placeholder]").parents("form").submit(function () {
	    $(this).find('[placeholder]').each(function() {
	      if ($(this).val() == $(this).attr("placeholder")) {
	        $(this).val("");
	        }
	     });
			});
	  }
	});

 
	$(window).resize(function() {
		resizeCss();
	});
	function resizeCss() {
		handleMenu();
	}
	

	handle_top();
	privacy();
	footer_slider();
	footer_collapse();
	textillate();
	$("#index").each(function(){
		index_banner_slick();
		insuranceChangeBg();
		$(".slide-navBtn").keypress(function(e){
			if(e.keyCode == 13){
				$(this).parent(".position-relative").parent("div").parent(".slick-slide").trigger("click");
			}
		});
	});
	$("#insurance").each(function () {
		kindOfInsuranceEvent();
		$(".bookmark-box li").each(function(){
			$data = $(this).attr("data-bookmark");
			if($data == 1){
				$(this).attr("aria-expanded","true")
			}
		})
		bookMark();
		accordion();
	});	
	$("#insurance-company").each(function () {
		kindOfInsuranceEvent();
		$(".bookmark-box li").each(function () {
			$data = $(this).attr("data-bookmark");
			if ($data == 1) {
				$(this).attr("aria-expanded", "true")
			}
		})
		bookMark();
		accordion();
	});	
	$("#insurance-FAQ").each(function(){
		accordion();
	});
	$("#claims-FAQ").each(function(){
		accordion();
	});
	$("#payment").each(function(){
		accordion();
		paymentSearch();
		tableMobileToggle();
	})
	$(".kind-of-about").each(function () {
		kindOfAbout();
	});	
	$("#claims,#payment").each(function () {
		bookMark();
		bind_menuToggle();
		changePosition();
	});
	$("#register").each(function () {
		$(".bookmark li").on("click", function () {
			$data = $(this).attr("data-bookmark");
			$(this).addClass("active").siblings().removeClass("active");
			$target = $(".data .content").filter(function () {
				$data_compar = $(this).attr("data-target");

				return $data == $data_compar;
			});
			$target.addClass("active").siblings().removeClass("active");
		});
	});	

	$(".links").each(function () {
		bind_menuToggle();
	});
	$("#claims.car").each(function(){	
		var fileInput = document.getElementById('file');
		if (fileInput){			
			var fileList = fileInput.files; 	
			$("body").on("click", ".trash-svg", function () {
				var ind = $(this).parent().index();
				$(this).parent().remove();
				fileList.splice(ind, 1);
			});

			$("#file").change(function () {
				$(".file-group").next().next(".files").html("");
				fileList = fileInput.files; 
				fileList = Array.prototype.slice.call(fileList); 
				if (fileList.length !== 0) {
					for (var i = 0, len = fileList.length; i < len; i++) {
						var str = "<li data-filename='" + fileList[i].name+"'>"+
									"<img class='svg file-svg' src='styles/images/common/file.svg' alt=''>"+
										"<span>" + fileList[i].name+"</span>"+
									"<img class='svg trash-svg' src='styles/images/common/trash.svg' alt=''>"+
								"</li>";
						$(".file-group").next().next(".files").append(str);
					};
				}
			});
		}

	});
	$("#inquire,#winning,#payment-FAQ").each(function () {
		$(".toggle").click(function () {
			if ($(this).hasClass("open")){
				$(this).attr("aria-expanded","false");
			}
			else{
				$(this).attr("aria-expanded","true");
        $('.link-section-control-m .toggle').removeClass("open").next("ul").slideUp();
			}
        $(this).toggleClass("open").next("ul").slideToggle();
			
			var notCurrent = $("#winning .main-section .toggle").not($(this));
			if (notCurrent.hasClass("open")) {
				notCurrent.removeClass("open");
				notCurrent.next("ul").slideUp();
				notCurrent.attr("aria-expanded","false");
			}
		});
		$(".toggle").keypress(function(e){
			if(e.keyCode == 13){
				$(this).trigger("click");
			}
		});
		$(document).keydown(function(e){
			if(e.keyCode == 27){
				if($windowWidth >= 577){
					$(".main-section .toggle").removeClass("open");
					$(".main-section .toggle").next("ul").slideUp();
				}
				else{
					$(".toggle").removeClass("open");
					$(".toggle").next("ul").slideUp();
				}
			}
		});
		changePosition();
	});
	$("#video").each(function(){
		uniformHeight($("ul.videos li a .img-wrapper"));
	});


/* ==========================================================================
		像wow特效
==========================================================================*/

if($(".msie").length==0){
	
	$(".lol").each(function () {
		
		if ($(window).height() + $(window).scrollTop() > $(this).offset().top) {
			$delay = $(this).attr("data-lol-delay") * 1000;
			$(this).delay($delay).queue(function () {
				$(this).css("opacity", 1).addClass($(this).attr("data-lol"));
			});
		}
	});
	$(window).scroll(function () {
		$(".lol").each(function () {
			if ($(window).scrollTop() > $(this).offset().top - $(window).height()) {
				if ($("html").hasClass("msie")) {
					if ($(this).prop("tagName") !== "TR") {
						$delay = $(this).attr("data-lol-delay") * 1000;
						$(this).delay($delay).queue(function () {
							$(this).css("opacity", "1").addClass($(this).attr("data-lol"));
						});
					}
					else {
						$(this).removeClass("lol");
					}
				}
				else {
					$delay = $(this).attr("data-lol-delay") * 1000;
					$(this).delay($delay).queue(function () {
						$(this).css("opacity", "1").addClass($(this).attr("data-lol"));
					});
				}


			}
		});
	});
}
	$("#register .read-list .field-group label").on("click", function (e) {
		if (!$(this).hasClass("checked")) {
			e.preventDefault();
			$(this).addClass("checked");
		}
		else {
			$(this).removeAttr("data-toggle");
		}
	})

	$("#insurance .kind-of-insurance").children("ul").children("li").each(function(){
		var ul_num=$(this).children("div").children(".additional-insurance").children("ul").length;
		if(ul_num==0){
			$(this).children("div").children(".additional-insurance").children(".toggle").attr("style","pointer-events:none;");
			$(this).children("div").children(".additional-insurance").children(".toggle").children(".icon-down-open").attr("style", "opacity : 0;");
		}
	})
	$("#insurance-company .kind-of-insurance").children("ul").children("li").each(function () {
		var ul_num2 = $(this).children("div").children(".additional-insurance").children("ul").length;
		if (ul_num2 == 0) {
			$(this).children("div").children(".additional-insurance").children(".toggle").attr("style", "pointer-events:none;");
			$(this).children("div").children(".additional-insurance").children(".toggle").children(".icon-down-open").attr("style", "opacity : 0;");
		}
	}) 

	// accessibility
	// $("header .acc-key-top").on("keydown",function(){
	// 	$('html, body').animate({
	// 		scrollTop: 0
	// 	}, 0);
	// })
	$("#footer .arrow").keypress(function(e){
		if (e.keyCode == 13) {
			$("#footer .top").toggleClass("active");
			$("#footer .sitemap").slideToggle();
			if ($("#footer .top").hasClass("active")){
				$("#footer .arrow").attr("title", "收合網站導覽")
				$("#footer .arrow").attr("aria-expanded", "true")
			}
			else{
				$("#footer .arrow").attr("title", "開啟網站導覽")
				$("#footer .arrow").attr("aria-expanded", "false");
			}
		}   
	});
	$(document).keydown(function (e) {
		if (e.keyCode == 27) {
			// esc close mobile navitems
			$(".menu-toggle").removeClass('active');
			$('#header').find(".bg_phone").removeClass('active');
			$(".gray_block").removeClass('active');
			$(".menu .menutitle").removeClass("slideIn");
			$("html").removeClass("noscroll");
			// esc close footer sitemap
			$("#footer .top").removeClass("active");
			$("#footer .sitemap").slideUp();
			$("#footer .arrow").attr("title","開啟網站導覽")
		}
	});
	$("#header .privacy-close").keypress(function () {
		if (e.keyCode == 13) {
			$("#header").removeClass("hasPrivacy");
			$(".inner").removeAttr("style");
			if ($windowWidth <= 991) {
				$windowH = $(window).height();
				$headerH = $("#header").height();
				$("#header .header-wrapper .bottom .bg_phone").height($windowH - $headerH);
			}
		}
	});
	$("#insurance-company .additional-insurance , #insurance .additional-insurance").each(function(){
		if($(this).children("ul").length == 0){
			$(this).removeAttr("tabindex");
			$(this).removeAttr("title");
		}
	});
	$("#insurance-company .detial-block .bookmark-box, #insurance .detial-block .bookmark-box").each(function(){
		if($(this).children(".content_title").length == 1){
			$(this).children(".content_title").removeAttr("tabindex").removeAttr("role").removeAttr("aria-expanded");
			$(this).children(".content_title").children("div").css("cursor","auto");
		}
	})
	$("#login").each(function () {
		$("#login .main-section .block .tabs ul li button").on("click", function () {
			$data = $(this).attr("data-tab");
			$(this).parent("li").addClass("active").siblings().removeClass("active");
			$target = $("#login .main-section .block form").filter(function () {
				$data_compar = $(this).attr("data-target");
				return $data == $data_compar;
			});
			$target.addClass("show").siblings().removeClass("show");
		})
	});
	$("#profile").each(function(){
		$(".field-group.will .radios label").keypress(function(e){
			if (e.keyCode == 13) {
				$(this).trigger("click");
			}
		});
	});
	$("#register").each(function () {
		$(".agreeCheck , .tabBtn").keypress(function (e) {
			if (e.keyCode == 13) {
				$(this).trigger("click");
			}
		});
	});
	$(".keyEnter").on("keypress",function(e){
		if (e.keyCode == 13) {
			$(this).trigger("click");
		}
	});
	$(".chooseFile").focus(function(){
		$(this).parent(".file-group").addClass("focus");
	});
	$(".chooseFile").blur(function () {
		$(this).parent(".file-group").removeClass("focus");
	});
	$("#claims").each(function () {
		$("#claims .main-section .block .tabs ul li button").on("click", function () {
			$data = $(this).attr("data-tab");
			$(this).parent("li").addClass("active").siblings().removeClass("active");
			$target = $("#claims .main-section .block form").filter(function () {
				$data_compar = $(this).attr("data-target");
				return $data == $data_compar;
			});
			$target.addClass("show").siblings().removeClass("show");
		})
	});
	$("#travelSystem").each(function(e){
		$(".travel-verify form #code1,.travel-verify form #code2,.travel-verify form #code3,.travel-verify form #code4,.travel-verify form #code5,.travel-verify form #code6").keyup(function(){
			if ($(this).val().length >= $(this).attr("maxlength")) {
				$(this).parent().next(".input").find("input").focus();
			}
		});
	});
	$("#inquire").each(function(e){
		$(".inquire_proof #numType").on("change",function(){
			var placeholder = $("#select2-numType-container").attr("title");
			if (placeholder == "牌照號碼") {
				placeholder = "例:ABC-123";
			} else if (placeholder == "車身/引擎號碼") {
				placeholder = "　";
			} 
			else{
				placeholder = "14XXXXXXXXXXXX";
			}
			$("#num").attr("placeholder",placeholder)
		});
	})
	svg();
	/*document END*/
});






