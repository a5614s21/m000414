var $windowWidth = $(window).outerWidth();


function handleMenu() {
    if ($(window).outerWidth() > 1199) {
        $("#header .menu>.menutitle").off('mouseenter').off('mouseleave').off("click");
        $("#header .menu>.menutitle").hover(function () { $(this).children(".submenu").stop().slideDown(); }, function () { $(this).children(".submenu").stop().slideUp(); });
        $("#header .menu>.menutitle").on("focusin",function () { 
            $(this).children(".submenu").stop().slideDown();
        })
        $("#header .menu>.menutitle").on("focusout",function(){
            $(this).children(".submenu").stop().slideUp();
        });
    }
    else {
        $("#header .menu>.menutitle").off('mouseenter').off('mouseleave');
        // $("#header .menu>.menutitle>span").unbind("click");
        // $("#header .menu>.menutitle>span").click(function () {
        $("#header .menu>.menutitle").off("click");
        $("#header .menu>.menutitle").click(function () {
            $(this).siblings().removeClass("open").children(".submenu").slideUp();
            $(this).toggleClass("open").find(".submenu").stop().slideToggle();
        });
        $("#header .menu>.menutitle").on("focusin", function () {
            $(this).children(".submenu").stop().slideDown();
        })
        $("#header .menu>.menutitle").on("focusout", function () {
            $(this).children(".submenu").stop().slideUp();
        });
    }
    if ($windowWidth <= 991) {

        $x = $("#header .submenu").detach();
        $("#header .submenu-group").append($x);
        $("#header .submenu-group .submenu").eq(0).addClass("show");
        
        $windowH=$(window).outerHeight();
        $headerH=$("#header").height();
        $("#header .header-wrapper .bottom .bg_phone").height($windowH - $headerH);

        $('#header .menu .home').remove();

        $('#header .menu .menutitle').removeClass("active");
        // $("#header .toggle").click(function(){
        //     $("#header .menu").stop().slideToggle();
        // });
        $("#header .menu .menutitle").click(function(){
            $index=$(this).index();
            $(".submenu-group .submenu").removeClass("show").eq($index).addClass("show");
        });

    }

    if ($("#header").hasClass("hasPrivacy")){
        $h=$("#header").height();
        $(".main>.inner").css("padding-top",$h);
    }

}

function handle_top() {
    if ($(window).scrollTop() >= 100) {
        $("#goTop").addClass("show");
    }
    $(window).scroll(function (event) {
        if ($(window).scrollTop() >= 100) {
            $("#goTop").addClass("show");
        }
        else {
            $("#goTop").removeClass("show");
        }
    });
    $("#goTop").on("click", function () {
        $('html, body').animate({ scrollTop: 0 }, 800);

    });
}

function uniformHeight($el) {
    if ($el.length !== 0) {
        var $h_array = [];
        $el.each(function (index) {
            $(this).height("auto");
            $h_array[index] = $(this).height();
        });
        $max = Math.max.apply(null, $h_array);
        $el.each(function (index) {
            $(this).height($max);
        });
    }
}


function privacy() {
    $("#header .icon-close").click(function () {
        $("#header").removeClass("hasPrivacy");
        $(".inner").removeAttr("style");
        if ($windowWidth <= 991) {
            $windowH = $(window).height();
            $headerH = $("#header").height();
            $("#header .header-wrapper .bottom .bg_phone").height($windowH - $headerH);
        }
    });

}
function footer_slider() {
    $('#footer .center .slider').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
        fade: false,
        autoplay: false,
        speed: 500,
        prevArrow: "<div class='slick-arrow slick-prev'><i class=icon-left-open></i></div>",
        nextArrow: "<div class='slick-arrow slick-next'><i class=icon-right-open></i></div>",
        responsive: [
            {
                breakpoint: 1400,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });

}

function footer_collapse() {
    $("#footer .arrow").click(function () {
        $("#footer .top").toggleClass("active");
        $("#footer .sitemap").slideToggle();
    });
}
function index_banner_slick() {
    if($windowWidth>575){
        console.log($windowWidth);
        $('#index .banner-section .for-PC .slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: false,
            asNavFor: '.for-PC .slider-nav',
            autoplay: true,
        });
        $('#index .banner-section .for-PC .slider-nav').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.for-PC .slider-for',
            dots: false,
            focusOnSelect: true,
            autoplay: true,
            autoplaySpeed: 3000,
            prevArrow: "<div class='slick-arrow slick-prev'><i class=icon-left-open></i></div>",
            nextArrow: "<div class='slick-arrow slick-next'><i class=icon-right-open></i></div>",
            responsive: [
                {
                    breakpoint: 1600,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 2
                    }
                }
            ]

        });
    }
    else{
        $('#index .banner-section .for-phone .slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: false,
            asNavFor: '.for-phone .slider-nav',
            autoplay: true,
        });
        $('#index .banner-section .for-phone .slider-nav').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.for-phone .slider-for',
            dots: false,
            focusOnSelect: true,
            autoplay: true,
            autoplaySpeed: 3000,
            prevArrow: "<div class='slick-arrow slick-prev'><i class=icon-left-open></i></div>",
            nextArrow: "<div class='slick-arrow slick-next'><i class=icon-right-open></i></div>",
        });    
    }
}
function insuranceChangeBg(){
    $(".box-1 ul li a").hover(function(){
        $index=$(this).attr("data-index");

        $target = $(".img-block div").filter(function () {
            $data_compar = $(this).attr("data-target");

            return $index == $data_compar;
        });
        $target.addClass("active").siblings().removeClass("active");
    });
}
function kindOfInsuranceEvent(){
    if($windowWidth>=1199){
        $(".additional-insurance").hover(
            function () {
                $(this).parents("li").siblings("li").find(".toggle").next("ul").slideUp();
                $(this).children("ul").stop().slideDown();
            },
            function () {
                $(this).children("ul").stop().slideUp();
            }
        );
    }
    else if ($windowWidth >991){
        $(".additional-insurance").click(function(){
            $(this).parents("li").siblings("li").find(".toggle").next("ul").slideUp();
            $(this).children("ul").slideToggle();
        });
    }
    else{
        $(".toggle-for-phone").click(function () {
            $(this).toggleClass("active");
            $(".kind-of-insurance").children("ul").slideToggle();
        });
    }

}
function kindOfAbout() {
    if ($windowWidth < 1199) {
        $(".toggle-for-phone").click(function () {
            $(this).toggleClass("active");
            $(".kind-of-about").children("ul").slideToggle();
        });
    }
}
function bookMark() {
    $(".bookmark-box  li").on("click", function () {
        $data = $(this).attr("data-bookmark");
        $(this).addClass("active").siblings().removeClass("active");
        $target = $(".data-box .content").filter(function () {
            $data_compar = $(this).attr("data-target");

            return $data == $data_compar;
        });
        $target.addClass("active").siblings().removeClass("active");
    });
}

function accordion(){
    $('.accordion .card').on('show.bs.collapse', function () {
        
        $(this).find(".icon-down-open").addClass("open");
    })
    $('.accordion .card').on('hide.bs.collapse', function () {
        $(this).find(".icon-down-open").removeClass("open");
    })
}

function textillate(){
    if($windowWidth>575){   
        $('.tlt').textillate({
            loop: false,
            initialDelay: 200,
            in: {
                effect: 'fadeInDown',
                delay: 80,

            },
        }); 
        $('.tlt2').textillate({
            loop: false,
            initialDelay: 300,
            in: {
                effect: 'fadeInDown',
                delay: 20,

            },
        }); 
    }
}

function bind_menuToggle(){
    $(".toggle").click(function(){
        if($(this).hasClass("open")){
            $(this).removeClass("open").next("ul").slideUp();
        }else{
            $(".toggle.open").removeClass("open").next("ul").slideUp();
            $(this).addClass("open").next("ul").slideDown();
        }
    });
    if($windowWidth<576){
        $(".toggle:not(.noMobile)").next("ul").children("li").click(function () {
            $(this).parent("ul").slideUp().prev(".toggle").removeClass("open");
        });
    }

}
function changePosition(){
    if($windowWidth<576){
        $el = $(".breadCrumbs").detach();
        $(".banner-section").after($el);
        $el = $(".bookmark-box:not(.noMobile)").detach();
        $(".link-section").after($el);
    }
}

// function svg() {
//     jQuery('img.svg').each(function () {
//         var $img = jQuery(this);
//         var imgID = $img.attr('id');
//         var imgClass = $img.attr('class');
//         var imgURL = $img.attr('src');

//         jQuery.get(imgURL, function (data) {
//             // Get the SVG tag, ignore the rest   
//             var $svg = jQuery(data).find('svg');

//             // Add replaced image's ID to the new SVG   
//             if (typeof imgID !== 'undefined') {
//                 $svg = $svg.prop('id', imgID);
//             }
//             // Add replaced image's classes to the new SVG   
//             if (typeof imgClass !== 'undefined') {
//                 $svg = $svg.addClass(imgClass).addClass('replaced-svg');
//             }

//             // Remove any invalid XML tags as per http://validator.w3.org   
//             $svg = $svg.removeAttr('xmlns:a');

//             // Check if the viewport is set, if the viewport is not set the SVG wont't scale.   
//             if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
//                 $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
//             }

//             // Replace image with new SVG   
//             $img.replaceWith($svg);

//         }, 'xml');

//     });
// }
// 為避開XSS風險，改以下JS
document.querySelectorAll('img.svg').forEach((el) => {
    const imgID = el.getAttribute('id');
    const imgClass = el.getAttribute('class');
    const imgURL = el.getAttribute('src');

    fetch(imgURL)
        .then(data => data.text())
        .then(response => {
              const parser = new DOMParser();
              const xmlDoc = parser.parseFromString(response, 'text/html');
              let svg = xmlDoc.querySelector('svg');

              if (typeof imgID !== 'undefined') {
                  svg.setAttribute('id', imgID);
              }

              if(typeof imgClass !== 'undefined') {
                  svg.setAttribute('class', imgClass + ' replaced-svg');
              }

              svg.removeAttribute('xmlns:a');

              el.parentNode.replaceChild(svg, el);
        })
});


function paymentSearch() {
    $('.radioWrap label').on('click', function(event) {
        let labelType = $(this).data('target')
        $('.radios').find('.input').each(function(index, el) {
            let inputType = $(this).data('anchor')
            let self = $(this)
            if (labelType == inputType) {
                self.show();
            }else{
                self.hide();
            }
        });
    });
}
function tableMobileToggle() {
    $('.tableBox .tableMobileBtn').on('click', function(event) {
        $(this).closest('td').siblings().toggleClass("d-flex");
    });
}