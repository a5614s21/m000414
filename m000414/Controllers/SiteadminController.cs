﻿using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Dynamic;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using Web.Models;
using Web.Models.EntityModel;
using Web.Models.ViewModels;
using Web.Repository;
using Web.Service;
using System.Net;
using System.Runtime.Remoting.Channels;
using Web.ServiceModels;
using Google.Apis.AnalyticsReporting.v4.Data;
using WebApp.Models;
using ClosedXML.Excel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Web.Controllers
{
    [SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]
    public class SiteadminController : Controller
    {
        public string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;

        //預設語系
        public string adminCathType = ConfigurationManager.ConnectionStrings["adminCathType"].ConnectionString;

        //後台登入記錄方式 Session or Cookie
        public string projeftNo = ConfigurationManager.ConnectionStrings["projeftNo"].ConnectionString;

        public int cacheTime = 15;

        //案件編號
        protected System.Web.Caching.Cache cacheContainer = HttpRuntime.Cache;

        private Model DB = new Model();

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult login()
        {
            ViewBag.username = "";
            if (Request.Cookies["keepMeAccount"] != null)
            {
                HttpCookie aCookie = Request.Cookies["keepMeAccount"];

                string keepAdminID = Server.HtmlEncode(aCookie.Value.Replace("keepMeAccount=", ""));

                if (keepAdminID != null && keepAdminID != "")
                {
                    ViewBag.account = keepAdminID;
                }
            }

            //bool isLogin = FunctionService.systemUserCheck();
            //if (isLogin)
            //{
            //    HttpCookie cookie = System.Web.HttpContext.Current.Request.Cookies["sysLogin"];
            //    string[] tempAccount = cookie.Value.Split('&');
            //    var userguid = tempAccount[1].Replace("sysUserGuid=", "").ToString();

            //    //return RedirectPermanent(Url.Content("~/siteadmin"));
            //    return RedirectPermanent(Url.Content("~/siteadmin/edituser/user/edit/" + userguid));
            //}

            return View();
        }
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        /// <summary>
        /// 首頁儀錶板
        /// </summary>
        /// <returns></returns>
        public ActionResult IndexDash()
        {
            return View();
        }


        public string analytics()
        {
            var GaData = new GoogleAnalyticsService();

            GaParameterModel GaParameter = new GaParameterModel();//宣告參數使用

            string sDate = DateTime.Now.ToString("yyyy") + "-01-01";
            string eDate = DateTime.Now.ToString("yyyy") + "-12-31";

            if (Session["sDate"] != null && Session["eDate"] != null)
            {
                sDate = Session["sDate"].ToString();
                eDate = Session["eDate"].ToString();
            }

            GaParameter.startDate = sDate;//預設起始日
            GaParameter.endDate = eDate;//預設結束日

            List<GaMonthModel> jsonY = new List<GaMonthModel>();
            string json = "";
            if (RouteData.Values["key"] != null)
            {
                switch (RouteData.Values["key"].ToString())
                {
                    case "year":
                        jsonY = GaData.ViewYear();
                        break;
                    //年齡層
                    case "age":

                        try
                        {
                            string CacheName = "age" + sDate + eDate;
                            dynamic tmp = null;
                            GaParameter.metrics = "ga:pageviews";
                            GaParameter.Dimensions = "ga:visitorAgeBracket";
                            tmp = GaData.ReGaContent(GaParameter);

                            if (cacheContainer.Get(CacheName) == null)
                            {
                                tmp = GaData.ReGaContent(GaParameter);
                                cacheContainer.Insert(CacheName, tmp, null, DateTime.Now.AddMinutes(cacheTime), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            }
                            else
                            {
                                tmp = cacheContainer.Get(CacheName);//自cache取出
                            }

                            //List<AgeViewModel> age = tmp.Rows;
                            List<AgeViewModel> AgeViewModel = new List<AgeViewModel>();
                            int s = 0;
                            foreach (List<string> item in tmp.Rows)
                            {
                                AgeViewModel m = new AgeViewModel();
                                m.age = item[0];//網頁
                                m.visits = int.Parse(item[1].ToString());
                                AgeViewModel.Add(m);
                                s++;
                            }

                            json = JsonConvert.SerializeObject(AgeViewModel);
                        }
                        catch { }

                        break;

                    ////流量來源
                    case "medium":

                        #region 流量來源

                        try
                        {
                            GaParameter.metrics = "ga:organicSearches";
                            GaParameter.Dimensions = "ga:medium";

                            string CacheName = "GaMedium";
                            dynamic tmp = null;
                            if (cacheContainer.Get(CacheName) == null)
                            {
                                tmp = GaData.ReGaContent(GaParameter);
                                cacheContainer.Insert(CacheName, tmp, null, DateTime.Now.AddMinutes(cacheTime), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            }
                            else
                            {
                                tmp = cacheContainer.Get(CacheName);//自cache取出
                            }

                            List<MediumViewModel> MediumViewModel = new List<MediumViewModel>();
                            int s = 0;
                            foreach (List<string> item in tmp.Rows)
                            {
                                MediumViewModel m = new MediumViewModel();

                                if (item[0].ToString() != "(none)" && item[0].ToString() != "(not set)")
                                {
                                    switch (item[0].ToString())
                                    {
                                        case "organic":
                                            m.source = item[0].ToString() + "(搜索)";//名稱
                                            break;

                                        case "WebPush":
                                            m.source = item[0].ToString() + "(直接)";//名稱

                                            break;

                                        case "referral":
                                            m.source = item[0].ToString() + "(推薦)";//名稱

                                            break;
                                    }

                                    m.litres = int.Parse(item[1].ToString());
                                    MediumViewModel.Add(m);
                                    s++;
                                }
                            }
                            json = JsonConvert.SerializeObject(MediumViewModel);
                        }
                        catch { }

                        #endregion

                        break;

                    ////流量來源(線性圖)
                    case "traffic":

                        string subkey = RouteData.Values["subkey"].ToString();

                        #region 流量來源(線性圖)

                        try
                        {
                            GaParameter.metrics = "ga:organicSearches";
                            GaParameter.Dimensions = "ga:medium,ga:date";

                            string CacheName = "GaMediumDate" + sDate + eDate;
                            dynamic tmp = GaData.ReGaContent(GaParameter); ;
                            if (cacheContainer.Get(CacheName) == null)
                            {
                                tmp = GaData.ReGaContent(GaParameter);
                                cacheContainer.Insert(CacheName, tmp, null, DateTime.Now.AddMinutes(cacheTime), TimeSpan.Zero, CacheItemPriority.Normal, null);
                            }
                            else
                            {
                                tmp = cacheContainer.Get(CacheName);//自cache取出
                            }
                            IFormatProvider ifp = new System.Globalization.CultureInfo("zh-TW", true);

                            List<TrafficViewModel> TrafficViewModel = new List<TrafficViewModel>();
                            int s = 0;
                            foreach (List<string> item in tmp.Rows)
                            {
                                TrafficViewModel m = new TrafficViewModel();

                                if (item[0].ToString() != "(none)" && item[0].ToString() != "(not set)" && item[0].ToString().ToLower() == subkey.ToLower())
                                {
                                    DateTime tempDate = DateTime.ParseExact(item[1].ToString(), "yyyyMMdd", ifp, System.Globalization.DateTimeStyles.None);
                                    m.date = tempDate.ToString("yyyy-MM-dd") + "T16:00:00.000Z";
                                    m.value = int.Parse(item[2].ToString());
                                    TrafficViewModel.Add(m);
                                    s++;
                                }
                            }
                            json = JsonConvert.SerializeObject(TrafficViewModel);
                        }
                        catch { }

                        #endregion

                        break;
                }
            }

            return JsonConvert.SerializeObject(jsonY);
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        // GET: Siteadmin
        public ActionResult Index()
        {
            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }
            if (ViewBag.permissions == null)
            {
                ViewBag.permissions = Session["permissions"];
                ViewBag.permissionsTop = Session["permissionsTop"];
            }

            HttpCookie cookie = System.Web.HttpContext.Current.Request.Cookies["sysLogin"];
            string[] tempAccount = cookie.Value.Split('&');
            //var userguid = tempAccount[1].Replace("sysUserGuid=", "").ToString();
            //return RedirectPermanent(Url.Content("~/siteadmin/edituser/user/edit/" + userguid));

            ViewBag.IsGA = "Y";
            DateRange dateRange = new DateRange { StartDate = DateTime.Now.ToString("yyyy") + "-01-01", EndDate = DateTime.Now.ToString("yyyy") + "-12-31" };

            GoogleAnalyticsService GaData = new GoogleAnalyticsService();
            GaParameterModel GaParameter = new GaParameterModel();//宣告參數使用

            try
            {
                //年度瀏覽次數
                var response = (new GoogleAnalyticsService()).GaPageCount(dateRange);
                if (response != null)
                {
                    if (response.Reports[0].Data.RowCount == null)
                    {
                        ViewBag.RealtimeData = "0";
                    }
                    else
                    {
                        ViewBag.RealtimeData = response.Reports[0].Data.RowCount.ToString();
                    }
                }
                else
                {
                    ViewBag.RealtimeData = "0";
                }
            }
            catch (Exception ex)
            {
                ViewBag.IsGA = "N";

                #region 產生LOG

                FunctionService.saveLog(ex, "Ga.txt");

                #endregion
            }


            //////年度數量

            //#region 年度數量

            //try
            //{

            //    var response = GaData.GaPageCount(dateRange);
            //}
            //catch (Exception ex)
            //{
            //    ViewBag.IsGA = "N";

            //    #region 產生LOG

            //    FunctionService.saveLog(ex, "Ga.txt");

            //    #endregion
            //}

            //#endregion


            ////總瀏覽數

            #region 總瀏覽數

            try
            {
                var response = (new GoogleAnalyticsService()).GaTotalWebViewNum(new DateRange { StartDate = "2019-01-01", EndDate = DateTime.Now.ToString("yyyy") + "-12-31" });
                if (response != null) { ViewBag.TotalWebViewNum = response.ToString(); } else { ViewBag.TotalWebViewNum = "0"; }
            }
            catch (Exception ex)
            {
                ViewBag.IsGA = "N";

                #region 產生LOG

                FunctionService.saveLog(ex, "Ga.txt");

                #endregion
            }

            #endregion

            //GaParameter.startDate = DateTime.Now.ToString("yyyy") + "-01-01";//預設起始日

            ////網站關鍵字

            #region 網站關鍵字

            try
            {
                List<GaModel> keywords = (new GoogleAnalyticsService()).GaKeywords(dateRange);

                if (keywords.Count > 0)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var keyWords = JsonConvert.SerializeObject(keywords.OrderByDescending(m => m.count).Skip(0).Take(5).ToList());
                    fullSearchEngine[] fullEngine = js.Deserialize<fullSearchEngine[]>(keyWords);
                    ViewBag.KeyWords = fullEngine;
                }
                else
                {
                    ViewBag.KeyWords = null;
                }
            }
            catch (Exception ex)
            {
                ViewBag.IsGA = "N";

                #region 產生LOG

                FunctionService.saveLog(ex, "Ga.txt");

                #endregion
            }

            #endregion

            ////搜尋引勤來源數量

            #region 搜尋引勤來源數量

            try
            {

                List<GaModel> organicSearches = (new GoogleAnalyticsService()).GaOrganicSearches(dateRange);

                if (organicSearches.Count > 0)
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var fullReferrer = JsonConvert.SerializeObject(organicSearches.OrderByDescending(m => m.count).ToList());
                    fullSearchEngine[] fullEngine = js.Deserialize<fullSearchEngine[]>(fullReferrer);
                    ViewBag.FullReferrer = fullEngine;
                }
                else
                {
                    ViewBag.FullReferrer = null;
                }
            }
            catch (Exception ex)
            {
                ViewBag.IsGA = "N";

                #region 產生LOG

                FunctionService.saveLog(ex, "Ga.txt");

                #endregion
            }

            #endregion

            //////本月流量次數
            //try
            //{
            //    string thisMonth = int.Parse(DateTime.Now.ToString("MM")).ToString();
            //    ViewBag.ViewYearUseMonth = JsonConvert.DeserializeObject(GaData.ViewYearUseMonth(thisMonth));
            //}
            //catch (Exception ex)
            //{
            //    ViewBag.IsGA = "N";

            //    #region 產生LOG

            //    FunctionService.saveLog(ex, "Ga.txt");

            //    #endregion
            //}

            // 新/舊訪問者

            #region 新舊訪問者

            try
            {
                List<GaModel> gaModel = (new GoogleAnalyticsService()).GaUserType(dateRange);

                List<string> re = new List<string>();

                if (gaModel.Count() == 0)
                {
                    ViewBag.UserType1 = "0";
                    ViewBag.UserType2 = "0";
                }
                else
                {
                    if (gaModel[0].count != null)
                    {
                        ViewBag.UserType1 = gaModel[0].count.ToString();
                    }
                    if (gaModel[1].count != null)
                    {
                        ViewBag.UserType2 = gaModel[1].count.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                //ViewBag.IsGA = "N";

                #region 產生LOG

                FunctionService.saveLog(ex, "Ga.txt");

                #endregion
            }

            #endregion

            //社交網站來源

            #region 社交網站來源

            try
            {
                var response = (new GoogleAnalyticsService()).GaSocialCount(dateRange);
                if (response != null)
                {
                    if (response.Reports[0].Data.RowCount == null)
                    {
                        ViewBag.SocialNetwork = "0";
                    }
                    else
                    {
                        ViewBag.SocialNetwork = response.Reports[0].Data.RowCount.ToString();
                    }
                }
                else
                {
                    ViewBag.SocialNetwork = "0";
                }


            }
            catch (Exception ex)
            {
                // ViewBag.IsGA = "N";

                #region 產生LOG

                FunctionService.saveLog(ex, "Ga.txt");

                #endregion
            }

            #endregion

            //聯絡我們

            //ViewBag.contactData = DB.contacts.OrderByDescending(m => m.create_date).ToList().Skip(0).Take(4);

            //網址
            ViewBag.WebUrl = FunctionService.getWebUrl();

            //最新公告
            ViewBag.News = DB.news.Where(m => m.lang == defLang).OrderByDescending(m => m.startdate).ToList().Skip(0).Take(3);

            //聯絡我們
            //ViewBag.contactData = DB.contacts.OrderByDescending(m => m.create_date).ToList().Skip(0).Take(4);
            return View();
        }

        public class fullSearchEngine
        {
            public string source { get; set; }
            public string count { get; set; }
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        /// <summary>
        /// 列表
        /// </summary>
        /// <returns></returns>
        public ActionResult list()
        {
            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }

            //檢查是否第一次登入或超過90天未變更密碼
            HttpCookie cookie = System.Web.HttpContext.Current.Request.Cookies["sysLogin"];
            string[] tempAccount = cookie.Value.Split('&');
            var userguid = tempAccount[1].Replace("sysUserGuid=", "").ToString();

            //string checkFirstorNinety = FunctionService.checkfirstlogin_ninety();
            //switch (checkFirstorNinety)
            //{
            //    case "First":
            //        TempData["CheckFirstNinety"] = "First";
            //        return RedirectPermanent(Url.Content("~/siteadmin/edituser/user/edit/" + userguid));

            //    case "Ninety":
            //        TempData["CheckFirstNinety"] = "Ninety";
            //        return RedirectPermanent(Url.Content("~/siteadmin/edituser/user/edit/" + userguid));

            //    case "Error":
            //        return RedirectPermanent(Url.Content("~/siteadmin/login"));
            //}

            if (ViewBag.permissions == null)
            {
                ViewBag.permissions = Session["permissions"];
                ViewBag.permissionsTop = Session["permissionsTop"];
            }
            Session.Remove("dataTableCategory");//移除暫存細項

            ViewBag.RetuenToList = "N";
            ViewBag.statusData = null;//狀態篩選顯示
            ViewBag.statusVal = null;//狀態篩選值

            ViewBag.RetuenID = "";

            if (RouteData.Values["tables"] != null)
            {
                string tables = RouteData.Values["tables"].ToString();

                ViewBag.Tables = tables;

                ViewBag.addTitle = "";
                ViewBag.addId = "";
                Session.Remove("dataTableCategory");

                //DataTable用額外參數
                if (RouteData.Values["id"] != null)
                {
                    ViewBag.RetuenToList = "Y";

                    if (tables == "insurance_content_add" || tables == "insurance_company_content_add")
                    {
                        var category = RouteData.Values["id"].ToString();
                        ViewBag.dataTableAddPostVal = "category=" + category;
                    }
                    Session.Add("dataTableCategory", RouteData.Values["id"].ToString());//紀錄傳值


                    //取得標題
                    string reTables = ViewBag.Tables;

                    ViewBag.addTitle = TablesService.getDataTitle(reTables, RouteData.Values["id"].ToString());
                    ViewBag.addId = "?c=" + RouteData.Values["id"].ToString();
                    ViewBag.RetuenID = RouteData.Values["id"].ToString();
                }

                //ViewBag.dataTableAddPostVal = "{'name': 'category', 'value': ''}";
                //if (RouteData.Values["id"] != null)
                //{
                //    ViewBag.dataTableAddPostVal = "{'name': 'category', 'value': '" + RouteData.Values["id"].ToString() + "'}";

                //    Session.Add("dataTableCategory", RouteData.Values["id"].ToString());//紀錄傳值

                //    //取得標題

                //    string reTables = ViewBag.Tables;

                //    ViewBag.addTitle = TablesService.getDataTitle(reTables, RouteData.Values["id"].ToString());
                //    ViewBag.addId = "?c=" + RouteData.Values["id"].ToString();
                //    ViewBag.RetuenToList = "Y";
                //    ViewBag.RetuenID = RouteData.Values["id"].ToString();
                //}
                /*if(ViewBag.forum_status != null)
                {
                    ViewBag.dataTableAddPostVal += ",{'name': 'forum_status', 'value': '" + ViewBag.forum_status.ToString() + "'}";
                }*/

                ViewBag.listMessage = TablesService.listMessage(tables);//取得列表說明

                //取得表頭
                ViewBag.dataTableTitle = TablesService.dataTableTitle(tables);
                ViewBag.columns = "";
                ViewBag.columnsLength = 0;//移除動作排序

                Dictionary<String, Object> defaultOrderBy = TablesService.defaultOrderBy(tables);//取得預設排續欄位名稱
                if (!defaultOrderBy.ContainsKey("orderByKey"))
                {
                    defaultOrderBy.Add("orderByKey", "create_date");
                    defaultOrderBy.Add("orderByType", "desc");
                }

                ViewBag.defaultOrderBy = 1;//預設排續欄位
                ViewBag.defaultOrderType = defaultOrderBy["orderByType"].ToString();//預設排續(asc或desc)

                foreach (KeyValuePair<string, object> item in ViewBag.dataTableTitle)
                {
                    ViewBag.columns += "{\"data\": \"" + item.Key + "\" },";
                    if (item.Key == defaultOrderBy["orderByKey"].ToString())
                    {
                        ViewBag.defaultOrderBy = ViewBag.columnsLength;
                    }
                    ViewBag.columnsLength++;
                }

                ViewBag.columnsLength = ViewBag.columnsLength - 1;//移除動作排序

                #region 取得檢視篩選

                var zzz = Session["returnCategory"];
                var xxx = ViewBag.returnCategory;
                ViewBag.searchReturnCategory = Session["dataTableCategory"];

                Dictionary<String, Object> tempData = new Dictionary<string, object>();
                tempData.Add("nums", 1);

                Dictionary<String, Object> colData = TablesService.getColData(RouteData.Values["tables"].ToString(), tempData);

                if (colData.ContainsKey("other") && ((Dictionary<String, Object>)colData["other"]).Count > 0)
                {
                    Dictionary<String, Object> otherCol = (Dictionary<String, Object>)colData["other"];
                    if (tables == "MLCONTACTUS")
                    {
                        if (otherCol["STATUS"] != null && otherCol["STATUS"].ToString() != "")
                        {
                            var mJObj = Newtonsoft.Json.Linq.JArray.Parse(otherCol["STATUS"].ToString());
                            string[] statusData = mJObj[0]["data"].ToString().Split('/');//狀態篩選顯示
                            string[] statusVal = mJObj[0]["Val"].ToString().Split('/');//狀態篩選值

                            ViewBag.statusData = statusData;//狀態篩選顯示
                            ViewBag.statusVal = statusVal;//狀態篩選值
                        }
                    }
                    else{
                        if (otherCol["status"] != null && otherCol["status"].ToString() != "")
                        {
                            var mJObj = Newtonsoft.Json.Linq.JArray.Parse(otherCol["status"].ToString());
                            string[] statusData = mJObj[0]["data"].ToString().Split('/');//狀態篩選顯示
                            string[] statusVal = mJObj[0]["Val"].ToString().Split('/');//狀態篩選值

                            ViewBag.statusData = statusData;//狀態篩選顯示
                            ViewBag.statusVal = statusVal;//狀態篩選值
                        }
                    }
                  
                }

                #endregion
            }


            var data_m = (from a in DB.projectsetting
                          where a.status == "Y"
                          select a).ToList();

            foreach (var item in data_m)
            {
                var entity = (from a in DB.rentsetting
                              where a.title == item.guid && a.lang == "tw" && a.status == "Y"
                              select a).ToList();

                if (entity.Count() != 0)
                {
                    item.statusname = "已設定";

                    DB.Entry(item).State = EntityState.Modified;
                    DB.SaveChanges();
                }
            }

            return View();
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        /// <summary>
        /// 新增/修改
        /// </summary>
        /// <returns></returns>
        public ActionResult edit()
        {
            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }


            //權限
            if (ViewBag.permissions == null)
            {
                ViewBag.permissions = Session["permissions"];
                ViewBag.permissionsTop = Session["permissionsTop"];
            }
            ViewBag.mainCol = null;
            ViewBag.mediaCol = null;
            ViewBag.otherCol = null;
            ViewBag.guid = "";
            ViewBag.data = null;
            ViewBag.addTitle = "";

            ViewBag.addId = "";
            //dataTableCategory
            if (Session["dataTableCategory"] != null)
            {
                ViewBag.addId = "/" + Session["dataTableCategory"].ToString();
            }

            if (RouteData.Values["tables"] != null)
            {
                ViewBag.useLang = TablesService.useLang(RouteData.Values["tables"].ToString());//是否使用語系

                ViewBag.RetuenID = "";
                //修改資料
                if (RouteData.Values["id"] != null)
                {
                    ViewBag.guid = RouteData.Values["id"].ToString();
                    string Data = TablesService.getPresetData(RouteData.Values["tables"].ToString(), ViewBag.guid);

                    var obj = Newtonsoft.Json.Linq.JArray.Parse(Data.ToString());

                    if (ViewBag.useLang == "Y")
                    {
                        Dictionary<String, Object> langValData = new Dictionary<string, object>();
                        foreach (var item in obj)
                        {
                            langValData.Add(item["lang"].ToString(), item);
                        }

                        ViewBag.data = langValData;
                    }
                    else
                    {
                        ViewBag.data = obj[0];
                    }

                    ViewBag.RetuenID = RouteData.Values["id"].ToString();
                }

                //子層
                if (Request["c"] != null)
                {
                    var tables = RouteData.Values["tables"].ToString();
                    var guid = Request["c"].ToString();
                    ViewBag.addTitle = TablesService.getDataTitle(tables, guid);
                }

                ViewBag.Tables = RouteData.Values["tables"].ToString();

                //NameValueCollection requests = Request.Params;
                //var temp = TablesService.getSearchCategory(RouteData.Values["tables"].ToString(), requests, Url.Content("~/"));
                if (ViewBag.Tables == "insurance_content" || ViewBag.Tables == "insurance_company_content")
                {
                    if (ViewBag.data != null)
                    {
                        Session["returnCategory"] = ViewBag.data["tw"]["category"];
                        ViewBag.returnCategory = ViewBag.data["tw"]["category"];
                    }
                }

                //Dictionary<String, Object> colData = TablesService.getColData(RouteData.Values["tables"].ToString() , ViewBag.data);
                Dictionary<String, Object> colData = new Dictionary<string, object>();
                if (RouteData.Values["edituser"] != null)
                {
                    colData = TablesService.getColData(RouteData.Values["edituser"].ToString(), ViewBag.data);
                    ViewBag.edituser = "Y";

                    ViewBag.CheckFirstNinety = TempData["CheckFirstNinety"];
                }
                else
                {
                    colData = TablesService.getColData(RouteData.Values["tables"].ToString(), ViewBag.data);
                }

                #region 基本欄位設定

                if (((Dictionary<String, Object>)colData["main"]).Count > 0)
                {
                    ViewBag.mainCol = colData["main"];
                }
                if (((Dictionary<String, Object>)colData["media"]).Count > 0)
                {
                    ViewBag.mediaCol = colData["media"];
                }
                if (((Dictionary<String, Object>)colData["other"]).Count > 0)
                {
                    ViewBag.otherCol = colData["other"];
                }

                #endregion

                #region 延伸欄位設定

                if (colData.ContainsKey("user") && ((Dictionary<String, Object>)colData["user"]).Count > 0)
                {
                    ViewBag.userCol = colData["user"];
                }

                if (colData.ContainsKey("review") && ((Dictionary<String, Object>)colData["review"]).Count > 0)
                {
                    ViewBag.reviewCol = colData["review"];
                }
                if (colData.ContainsKey("verify") && ((Dictionary<String, Object>)colData["verify"]).Count > 0)
                {
                    ViewBag.verifyCol = colData["verify"];
                }
                if (colData.ContainsKey("verifyForum") && ((Dictionary<String, Object>)colData["verifyForum"]).Count > 0)
                {
                    ViewBag.verifyColForum = colData["verifyForum"];
                }

                if (colData.ContainsKey("reInfo") && ((Dictionary<String, Object>)colData["reInfo"]).Count > 0)
                {
                    ViewBag.reInfoCol = colData["reInfo"];
                }
                if (colData.ContainsKey("reUser") && ((Dictionary<String, Object>)colData["reUser"]).Count > 0)
                {
                    ViewBag.reUserCol = colData["reUser"];
                }
                if (colData.ContainsKey("modifyer") && ((Dictionary<String, Object>)colData["modifyer"]).Count > 0)
                {
                    ViewBag.modifyerCol = colData["modifyer"];
                }

                if (colData.ContainsKey("delay") && ((Dictionary<String, Object>)colData["delay"]).Count > 0)
                {
                    ViewBag.delayCol = colData["delay"];
                }

                if (colData.ContainsKey("delay2") && ((Dictionary<String, Object>)colData["delay2"]).Count > 0)
                {
                    ViewBag.delay2Col = colData["delay2"];
                }

                if (colData.ContainsKey("viewSet") && ((Dictionary<String, Object>)colData["viewSet"]).Count > 0)
                {
                    ViewBag.viewSetCol = colData["viewSet"];
                }

                if (colData.ContainsKey("hidden") && ((Dictionary<String, Object>)colData["hidden"]).Count > 0)
                {
                    ViewBag.hiddenCol = colData["hidden"];
                }
                if (colData.ContainsKey("indexUse") && ((Dictionary<String, Object>)colData["indexUse"]).Count > 0)
                {
                    ViewBag.reIndexUse = colData["indexUse"];
                }

                #endregion
            }


            return View();
        }
        [HttpPost]
        public ActionResult Upload(FormContext context, string selectproj)
        {
            HttpPostedFileBase file = Request.Files[0];
            string proj = Request["params"];
            if (file.ContentLength > 0)
            {
                string filename = file.FileName;
                var path = Path.Combine(Server.MapPath("~/fileuploads"), filename);
                file.SaveAs(path);
                //string filecontenttype = file.ContentType;
                //byte[] filebytes = new byte[file.ContentLength];
                //var data = file.InputStream.Read(filebytes, 0, Convert.ToInt32(file.FileName));

                IWorkbook workbook = null;
                FileStream filestream = new FileStream(path, FileMode.Open, FileAccess.Read);
                workbook = new XSSFWorkbook(filestream);
                ISheet sheet = workbook.GetSheetAt(0);
                IRow row;
                row = sheet.GetRow(0);
                //if (row != null)
                //{
                //    for (int m = 0; m < row.LastCellNum; m++) //表頭 
                //    {
                //        string cellValue = row.GetCell(m).ToString(); //獲取i行j列數據 
                //        //dt.Columns.Add(cellValue);
                //    }
                //}
                for (int i = 1; i <= sheet.LastRowNum; i++) //對工作表每一行 
                {
                    rentsetting form = new rentsetting();
                    form.guid = Guid.NewGuid().ToString();
                    form.title = proj;
                    form.start_date = DateTime.Now;
                    form.create_date = DateTime.Now;
                    form.status = "Y";
                    form.lang = "tw";
                    form.sortIndex = 1;

                    //System.Data.DataRow dr = dt.NewRow();
                    row = sheet.GetRow(i); //row讀入第i行數據 
                    if (row != null)
                    {
                        for (int j = 0; j < row.LastCellNum; j++) //對工作表每一列 
                        {
                            string cellValue = row.GetCell(j).ToString(); //獲取i行j列數據 
                                                                          //dr[j] = cellValue;
                            if (j == 0)
                            {
                                var tempprj = (from a in DB.projectsetting
                                               where a.title == cellValue && a.lang == "tw" && a.status == "Y"
                                               select a.guid).FirstOrDefault();
                                form.title = tempprj;
                                form.titlename = cellValue;
                            }
                            else if (j == 1)
                            {
                                form.model = cellValue;

                            }
                            else if (j == 2)
                            {
                                if (cellValue == "0")
                                {
                                    form.price = "0";
                                }
                                else
                                {
                                    form.price = Int32.Parse(cellValue.Replace(",", "")).ToString("###,###");
                                }
                            }
                            else if (j == 3)
                            {
                                if (cellValue == "0")
                                {
                                    form.pricediscount = "0";
                                }
                                else
                                {
                                    form.pricediscount = Int32.Parse(cellValue.Replace(",", "")).ToString("###,###");
                                }
                            }
                            else if (j == 4)
                            {
                                form.rentyear = cellValue;
                            }
                            else if (j == 5)
                            {
                                if (cellValue == "0")
                                {
                                    form.rentmonth = "0";
                                }
                                else
                                {
                                    form.rentmonth = Int32.Parse(cellValue.Replace(",", "")).ToString("###,###");
                                }
                            }
                            else if (j == 6)
                            {
                                if (cellValue == "0")
                                {
                                    form.margin = "0";
                                }
                                else
                                {
                                    form.margin = Int32.Parse(cellValue.Replace(",", "")).ToString("###,###");
                                }
                            }
                            else if (j == 7)
                            {
                                if (cellValue == "0")
                                {
                                    form.rebuyprice = "0";
                                }
                                else
                                {
                                    form.rebuyprice = Int32.Parse(cellValue.Replace(",", "")).ToString("###,###");
                                }
                            }
                        }
                    }
                    if (form.title != null)
                    {
                        DB.rentsetting.Add(form);
                        DB.SaveChanges();
                    }

                    //dt.Rows.Add(dr);
                }

                filestream.Close();
            }

            //return RedirectToAction("list");
            return Json(proj, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SelectChangeBackProj(string project)
        {
            Model DB = new Model();

            string resultyear = "";
            var projectname = (from a in DB.projectsetting
                               where a.guid == project && a.status == "Y" && a.lang == "tw"
                               select a).FirstOrDefault();

            //var rentmodel = (from a in DB.rentsetting
            //                 where a.title == projectname.guid && a.status == "Y" && a.lang == "tw"
            //                 select a.model).Distinct().ToList();

            //var rentperiod = (from a in DB.rentsetting
            //                  where a.title == projectname.guid && a.status == "Y" && a.lang == "tw"
            //                  select a.rentyear).Distinct().ToList();
            var rentperiod = projectname.renttime.Split(',').Distinct();
            foreach (var item in rentperiod)
            {
                resultyear = resultyear + item.ToString().Substring(0,1)  + ",";
            }
            resultyear = resultyear.TrimEnd(',');

            projectname.renttime = resultyear;

            return Json(projectname, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SelectChangeBackProjM(string project,string guid)
        {
            Model DB = new Model();

            string resultyear = "";
            var projectname = (from a in DB.projectsetting
                               where a.guid == project && a.status == "Y" && a.lang == "tw"
                               select a).FirstOrDefault();

            //var rentmodel = (from a in DB.rentsetting
            //                 where a.title == projectname.guid && a.status == "Y" && a.lang == "tw"
            //                 select a.model).Distinct().ToList();

            //var rentperiod = (from a in DB.rentsetting
            //                  where a.title == projectname.guid && a.status == "Y" && a.lang == "tw"
            //                  select a.rentyear).Distinct().ToList();
            var rentperiod = projectname.renttime.Split(',').Distinct();
            foreach (var item in rentperiod)
            {
                resultyear = resultyear + item.ToString().Substring(0, 1) + ",";
            }
            resultyear = resultyear.TrimEnd(',');

            projectname.renttime = resultyear;
            var tempyear = (from a in DB.rentsetting
                            where a.guid == guid && a.status == "Y" && a.lang == "tw"
                            select a.rentyear).FirstOrDefault();
            projectname.carstatus = tempyear;

            return Json(projectname, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult save(FormCollection form)
        {
            if (RouteData.Values["tables"] != null)
            {
                string useLang = TablesService.useLang(RouteData.Values["tables"].ToString());//是否使用語系

                //呼叫寫入或修改
                string guid = FunctionService.getGuid();
                string actType = "add";
                if (RouteData.Values["tables"].ToString() == "MLCONTACTUS")
                {
                    if (form["SNO"].ToString() != "")
                    {
                        guid = form["SNO"].ToString();
                        actType = "edit";
                    }
                }
                else
                {
                    if (form["guid"].ToString() != "")
                    {
                        guid = form["guid"].ToString();
                        actType = "edit";
                    }
                }


                Dictionary<String, Object> dic = new Dictionary<string, object>();
                if (useLang == "Y")
                {
                    if (ViewBag.language != null)
                    {
                        int i = 0;
                        foreach (language item in ViewBag.language)
                        {
                            Dictionary<String, Object> subForm = new Dictionary<string, object>();
                            subForm.Add("lang", item.lang);
                            foreach (var key in form.AllKeys)
                            {
                                if (item.lang == "en")
                                {
                                    if (key == "id_tw")
                                    {
                                        subForm.Add(key.Replace("_" + "tw", ""), form[key].ToString());
                                    }
                                    else if (key == "id_en")
                                    {

                                    }
                                    else
                                    {
                                        subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                    }
                                }
                                else
                                {
                                    subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                                }
                            }

                            if (actType == "add")
                            {
                                subForm.Remove("id");
                            }

                            dic.Add(item.lang, subForm);
                        }
                    }
                }
                else
                {
                    foreach (var key in form.AllKeys)
                    {
                        if (actType == "add")
                        {
                            if (key == "id")
                            {

                            }
                            else
                            {
                                dic.Add(key, form[key].ToString());
                            }
                        }
                        else
                        {
                            dic.Add(key, form[key].ToString());
                        }
                    }
                }

                string Field = "";
                if (RouteData.Values["tables"].ToString() == "forum_message")
                {
                    Field = "forum_message";
                }

                if (useLang == "Y")
                {
                    if (ViewBag.language != null)
                    {
                        int i = 0;
                        foreach (language item in ViewBag.language)
                        {
                            string json = JsonConvert.SerializeObject((Dictionary<string, object>)dic[item.lang]);
                            TablesService.saveData(RouteData.Values["tables"].ToString(), json, Field, guid, actType, (Dictionary<string, object>)dic[item.lang]);
                        }
                    }
                }
                else
                {
                    string json = JsonConvert.SerializeObject((Dictionary<string, object>)dic);
                    TablesService.saveData(RouteData.Values["tables"].ToString(), json, Field, guid, actType, (Dictionary<string, object>)dic);
                }

                //完成轉跳
                TempData["success"] = true;
                switch (RouteData.Values["tables"].ToString())
                {
                    case "web_data":
                    case "smtp_data":
                    case "terms":
                    case "privacy":
                    case "contact_content":
                    case "service_content":
                    case "recruiting_content":
                    case "pdf_setting":
                        TempData["Url"] = Url.Content("~/siteadmin/" + RouteData.Values["tables"].ToString() + "/edit/" + guid);
                        break;

                    default:
                        TempData["Url"] = Url.Content("~/siteadmin/" + RouteData.Values["tables"].ToString() + "/list/");
                        break;
                }
                Response.Write("<script>window.location.href='" + Url.Content("~/siteadmin/" + RouteData.Values["tables"].ToString() + "/edit/" + guid) + "';</script>");
            }
            return null;
        }

        /// <summary>
        /// DataTable
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public string dataTable()
        {
            try
            {
                EFUnitOfWork model = new EFUnitOfWork(DB);
                Dictionary<String, Object> listData = new Dictionary<string, object>();
                Dictionary<String, Object> tempListData = new Dictionary<string, object>();

                string json = "";

                dynamic data = null;

                if (RouteData.Values["tables"] != null)
                {
                    NameValueCollection requests = Request.Params;

                    data = TablesService.getListData(RouteData.Values["tables"].ToString(), requests, Url.Content("~/"));
                }

                listData = data;

                json = JsonConvert.SerializeObject(listData, Formatting.Indented);

                return json;
            }
            catch (Exception ex)
            {
                return ex.StackTrace;
            }
        }
        private static string formatOrderByKey(string tables, string orderByKey)
        {
            string re = "";
            switch (orderByKey)
            {
                case "info":
                    re = "title";
                    break;

                case "view_info":
                    re = "nums";
                    break;

                case "forum_status":
                    re = "status";
                    break;

                case "action":
                    re = "guid";
                    break;

                case "user":
                    re = "user_guid";
                    break;

                case "category":
                    re = "category";
                    if (tables == "package_content")
                    {
                        re = "package_guid";
                    }
                    break;

                default:
                    re = orderByKey;
                    break;
            }
            return HttpUtility.HtmlEncode(re);
        }
        private static dynamic dataTableTitle(string tables)
        {
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //最新消息分類
                case "news_category":
                    re = Repository.newsCategory__Repository.dataTableTitle();
                    break;

                //最新消息
                case "news":
                    re = Repository.news__Repository.dataTableTitle();
                    break;

                #region -- 控制台 --

                //資源回收桶
                case "ashcan":
                    re = Repository.ashcan__Repository.dataTableTitle();
                    break;

                //防火牆
                case "firewalls":
                    re = Repository.firewalls__Repository.dataTableTitle();
                    break;

                //系統日誌
                case "system_log":
                    re = Repository.systemLog__Repository.dataTableTitle();
                    break;

                #endregion

                #region -- 帳戶資訊 --

                //使用者
                case "user":
                    re = Repository.user__Repository.dataTableTitle();
                    break;

                //群組管理
                case "roles":
                    re = Repository.roles__Repository.dataTableTitle();
                    break;

                    #endregion
            }

            return re;
        }
        private static string tableTitleKey(string tables, dynamic dataList)
        {
            Model DB = new Model();
            string re = "";
            switch (tables)
            {
                case "user":
                    re = dataList["name"];
                    break;

                default:
                    re = dataList["title"];
                    break;
            }
            return HttpUtility.HtmlEncode(re);
        }
        [HttpPost]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public string Ajax(FormCollection form)
        {
            Dictionary<String, Object> dic = new Dictionary<string, object>();

            NameValueCollection getData = FunctionService.reSubmitFormDataJson(HttpUtility.UrlDecode(form["Val"].ToString()));//回傳JSON資料
            dic.Add("Func", form["Func"].ToString());

            switch (form["Func"].ToString())
            {
                #region 徹底刪除

                case "removeData":

                    if (getData != null)
                    {
                        string[] guidArr = getData["guid"].ToString().Split(',');
                        string[] titleArr = getData["title"].ToString().Split('|');
                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            Guid guid = Guid.Parse(guidArr[i].ToString());
                            var ashcan = DB.ashcan.Where(m => m.guid == guid).FirstOrDefault();

                            switch (ashcan.tables)
                            {
                                case "forum":
                                    DB.Database.ExecuteSqlCommand("delete from forum where top_guid = '" + ashcan.from_guid + "'");
                                    DB.Database.ExecuteSqlCommand("delete from forum_message where forum_guid = '" + ashcan.from_guid + "'");
                                    break;
                            }

                            //DB.Database.ExecuteSqlCommand("delete from [" + ashcan.tables + "] where guid = '" + ashcan.from_guid + "'");
                            //DB.Database.ExecuteSqlCommand("delete from ashcan where guid = '" + guid.ToString() + "'");

                            if (getData["type"].ToString() == "del")
                            {
                                DB.Database.ExecuteSqlCommand("delete from [" + ashcan.tables + "] where guid = '" + ashcan.from_guid + "'");
                            }
                            else
                            {
                                DB.Database.ExecuteSqlCommand("update [" + ashcan.tables + "] set status = 'N' where guid = '" + ashcan.from_guid + "'");
                            }

                            DB.Database.ExecuteSqlCommand("delete from [ashcan] where guid = '" + guid.ToString() + "'");
                        }
                    }
                    break;

                #endregion

                #region 修改狀態

                case "changeStatus":

                    if (getData != null)
                    {
                        string useLang = TablesService.useLang(getData["tables"].ToString());//是否使用語系

                        string[] guidArr = getData["guid"].ToString().Split(',');
                        string[] titleArr = getData["title"].ToString().Split('|');
                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            if (useLang == "Y")
                            {
                                if (ViewBag.language != null)
                                {
                                    foreach (language item in ViewBag.language)
                                    {
                                        Dictionary<String, Object> temp = new Dictionary<string, object>();
                                        if (getData["tables"].ToString() == "MLCONTACTUS")
                                        {
                                            temp.Add("SNO", guidArr[i].ToString());
                                        }
                                        else
                                        {
                                            temp.Add("guid", guidArr[i].ToString());
                                        }
                                        
                                        temp.Add("status", getData["status"].ToString());
                                        temp.Add("lang", item.lang);
                                        string jsonData = JsonConvert.SerializeObject(temp);
                                        //呼叫寫入或修改
                                        string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "status", guidArr[i].ToString(), "edit", null);
                                    }
                                }
                            }
                            else
                            {
                                Dictionary<String, Object> temp = new Dictionary<string, object>();
                                if (getData["tables"].ToString() == "MLCONTACTUS")
                                {
                                    temp.Add("SNO", guidArr[i].ToString());
                                }
                                else
                                {
                                    temp.Add("guid", guidArr[i].ToString());
                                }
                                temp.Add("status", getData["status"].ToString());
                                string jsonData = JsonConvert.SerializeObject(temp);
                                //呼叫寫入或修改
                                string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "status", guidArr[i].ToString(), "edit", null);
                            }
                        }

                        //資源回收桶
                        if (getData["status"].ToString() == "D")
                        {
                            ashcan ashcan = new ashcan();
                            for (int i = 0; i < guidArr.Length; i++)
                            {
                                ashcan.title = titleArr[i].ToString();
                                ashcan.tables = getData["tables"].ToString();
                                ashcan.from_guid = guidArr[i].ToString();
                                ashcan.create_date = DateTime.Now;
                                ashcan.modifydate = DateTime.Now;
                                DB.ashcan.Add(ashcan);

                                DB.SaveChanges();
                            }
                        }

                        dic["status"] = getData["status"].ToString();
                    }
                    break;

                #endregion

                #region 修改排序

                case "cahngeSortIndex":

                    if (getData != null)
                    {
                        string useLang = TablesService.useLang(getData["tables"].ToString());//是否使用語系
                        string[] guidArr = getData["guid"].ToString().Split(',');

                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            /*Dictionary<String, Object> temp = new Dictionary<string, object>();
                            temp.Add("guid", guidArr[i].ToString());
                            temp.Add("sortIndex", getData["sortIndex"].ToString());
                            string jsonData = JsonConvert.SerializeObject(temp);
                            //呼叫寫入或修改
                            string guid = TablesService.saveData(getData["tables"].ToString(), guidArr[i].ToString(), "edit", jsonData, "sortIndex");
                            */
                            if (useLang == "Y")
                            {
                                if (ViewBag.language != null)
                                {
                                    foreach (language item in ViewBag.language)
                                    {
                                        Dictionary<String, Object> temp = new Dictionary<string, object>();
                                        temp.Add("guid", guidArr[i].ToString());
                                        temp.Add("sortIndex", getData["sortIndex"].ToString());
                                        temp.Add("lang", item.lang);
                                        string jsonData = JsonConvert.SerializeObject(temp);
                                        //呼叫寫入或修改
                                        string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "sortIndex", guidArr[i].ToString(), "edit", null);
                                    }
                                }
                            }
                            else
                            {
                                Dictionary<String, Object> temp = new Dictionary<string, object>();
                                temp.Add("guid", guidArr[i].ToString());
                                temp.Add("sortIndex", getData["sortIndex"].ToString());
                                string jsonData = JsonConvert.SerializeObject(temp);
                                //呼叫寫入或修改
                                string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "sortIndex", guidArr[i].ToString(), "edit", null);
                            }
                        }
                    }
                    break;

                #endregion

                #region 登入

                case "sysLogin":

                    if (getData != null)
                    {
                        system_log system_Log = new system_log();

                        dic["re"] = "OK";

                        if (getData["verification"].ToString() != Session["_ValCheckCode"].ToString())
                        {
                            dic["re"] = "codeError";
                            system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，驗證碼輸入錯誤!</code>";
                            system_Log.status = "N";
                        }
                        else
                        {
                            string username = getData["username"].ToString();

                            var users = DB.user.Where(m => m.username == username);
                            if (users.Count() <= 0)
                            {
                                dic["re"] = "usernameError";
                                system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，查無此帳號!</code>";
                                system_Log.status = "N";
                            }
                            else
                            {
                                string password = FunctionService.md5(getData["password"].ToString());

                                users = users.Where(m => m.thepr == password);
                                if (users.Count() <= 0)
                                {
                                    dic["re"] = "passwordError";
                                }
                                else
                                {
                                    users = users.Where(m => m.status == "Y");
                                    if (users.Count() <= 0)
                                    {
                                        dic["re"] = "statusError";
                                        system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，帳號為停用狀態!</code>";
                                        system_Log.status = "N";
                                    }
                                    else
                                    {
                                        if (adminCathType == "Session")
                                        {
                                            Session.Add("sysUsername", username);
                                            Session.Add("sysUserGuid", users.FirstOrDefault().guid);
                                        }
                                        else
                                        {
                                            //判斷IP
                                            int firewallsCount = DB.firewalls.Where(m => m.status != "D").Count();


                                            // 取得本機名稱
                                            string strHostName = Dns.GetHostName();
                                            string ipv4 = "";
                                            // 取得本機的IpHostEntry類別實體，用這個會提示已過時
                                            //IPHostEntry iphostentry = Dns.GetHostByName(strHostName);

                                            // 取得本機的IpHostEntry類別實體，MSDN建議新的用法
                                            IPHostEntry iphostentry = Dns.GetHostEntry(strHostName);

                                            // 取得所有 IP 位址
                                            foreach (IPAddress ipaddress in iphostentry.AddressList)
                                            {
                                                // 只取得IP V4的Address
                                                if (ipaddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                                                {
                                                    //Console.WriteLine("Local IP: " + ipaddress.ToString());
                                                    ipv4 = ipaddress.ToString();
                                                    //Response.Write("<script>alert('" + ipaddress.ToString() + "')</script>");
                                                }
                                            }


                                            String strHostName2 = string.Empty;
                                            IPHostEntry ipEntry = Dns.GetHostEntry(Dns.GetHostName());
                                            IPAddress[] addr = ipEntry.AddressList;






                                            bool ipCheck = true;

                                            //有IP判斷是否可通行
                                            if (firewallsCount > 0 && getData["username"].ToString() != "sysadmin" && getData["username"].ToString() != "admin")
                                            {
                                                string nowIP = FunctionService.GetIP();
                                                //string nowIP = addr[1].ToString();
                                                //string nowIP = ip;
                                                //string nowIP = ipv4;

                                                if (nowIP == "")
                                                {
                                                    nowIP = ipv4;
                                                }

                                                Web.Models.firewalls firewalls = DB.firewalls.Where(m => m.ip == nowIP && m.status == "Y").FirstOrDefault();

                                                if (firewalls == null)
                                                {
                                                    dic["re"] = "ipError";
                                                    ipCheck = false;
                                                    system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，IP比對錯誤!</code>";
                                                    system_Log.status = "N";
                                                }
                                                else
                                                {
                                                    if (firewalls.status == "N")
                                                    {
                                                        dic["re"] = "ipError";
                                                        ipCheck = false;
                                                        system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，IP比對錯誤!</code>";
                                                        system_Log.status = "N";
                                                    }
                                                }
                                            }

                                            if (ipCheck)
                                            {
                                                system_Log.notes = "登入成功";
                                                system_Log.status = "Y";

                                                if (adminCathType == "Session")
                                                {
                                                    Session.Add("sysUsername", username);
                                                    Session.Add("sysUserGuid", users.FirstOrDefault().guid);
                                                }
                                                else
                                                {
                                                    //產生一個Cookie
                                                    HttpCookie cookie = new HttpCookie("sysLogin");
                                                    //設定過期日
                                                    cookie.Expires = DateTime.Now.AddDays(365);
                                                    cookie.Values.Add("sysUsername", username);//增加属性
                                                    cookie.Values.Add("sysUserGuid", users.FirstOrDefault().guid);
                                                    Response.AppendCookie(cookie);//确定写入cookie中
                                                }

                                                FunctionService.getAdminPermissions(username);//權限
                                                                                              //更新登入日期
                                                var saveData = DB.user.Where(m => m.username == username).Where(m => m.status == "Y").FirstOrDefault();
                                                saveData.logindate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                                DB.SaveChanges();
                                            }
                                        }
                                    }

                                    if (getData["keepMe"] != null)
                                    {
                                        if (getData["keepMe"] == "Y")
                                        {
                                            //產生一個Cookie
                                            HttpCookie cookie2 = new HttpCookie("keepMeAccount");
                                            //設定單值
                                            cookie2.Value = getData["username"].ToString();
                                            //設定過期日
                                            cookie2.Expires = DateTime.Now.AddDays(365);
                                            //寫到用戶端
                                            Response.Cookies.Add(cookie2);
                                        }
                                    }
                                }
                            }

                            dic["url"] = Url.Content("~/siteadmin");
                        }

                        system_Log.title = "後台登入帳號：" + getData["username"].ToString();
                        system_Log.ip = FunctionService.GetIP();
                        system_Log.types = "sysLogin";
                        system_Log.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        system_Log.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        system_Log.guid = Guid.NewGuid().ToString();
                        system_Log.username = getData["username"].ToString();

                        DB.system_log.Add(system_Log);
                        DB.SaveChanges();
                    }
                    break;

                    #endregion
            }

            string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
            //輸出json格式
            return json;
        }

        /// <summary>
        /// 帳號判斷
        /// </summary>
        [HttpGet]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public void CkAccount()
        {
            dynamic data = null;
            string username = Request["username"].ToString();

            if (RouteData.Values["tokens"] != null)
            {
                string[] inputAccount = RouteData.Values["tokens"].ToString().Split('@');

                switch (inputAccount[0])
                {
                    case "user":
                        data = DB.user.Where(m => m.username == username).Count();
                        break;
                }
            }
            else
            {
                data = DB.user.Where(m => m.username == username).Count();
            }

            if (data != null && data > 0)
            {
                Response.Write("false");
            }
            else
            {
                Response.Write("true");
            }
        }

        public ActionResult Logout()
        {
            Response.Cookies.Clear();

            //FormsAuthentication.SignOut();

            HttpCookie c = new HttpCookie("sysLogin");
            c.Values.Remove("adminDataID");
            c.Values.Remove("adminCategory");
            c.Values.Remove("adminUsername");
            c.Values.Remove("sysUsername");
            c.Values.Remove("sysUserGuid");

            c.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(c);

            Session.Clear();

            return RedirectPermanent(Url.Content("~/siteadmin/login"));
        }

        //更改較少資訊Cache使用
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //登入判斷
            ViewBag.projeftNo = projeftNo;

            ViewBag.username = "";
            ViewBag.role_guid = null;

            ViewBag.defLang = defLang;

            ViewBag.permissions = Session["permissions"];
            ViewBag.permissionsTop = Session["permissionsTop"];

            if (Request.Cookies["keepMeAccount"] != null || Session["sysUsername"] != null)
            {
                if (adminCathType == "Session" && Session["sysUsername"] != null && Session["sysUsername"].ToString() != "")
                {
                    ViewBag.username = Session["sysUsername"].ToString();
                }
                else
                {
                    if (Request.Cookies["sysLogin"] != null)
                    {
                        HttpCookie aCookie = Request.Cookies["sysLogin"];
                        if (aCookie != null && aCookie.Value != "" && aCookie.Value.IndexOf("&") != -1)
                        {
                            List<string> sysUser = aCookie.Value.Split('&').ToList();
                            string sysUsername = Server.HtmlEncode(sysUser[0].Replace("sysUsername=", ""));
                            ViewBag.username = sysUsername;
                        }
                    }
                }
            }

            string changestatus = HttpUtility.HtmlEncode("Y");
            string changearea = HttpUtility.HtmlEncode("admin");
            if (ViewBag.username == "sysadmin")
            {
                List<system_menu> changeSystemMenu = new List<system_menu>();
                system_menu singleChangeSystemMenu = new system_menu();
                foreach (var item in DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.status == changestatus).ToList())
                {
                    singleChangeSystemMenu.guid = HttpUtility.HtmlEncode(item.guid);
                    singleChangeSystemMenu.title = HttpUtility.HtmlEncode(item.title);
                    singleChangeSystemMenu.category = HttpUtility.HtmlEncode(item.category);
                    singleChangeSystemMenu.tables = HttpUtility.HtmlEncode(item.tables);
                    singleChangeSystemMenu.sortindex = Int32.Parse(HttpUtility.HtmlEncode(item.sortindex.ToString()));
                    singleChangeSystemMenu.status = HttpUtility.HtmlEncode(item.status);
                    singleChangeSystemMenu.act_path = HttpUtility.HtmlEncode(item.act_path);
                    singleChangeSystemMenu.icon = HttpUtility.HtmlEncode(item.icon);
                    singleChangeSystemMenu.area = HttpUtility.HtmlEncode(item.area);
                    singleChangeSystemMenu.category_table = HttpUtility.HtmlEncode(item.category_table);
                    singleChangeSystemMenu.index_view_url = HttpUtility.HtmlEncode(item.index_view_url);
                    singleChangeSystemMenu.can_add = HttpUtility.HtmlEncode(item.can_add);
                    singleChangeSystemMenu.can_edit = HttpUtility.HtmlEncode(item.can_edit);
                    singleChangeSystemMenu.can_del = HttpUtility.HtmlEncode(item.can_del);
                    singleChangeSystemMenu.prev_table = HttpUtility.HtmlEncode(item.prev_table);
                    singleChangeSystemMenu.system_sortindex = Int32.Parse(HttpUtility.HtmlEncode(item.system_sortindex.ToString()));

                    changeSystemMenu.Add(singleChangeSystemMenu);
                    singleChangeSystemMenu = new system_menu();
                }
                ViewBag.system_menu = changeSystemMenu;
            }
            else
            {
                List<system_menu> changeSystemMenu = new List<system_menu>();
                system_menu singleChangeSystemMenu = new system_menu();
                foreach (var item in DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.status == changestatus).Where(m => m.area != changearea).ToList())
                {
                    singleChangeSystemMenu.guid = HttpUtility.HtmlEncode(item.guid);
                    singleChangeSystemMenu.title = HttpUtility.HtmlEncode(item.title);
                    singleChangeSystemMenu.category = HttpUtility.HtmlEncode(item.category);
                    singleChangeSystemMenu.tables = HttpUtility.HtmlEncode(item.tables);
                    singleChangeSystemMenu.sortindex = Int32.Parse(HttpUtility.HtmlEncode(item.sortindex.ToString()));
                    singleChangeSystemMenu.status = HttpUtility.HtmlEncode(item.status);
                    singleChangeSystemMenu.act_path = HttpUtility.HtmlEncode(item.act_path);
                    singleChangeSystemMenu.icon = HttpUtility.HtmlEncode(item.icon);
                    singleChangeSystemMenu.area = HttpUtility.HtmlEncode(item.area);
                    singleChangeSystemMenu.category_table = HttpUtility.HtmlEncode(item.category_table);
                    singleChangeSystemMenu.index_view_url = HttpUtility.HtmlEncode(item.index_view_url);
                    singleChangeSystemMenu.can_add = HttpUtility.HtmlEncode(item.can_add);
                    singleChangeSystemMenu.can_edit = HttpUtility.HtmlEncode(item.can_edit);
                    singleChangeSystemMenu.can_del = HttpUtility.HtmlEncode(item.can_del);
                    singleChangeSystemMenu.prev_table = HttpUtility.HtmlEncode(item.prev_table);
                    if (item.system_sortindex != null)
                    {
                        singleChangeSystemMenu.system_sortindex = Int32.Parse(HttpUtility.HtmlEncode(item.system_sortindex.ToString()));
                    }
                    else
                    {
                        singleChangeSystemMenu.system_sortindex = null;
                    }

                    changeSystemMenu.Add(singleChangeSystemMenu);
                    singleChangeSystemMenu = new system_menu();
                }
                ViewBag.system_menu = changeSystemMenu;
            }

            ViewBag.system_menu_data = null;
            ViewBag.tables = "";
            if (RouteData.Values["tables"] != null)
            {
                string tables = HttpUtility.HtmlEncode(RouteData.Values["tables"].ToString());

                system_menu systemMenu_pre = new system_menu();
                systemMenu_pre.guid = HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().guid);
                systemMenu_pre.title = HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().title);
                systemMenu_pre.category = HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().category);
                systemMenu_pre.tables = HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().tables);
                systemMenu_pre.sortindex = Int32.Parse(HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().sortindex.ToString()));
                systemMenu_pre.status = HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().status);
                systemMenu_pre.act_path = HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().act_path);
                systemMenu_pre.icon = HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().icon);
                systemMenu_pre.area = HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().area);
                systemMenu_pre.category_table = HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().category_table);
                systemMenu_pre.index_view_url = HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().index_view_url);
                systemMenu_pre.can_add = HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().can_add);
                systemMenu_pre.can_edit = HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().can_edit);
                systemMenu_pre.can_del = HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().can_del);
                systemMenu_pre.prev_table = HttpUtility.HtmlEncode(DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault().prev_table);

                ViewBag.system_menu_data = systemMenu_pre;

                string topCategory = ViewBag.system_menu_data.category;
                //ViewBag.system_menu_top_data = HttpUtility.HtmlEncode(DB.system_menu.Where(m => m.guid == topCategory).FirstOrDefault());
                ViewBag.system_menu_top_data_title = HttpUtility.HtmlEncode(DB.system_menu.Where(m => m.guid == topCategory).FirstOrDefault().title);
                ViewBag.tables = RouteData.Values["tables"].ToString();

                //if (tables == "notes_data")
                //{
                //    if (RouteData.Values["id"] != null && RouteData.Values["id"].ToString() != "")
                //    {
                //        string act_path = HttpUtility.HtmlEncode("edit/" + RouteData.Values["id"].ToString());

                //        ViewBag.system_menu_data = DB.system_menu.Where(m => m.tables == tables).Where(m => m.act_path == act_path).FirstOrDefault();
                //    }
                //}
                //if (tables == "governances")
                //{
                //    if (RouteData.Values["id"] != null && RouteData.Values["id"].ToString() != "")
                //    {
                //        string act_path = "list/" + RouteData.Values["id"].ToString();

                //        ViewBag.system_menu_data = DB.system_menu.Where(m => m.tables == tables).Where(m => m.act_path == act_path).FirstOrDefault();
                //    }
                //}
            }

            ViewBag.language = null;
            ViewBag.viewLanguage = "語系";

            language changeLanguage = new language();
            List<language> changeLanguageList = new List<language>();

            foreach (var itemChangeLang in DB.language.OrderBy(m => m.sortIndex).Where(m => m.status == changestatus).ToList())
            {
                changeLanguage.id = Int32.Parse(HttpUtility.HtmlEncode(itemChangeLang.id.ToString()));
                changeLanguage.title = HttpUtility.HtmlEncode(itemChangeLang.title);
                changeLanguage.codes = HttpUtility.HtmlEncode(itemChangeLang.codes);
                changeLanguage.status = HttpUtility.HtmlEncode(itemChangeLang.status);
                if (itemChangeLang.sortIndex != null)
                {
                    changeLanguage.sortIndex = Int32.Parse(HttpUtility.HtmlEncode(itemChangeLang.sortIndex.ToString()));
                }
                else
                {
                    changeLanguage.sortIndex = null;
                }

                changeLanguage.date = DateTime.Parse(HttpUtility.HtmlEncode(itemChangeLang.date.ToString()));
                changeLanguage.lang = HttpUtility.HtmlEncode(itemChangeLang.lang
                    );

                changeLanguageList.Add(changeLanguage);
                changeLanguage = new language();
            }

            if (changeLanguageList.Count > 0)
            {
                ViewBag.language = changeLanguageList;
                ViewBag.viewLanguage = HttpUtility.HtmlEncode(changeLanguageList[0].title);
            }

            ViewBag.userData = FunctionService.ReUserData();//回傳使用者資訊

            //系統資訊
            Guid systemGuid = Guid.Parse("4795DABF-18DE-490E-9BB2-D57B4D99C127");
            //ViewBag.systemData = DB.system_data.Where(m => m.guid == systemGuid).FirstOrDefault();
            ViewBag.systemDataTitle = HttpUtility.HtmlEncode(DB.system_data.Where(m => m.guid == systemGuid).FirstOrDefault().title);
            ViewBag.systemDataLoginTitle = HttpUtility.HtmlEncode(DB.system_data.Where(m => m.guid == systemGuid).FirstOrDefault().login_title);
            ViewBag.systemDataBackground = HttpUtility.HtmlEncode(DB.system_data.Where(m => m.guid == systemGuid).FirstOrDefault().background);
            ViewBag.systemDataLogo = HttpUtility.HtmlEncode(DB.system_data.Where(m => m.guid == systemGuid).FirstOrDefault().logo);
            ViewBag.systemDataDesign = HttpUtility.HtmlEncode(DB.system_data.Where(m => m.guid == systemGuid).FirstOrDefault().design_by);
        }

        private void errorLog(string sErrMsg)
        {
            StreamWriter sw = new StreamWriter(Server.MapPath("..\\Log\\Error.log"), true);
            sw.WriteLine(sErrMsg);
            sw.Flush();
            sw.Close();
        }

        /// <summary>
        /// 無限欄位用
        /// </summary>
        public partial class infinityLayoutModel
        {
            public string key { get; set; }
            public List<string> val { get; set; }
        }

        public partial class verifyInfoModel
        {
            public string date { get; set; }

            public string name { get; set; }

            public string info { get; set; }

            public string status { get; set; }
        }

        /// <summary>
        /// 取得是否有審核功能
        /// </summary>
        /// <returns></returns>
        public static List<string> verifyTables()
        {
            List<string> tables = new List<string>();

            tables.Add(HttpUtility.HtmlEncode("insurance_category"));
            tables.Add(HttpUtility.HtmlEncode("insurance_content"));
            tables.Add(HttpUtility.HtmlEncode("insurance_content_main"));
            tables.Add(HttpUtility.HtmlEncode("insurance_content_add"));
            tables.Add(HttpUtility.HtmlEncode("insurance_faq_category"));
            tables.Add(HttpUtility.HtmlEncode("insurance_faq"));
            tables.Add(HttpUtility.HtmlEncode("insurance_company_category"));
            tables.Add(HttpUtility.HtmlEncode("insurance_company_content"));
            tables.Add(HttpUtility.HtmlEncode("insurance_companyfaq_category"));
            tables.Add(HttpUtility.HtmlEncode("insurance_companyfaq"));

            return tables;
        }
    }
}