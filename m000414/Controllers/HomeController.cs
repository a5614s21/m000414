﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Models.EntityModel;

namespace m000414.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Model DB = new Model();

            var homebanner = DB.homebanner.Where(x => x.lang == "tw" && x.status == "Y").OrderBy(x => x.sortIndex).ToList();
            ViewBag.homebanerData = homebanner;

            var videotitlecontent = DB.videocontent.Where(x => x.lang == "tw" && x.status == "Y").Select(x=>x.titlecontent).FirstOrDefault();
            ViewBag.videoTitleContent = videotitlecontent;

            var videocontent = DB.videocontent.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.content).FirstOrDefault();
            ViewBag.videoContent = videocontent;

            var videolink = DB.videocontent.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.url).FirstOrDefault();
            ViewBag.videoLink = videolink;

            var videolinkone = DB.benfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.videolinkone).FirstOrDefault();
            ViewBag.videoLinkone = videolinkone;

            var videolinktwo = DB.benfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.videolinktwo).FirstOrDefault();
            ViewBag.videoLinktwo = videolinktwo;

            var videolinkthree = DB.benfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.videolinkthree).FirstOrDefault();
            ViewBag.videoLinkthree = videolinkthree;

            var videolinkfour = DB.benfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.videolinkfour).FirstOrDefault();
            ViewBag.videoLinkfour = videolinkfour;

            var videoimg = DB.videocontent.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.pic).FirstOrDefault();
            ViewBag.videoImg = videoimg;

            var benfittitlecontent = DB.benfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.titlecontent).FirstOrDefault();
            ViewBag.benfitTitleContent = benfittitlecontent;

            var benfitcontent = DB.benfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.content).FirstOrDefault();
            ViewBag.benfitContent = benfitcontent;

            var benfitcontentone = DB.benfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.contentone).FirstOrDefault();
            ViewBag.benfitContentOne = benfitcontentone;

            var benfitcontenttwo = DB.benfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.contenttwo).FirstOrDefault();
            ViewBag.benfitContentTwo = benfitcontenttwo;

            var benfitcontentthree = DB.benfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.contentthree).FirstOrDefault();
            ViewBag.benfitContentThree = benfitcontentthree;

            var benfitcontentfour = DB.benfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.contentfour).FirstOrDefault();
            ViewBag.benfitContentFour = benfitcontentfour;

            var benfitImg = DB.benfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.pic).FirstOrDefault();
            ViewBag.benfitImg = benfitImg;

            var benfitImgtwo = DB.benfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.pictwo).FirstOrDefault();
            ViewBag.benfitImgTwo = benfitImgtwo;

            var benfitImgthree = DB.benfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.picthree).FirstOrDefault();
            ViewBag.benfitImgThree = benfitImgthree;

            var benfitImgfour = DB.benfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.picfour).FirstOrDefault();
            ViewBag.benfitImgFour = benfitImgfour;

            var rentbenfitcontent = DB.rentbenfit.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.content).FirstOrDefault();
            ViewBag.rentbenfitContent = rentbenfitcontent;

            var servicecontent = DB.contact_service.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.content).FirstOrDefault();
            ViewBag.serviceContent = servicecontent;

            var multipleselectcontent = DB.multipleselect.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.content).FirstOrDefault();
            ViewBag.multipleselectContent = multipleselectcontent;

            var comparemoneycontent = DB.comparemoney.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.content).FirstOrDefault();
            ViewBag.comparemoneyContent = comparemoneycontent;

            var comparetimecontent = DB.comparetime.Where(x => x.lang == "tw" && x.status == "Y").Select(x => x.content).FirstOrDefault();
            ViewBag.comparetimeContent = comparetimecontent;

            var membershare = DB.membershare.Where(x => x.lang == "tw" && x.status == "Y" && x.inoutline == "上架").OrderByDescending(x => x.sticky).ThenBy(x => x.sortIndex).ToList();
            ViewBag.membershareData = membershare;

            var activeproject = (from a in DB.projectsetting
                            where a.status == "Y" && a.lang == "tw"
                            select a).ToList();
            ViewBag.activeprojectData = activeproject;

            List<projectsetting> reProjoption = new List<projectsetting>();
            var projoption = (from a in DB.projectsetting
                             where a.status == "Y" && a.lang == "tw" && a.indoor == "上架"
                             select a).ToList();
            foreach (var item in projoption)
            {
                var tempisrent = (from a in DB.rentsetting
                              where a.title == item.guid && a.status == "Y" && a.lang == "tw"
                              select a).ToList();
                if (tempisrent.Count() != 0)
                {
                    reProjoption.Add(item);
                }
            }
            ViewBag.projoptionData = reProjoption;

            var carstatus = (from a in DB.car_status
                             where a.status == "Y" && a.lang == "tw"
                             select a).ToList();
            ViewBag.carStatusData = carstatus;

            var renttime = (from a in DB.renttime
                            where a.status == "Y" && a.lang == "tw"
                            select a).ToList();
            ViewBag.rentTimeData = renttime;

            var brandcategory = (from a in DB.brand_category
                             where a.status == "Y" && a.lang == "tw"
                             select a).ToList();
            ViewBag.brandCategoryData = brandcategory;

            var monthrentcategory = (from a in DB.monthrent_category
                             where a.status == "Y" && a.lang == "tw"
                             select a).ToList();
            ViewBag.monthRentCategoryData = monthrentcategory;

            var carmodel = (from a in DB.carmodel
                            where a.status == "Y" && a.lang == "tw"
                            select a).ToList();
            ViewBag.carModelData = carmodel;

            var seodata = (from a in DB.web_data
                           where a.lang == "tw"
                           select a).FirstOrDefault();
            if (seodata != null)
            {
                ViewBag.description = seodata.seo_description;
                ViewBag.keywords = seodata.seo_keywords;
            }
            else
            {
                ViewBag.description = "";
                ViewBag.keywords = "";
            }

            var countrycitydata = (from a in DB.countrycity
                                   where a.status == "Y"
                                   select a).ToList();
            ViewBag.countrycitydata = countrycitydata;

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult Send(FormCollection form)
        {
            Model DB = new Model();

            var formData1 = form["carstatus"].ToString();
            var formData2 = form["renttime"].ToString();
            var formData3 = form["brand"].ToString();
            var formData4 = form["monthrent"].ToString();

            var searchData1 = (from a in DB.projectsetting
                              where a.carstatus == formData1 && a.status == "Y" && a.lang == "tw"
                              select a).ToList();

            var searchData2 = (from a in searchData1
                               where a.renttime.Contains(formData2) && a.status == "Y" && a.lang == "tw"
                               select a).ToList();

            var searchData3 = (from a in searchData2
                               where a.brand == formData3 && a.status == "Y" && a.lang == "tw"
                               select a).ToList();

            var searchData4 = (from a in searchData3
                               where a.monthrent == formData4 && a.status == "Y" && a.lang == "tw"
                               select a).ToList();

            if (searchData4 != null)
            {
                if (searchData4.Count() == 1)
                {
                    TempData["searchData"] = searchData4;
                    TempData["singleProject"] = searchData4.FirstOrDefault().guid;
                    TempData["singleProjectTitle"] = "Y";
                }
                else
                {
                    TempData["searchData"] = searchData4;
                }
            }

            TempData["isSearch"] = "Y";
            TempData["carstatus"] = formData1;
            TempData["renttime"] = formData2;
            TempData["brand"] = formData3;
            TempData["monthrent"] = formData4;

            return RedirectToAction("Index", "Home");
        }

        public ActionResult SendProject(string carstatus, string renttime, string brand, string monthrent)
        {
            Model DB = new Model();

            var formData1 = carstatus;
            var formData2 = renttime;
            var formData3 = brand;
            var formData4 = monthrent;
            List<projectsetting> resultPrj = new List<projectsetting>();

            var searchData1 = (from a in DB.projectsetting
                               where a.carstatus == formData1 && a.status == "Y" && a.lang == "tw" && a.indoor == "上架"
                               select a).ToList();

            //var searchData2 = (from a in searchData1
            //                   where a.renttime.Contains(formData2) && a.status == "Y" && a.lang == "tw" && a.indoor == "上架"
            //                   select a).ToList();

            var searchData3 = (from a in searchData1
                               where a.brand == formData3 && a.status == "Y" && a.lang == "tw" && a.indoor == "上架"
                               select a).ToList();

            if (formData4 == "a")
            {
                int isPrj = 0;
                foreach (var item in searchData3)
                {
                    var rentperiod = (from a in DB.rentsetting
                                      where a.title == item.guid && a.rentyear == formData2 && a.status == "Y" && a.lang == "tw"
                                      select a).ToList();

                    if (rentperiod.Count() != 0)
                    {
                        var judgerent = (from a in DB.rentsetting
                                         where a.title == item.guid && a.lang == "tw" && a.status == "Y"
                                         select a).ToList();
                        foreach (var rent in judgerent)
                        {
                            if (Int32.Parse(rent.rentmonth.Replace(",", "")) < 19999)
                            {
                                isPrj = 1;
                            }
                        }
                        if (isPrj == 1)
                        {
                            resultPrj.Add(item);
                        }
                        isPrj = 0;
                    }
                }
            }
            else if (formData4 == "b")
            {
                int isPrj = 0;
                foreach (var item in searchData3)
                {
                    var rentperiod = (from a in DB.rentsetting
                                      where a.title == item.guid && a.rentyear == formData2 && a.status == "Y" && a.lang == "tw"
                                      select a).ToList();

                    if (rentperiod.Count() != 0)
                    {
                        var judgerent = (from a in DB.rentsetting
                                         where a.title == item.guid && a.lang == "tw" && a.status == "Y"
                                         select a).ToList();
                        foreach (var rent in judgerent)
                        {
                            if (Int32.Parse(rent.rentmonth.Replace(",", "")) >= 20000 && Int32.Parse(rent.rentmonth.Replace(",", "")) <= 24999)
                            {
                                isPrj = 1;
                            }
                        }
                        if (isPrj == 1)
                        {
                            resultPrj.Add(item);
                        }
                        isPrj = 0;
                    }
                }
            }
            else if (formData4 == "c")
            {
                int isPrj = 0;
                foreach (var item in searchData3)
                {
                    var rentperiod = (from a in DB.rentsetting
                                      where a.title == item.guid && a.rentyear == formData2 && a.status == "Y" && a.lang == "tw"
                                      select a).ToList();

                    if (rentperiod.Count() != 0)
                    {
                        var judgerent = (from a in DB.rentsetting
                                         where a.title == item.guid && a.lang == "tw" && a.status == "Y"
                                         select a).ToList();
                        foreach (var rent in judgerent)
                        {
                            if (Int32.Parse(rent.rentmonth.Replace(",", "")) >= 25000 && Int32.Parse(rent.rentmonth.Replace(",", "")) <= 29999)
                            {
                                isPrj = 1;
                            }
                        }
                        if (isPrj == 1)
                        {
                            resultPrj.Add(item);
                        }
                        isPrj = 0;
                    }
                }
            }
            else if (formData4 == "d")
            {
                int isPrj = 0;
                foreach (var item in searchData3)
                {
                    var rentperiod = (from a in DB.rentsetting
                                      where a.title == item.guid && a.rentyear == formData2 && a.status == "Y" && a.lang == "tw"
                                      select a).ToList();

                    if (rentperiod.Count() != 0)
                    {
                        var judgerent = (from a in DB.rentsetting
                                         where a.title == item.guid && a.lang == "tw" && a.status == "Y"
                                         select a).ToList();
                        foreach (var rent in judgerent)
                        {
                            if (Int32.Parse(rent.rentmonth.Replace(",", "")) >= 30000)
                            {
                                isPrj = 1;
                            }
                        }
                        if (isPrj == 1)
                        {
                            resultPrj.Add(item);
                        }
                        isPrj = 0;
                    }
                }
            }


            TempData["isSearch"] = "Y";
            TempData["carstatus"] = formData1;
            TempData["renttime"] = formData2;
            TempData["brand"] = formData3;
            TempData["monthrent"] = formData4;

            if (resultPrj != null)
            {
                if (resultPrj.Count() == 1)
                {
                    TempData["searchData"] = resultPrj;
                    TempData["singleProject"] = resultPrj.FirstOrDefault().guid;
                    TempData["singleProjectTitle"] = "Y";
                }
                else
                {
                    TempData["searchData"] = resultPrj;
                }

                return Json(resultPrj, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        [ValidateInput(false)]
        [ValidateAntiForgeryToken]
        public ActionResult RentSend(FormCollection form)
        {
            Model DB = new Model();

            var formData1 = form["rentpj"].ToString();
            var formData2 = form["rentperiod"].ToString();
            var formData3 = form["rentcarmodel"].ToString();
            var formData4 = form["discount"].ToString();

            var searchData1 = (from a in DB.rentsetting
                               where a.title == formData1 && a.status == "Y" && a.lang == "tw"
                               select a).ToList();

            var searchData2 = (from a in searchData1
                               where a.rentyear == formData2 && a.status == "Y" && a.lang == "tw"
                               select a).ToList();

            var searchData3 = (from a in searchData2
                               where a.model == formData3 && a.status == "Y" && a.lang == "tw"
                               select a).ToList();

            var searchData4 = (from a in searchData3
                               where a.pricediscount == formData4 && a.status == "Y" && a.lang == "tw"
                               select a).FirstOrDefault();

            if (searchData4 != null)
            {
                TempData["searchRentPrice"] = searchData4.price;
                TempData["searchRentMargin"] = searchData4.margin;
                TempData["searchRentMonth"] = searchData4.rentmonth;

                TempData["searchRentReBuy"] = Int32.Parse(searchData4.rebuyprice).ToString("###,###");

                var searchRentProject = (from a in DB.projectsetting
                                        where a.guid == searchData4.title && a.status == "Y" && a.lang == "tw"
                                        select a).FirstOrDefault();

                if(searchRentProject != null)
                {
                    var outline = searchRentProject.outline.Split(',');
                    foreach (var iO in outline)
                    {
                        if (iO == "歸還車輛")
                        {
                            TempData["outline1"] = "歸還車輛";
                        }
                        else if (iO == "期滿贖回(可複選)")
                        {
                            TempData["outline2"] = "期滿購回";
                        }
                        else
                        {

                        }
                    }

                    var roundservice1 = searchRentProject.roundservice.Split(',');
                    foreach (var item in roundservice1)
                    {
                        if (item == "領牌費用")
                        {
                            TempData["round11"] = "領牌費用";
                        }
                        else if (item == "牌照燃料稅")
                        {
                            TempData["round12"] = "牌照燃料稅";
                        }
                        else if (item == "強制險")
                        {
                            TempData["round13"] = "強制險";
                        }
                        else if (item == "代步車")
                        {
                            TempData["round14"] = "代步車";
                        }
                        else if (item == "保養維修")
                        {
                            TempData["round15"] = "保養維修";
                            TempData["round15num"] = searchRentProject.mileage;
                        }
                        else
                        {

                        }

                    }

                    TempData["searchRentRound2"] = searchRentProject.roundservicefree;
                }
            }

            TempData["isRent"] = "Y";

            var temp = (from a in DB.projectsetting
                               where a.guid == formData1 && a.status == "Y" && a.lang == "tw"
                               select a).FirstOrDefault();
            //TempData["searchData"] = temp.title;
            //TempData["searchDataG"] = temp.guid;
            TempData["rentpj"] = formData1;
            TempData["rentperiod"] = formData2;
            TempData["rentcarmodel"] = formData3;

            TempData["rentData"] = (from a in DB.rentsetting
                                    where a.pricediscount == formData4 && a.status == "Y" && a.lang == "tw"
                                    select a).ToList();
            TempData["discount"] = formData4;

            return RedirectToAction("Index", "Home");
        }
        public ActionResult SendRentForm(string project, string period,string carmodel,string cardisacount)
        {
            Model DB = new Model();

            var changeperiod = period.Substring(0,1);

            var searchData1 = (from a in DB.rentsetting
                               where a.title == project && a.status == "Y" && a.lang == "tw"
                               select a).ToList();

            var searchData2 = (from a in searchData1
                               where a.rentyear == changeperiod && a.status == "Y" && a.lang == "tw"
                               select a).ToList();

            var searchData3 = (from a in searchData2
                               where a.model == carmodel && a.status == "Y" && a.lang == "tw"
                               select a).ToList();

            var searchData4 = (from a in searchData3
                               where a.pricediscount == cardisacount && a.status == "Y" && a.lang == "tw"
                               select a).FirstOrDefault();

            if (searchData4 != null)
            {
                var outlinetype = (from a in DB.projectsetting
                                   where a.guid == project && a.lang == "tw"
                                   select a).FirstOrDefault();

                var typesplit = outlinetype.outline.Split(',');
                searchData4.model = "";
                //searchData4.margin = Int32.Parse(searchData4.margin).ToString("###,###");
                //searchData4.rentmonth = Int32.Parse(searchData4.rentmonth).ToString("###,###");
                searchData4.margin = "$" + searchData4.margin;
                searchData4.rentmonth = "$" + searchData4.rentmonth;

                searchData4.titlename = "";
                searchData4.model = "";

                foreach (var item in typesplit)
                {
                    if (item != "")
                    {
                        if (item == "歸還車輛")
                        {
                            searchData4.titlename = "歸還車輛";
                        }
                        else if (item.Substring(0, 4) == "期滿購回")
                        {
                            //searchData4.model = "期滿購回 $" + Int32.Parse(searchData4.rebuyprice).ToString("###,###") + " 元";
                            searchData4.model = "期滿購回 $" + searchData4.rebuyprice + " 元";
                        }
                    }
                }

                return Json(searchData4, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult SendSelectProj(string project)
        {
            Model DB = new Model();

            string result = "";
            string resultyear = "";
            var projectname = (from a in DB.projectsetting
                               where a.guid == project && a.status == "Y" && a.lang == "tw"
                               select a).FirstOrDefault();

            var rentmodel = (from a in DB.rentsetting
                             where a.title == projectname.guid && a.status == "Y" && a.lang == "tw"
                             select a.model).Distinct().ToList();
            foreach (var item in rentmodel)
            {
                result = result + item + ",";
            }
            result = result.TrimEnd(',');
            Session["searchDataF"] = projectname.feature;
            ViewBag.htmlFeature = projectname.feature;

            var rentperiod = (from a in DB.rentsetting
                             where a.title == projectname.guid && a.status == "Y" && a.lang == "tw"
                             select a.rentyear).Distinct().ToList();
            foreach (var item in rentperiod)
            {
                resultyear = resultyear + item + "年" + ",";
            }
            resultyear = resultyear.TrimEnd(',');

            projectname.carstatus = result;
            projectname.renttime = resultyear;

            return Json(projectname, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SendSelectPeriod(string project, string period)
        {
            Model DB = new Model();

            string result = "";
            string reperiod = period.Substring(0, 1);

            var rentmodel = (from a in DB.rentsetting
                             where a.title == project && a.rentyear == reperiod && a.status == "Y" && a.lang == "tw"
                             select a.model).Distinct().ToList();
            foreach (var item in rentmodel)
            {
                result = result + item + ",";
            }
            result = result.TrimEnd(',');

            if (rentmodel.Count() == 0)
            {

                return Json("", JsonRequestBehavior.AllowGet);
            }
            else
            {
                rentmodel[0] = result;

                return Json(rentmodel[0], JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SendSelectModel(string project,string model,string period)
        {
            Model DB = new Model();

            string reperiod = period.Substring(0, 1);

            var rentmodel = (from a in DB.rentsetting
                             where a.title == project && a.model == model && a.rentyear == reperiod && a.status == "Y" && a.lang == "tw"
                             select a).ToList();
            if (rentmodel.Count() >1)
            {
                foreach (var item in rentmodel)
                {
                    item.sortIndex = 1;
                }
            }
            else
            {
                foreach (var item in rentmodel)
                {
                    item.sortIndex = 0;
                }
            }

            return Json(rentmodel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendTalk(string project,string period,string model, string price,string discount,string monthprice)
        {
            Model DB = new Model();

            string result = "";
            var projectname = (from a in DB.projectsetting
                               where a.guid == project && a.status == "Y" && a.lang == "tw"
                               select a).FirstOrDefault();
            if (discount == "0" || discount == "" || discount == null)
            {
                result = result + projectname.title + "  " + period + model + "  車價  " + Int32.Parse(price).ToString("###,###") + "  車價折扣  " + discount + "  月租金  " + monthprice;
            }
            else
            {
                result = result + projectname.title + "  " + period + model + "  車價  " + Int32.Parse(price.Replace(",", "")).ToString("###,###") + "  車價折扣  " + Int32.Parse(discount.Replace(",", "")).ToString("###,###") + "  月租金  " + monthprice;
            }

            return Json(result,JsonRequestBehavior.AllowGet);
        }

        public ActionResult SendAdvanceForm(string formname, string formemail, string formphone, int formadvance, string country, string city,string activeproject,string requireNote,string vcode)
        {
            Model DB = new Model();

            var cCode = Session["_ValCheckCode"].ToString();
            if (vcode == cCode)
            {
                var projectname = (from a in DB.projectsetting
                                   where a.guid == activeproject
                                   select a).FirstOrDefault();

                //advanceform form = new advanceform();
                //form.guid = Guid.NewGuid().ToString();
                //var addGuid = form.guid;
                //form.title = formname;
                //form.email = formemail;
                //form.phone = formphone;
                //form.contactmethod = formadvance;
                //form.country = country;
                //form.city = city;
                //form.project = projectname.title;
                //form.requirenote = requireNote;
                //form.create_date = DateTime.Now;
                //form.status = "N";
                //form.lang = "tw";

                //DB.advanceform.Add(form);
                //DB.SaveChanges();

                string encrypt_key1 = "LEXUSADMSYS2020";
                string str1 = "";
                string str2 = "";
                string str3 = "";
                try
                {
                    AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
                    MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                    SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();
                    byte[] key = sha256.ComputeHash(Encoding.UTF8.GetBytes(encrypt_key1));
                    byte[] iv = md5.ComputeHash(Encoding.UTF8.GetBytes(encrypt_key1));
                    aes.Key = key;
                    aes.IV = iv;

                    byte[] dataByteArray1 = Encoding.UTF8.GetBytes(formname);
                    byte[] dataByteArray2 = Encoding.UTF8.GetBytes(formemail);
                    byte[] dataByteArray3 = Encoding.UTF8.GetBytes(formphone);
                    using (MemoryStream ms = new MemoryStream())
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(dataByteArray1, 0, dataByteArray1.Length);
                        cs.FlushFinalBlock();
                        str1 = Convert.ToBase64String(ms.ToArray());
                    }

                    using (MemoryStream ms = new MemoryStream())
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(dataByteArray2, 0, dataByteArray2.Length);
                        cs.FlushFinalBlock();
                        str2 = Convert.ToBase64String(ms.ToArray());
                    }

                    using (MemoryStream ms = new MemoryStream())
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(dataByteArray3, 0, dataByteArray3.Length);
                        cs.FlushFinalBlock();
                        str3 = Convert.ToBase64String(ms.ToArray());
                    }
                }
                catch (Exception exception1)
                {
                    Exception exception = exception1;
                    str1 = "ERR";
                    str2 = "ERR";
                    str3 = "ERR";
                }

                MLCONTACTUS form = new MLCONTACTUS();

                form.NAME = str1;
                form.EMLE = str2;
                form.MOBILEE = str3;
                form.QUESTION = "99";
                form.REPLAYTP = formadvance;
                form.TOWN = city;
                form.CITY = country;
                form.OVERYEAR = 0;
                form.SOURCE = 4;
                form.PROJECT = projectname.title;
                form.CONTENT = requireNote;
                form.CDATE = DateTime.Now;
                form.STATUS = 0;

                DB.MLCONTACTUS.Add(form);
                DB.SaveChanges();

                try
                {
                    var t = DB.web_data.Where(x => x.guid == "1").FirstOrDefault();

                    var smtpdata = DB.smtp_data.SingleOrDefault();

                    //設定smtp主機
                    string smtpAddress = smtpdata.host;
                    //設定Port
                    int portNumber = Int32.Parse(smtpdata.port);
                    bool enableSSL = true;
                    //SSL是否啟用
                    enableSSL = (smtpdata.smtp_auth == "Y" ? true : false);
                    //填入寄送方email和password
                    string emailFrom = smtpdata.username;
                    string password = smtpdata.thepr;
                    //收信方email 可以用逗號區分多個收件人
                    string emailTo = t.servicephone1+","+t.company;
                    string subject = "【和運租車】近一步洽詢";

                    string subjectTitle = "";
                    string metaFile = "";
                    subjectTitle = "洽詢表單";
                    metaFile = "Contact_tw.html";

                    string sMessage = System.IO.File.ReadAllText(Server.MapPath("~/Content/Mail") + "\\" + metaFile);
                    sMessage = sMessage.Replace("{$web_title}", "和運租車");
                    sMessage = sMessage.Replace("{$web_url}", "http://" + Request.ServerVariables["Server_Name"]);
                    sMessage = sMessage.Replace("{$use_title}", subjectTitle);

                    sMessage = sMessage.Replace("{$name}", formname);
                    sMessage = sMessage.Replace("{$phone}", formphone);
                    sMessage = sMessage.Replace("{$email}", formemail);
                    sMessage = sMessage.Replace("{$city}", country);
                    sMessage = sMessage.Replace("{$dist}", city);
                    if (formadvance == 0)
                    {
                        sMessage = sMessage.Replace("{$advancemethod}", "行動電話");
                    }
                    else if (formadvance == 1)
                    {
                        sMessage = sMessage.Replace("{$advancemethod}", "EMAIL");
                    }
                    else
                    {
                        sMessage = sMessage.Replace("{$advancemethod}", "");
                    }
                    sMessage = sMessage.Replace("{$activeproject}", projectname.title);
                    sMessage = sMessage.Replace("{$requireNote}", requireNote);

                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress(emailFrom);
                        mail.To.Add(emailTo);
                        mail.Subject = subject;
                        mail.Body = sMessage;
                        // 若你的內容是HTML格式，則為True
                        mail.IsBodyHtml = true;

                        using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                        {
                            smtp.Credentials = new NetworkCredential(emailFrom, password);
                            smtp.EnableSsl = enableSSL;
                            smtp.Send(mail);
                        }
                    }
                }
                catch (Exception ex)
                {
                    var error = ex;
                }

                return Json("Y", JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("N", JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult Export(string data)
        {
            Model DB = new Model();

            var splitdata = data.Split(',');

            IWorkbook wb = new XSSFWorkbook();
            ISheet ws = wb.CreateSheet("Class");

            //Style 置中
            ICellStyle style = wb.CreateCellStyle();
            style.Alignment = HorizontalAlignment.Center;
            style.VerticalAlignment = VerticalAlignment.Center;
            List<advanceform> resultlist = new List<advanceform>();
            List<rentsetting> resultlist2 = new List<rentsetting>();

            foreach (var item in splitdata)
            {
                var list = (from a in DB.advanceform
                            where a.guid == item && a.lang == "tw"
                            orderby a.create_date descending
                            select a).FirstOrDefault();
                if (list != null)
                {
                    resultlist.Add(list);
                }
            }
            if (resultlist.Count() == 0)
            {

                foreach (var item in splitdata)
                {
                    var list = (from a in DB.rentsetting
                                where a.guid == item && a.lang == "tw"
                                orderby a.create_date descending
                                select a).FirstOrDefault();
                    if (list != null)
                    {
                        resultlist2.Add(list);
                    }
                }

                ws.CreateRow(0); //第一行為欄位名稱
                ws.GetRow(0).CreateCell(0).SetCellValue("專案名稱");
                ws.GetRow(0).CreateCell(1).SetCellValue("車型");
                ws.GetRow(0).CreateCell(2).SetCellValue("車價");
                ws.GetRow(0).CreateCell(3).SetCellValue("車價折扣");
                ws.GetRow(0).CreateCell(4).SetCellValue("租期(年)");
                ws.GetRow(0).CreateCell(5).SetCellValue("月租金");
                ws.GetRow(0).CreateCell(6).SetCellValue("保證金");
                ws.GetRow(0).CreateCell(7).SetCellValue("購回價");
                ws.GetRow(0).CreateCell(8).SetCellValue("排序");
                ws.GetRow(0).CreateCell(9).SetCellValue("狀態");

                ws.GetRow(0).GetCell(0).CellStyle = style;
                ws.GetRow(0).GetCell(1).CellStyle = style;
                ws.GetRow(0).GetCell(2).CellStyle = style;
                ws.GetRow(0).GetCell(3).CellStyle = style;
                ws.GetRow(0).GetCell(4).CellStyle = style;
                ws.GetRow(0).GetCell(5).CellStyle = style;
                ws.GetRow(0).GetCell(6).CellStyle = style;
                ws.GetRow(0).GetCell(7).CellStyle = style;
                ws.GetRow(0).GetCell(8).CellStyle = style;
                ws.GetRow(0).GetCell(9).CellStyle = style;

                int r = 1;
                foreach (var item in resultlist2)
                {
                    var pjname = (from a in DB.projectsetting
                                where a.guid == item.title && a.lang == "tw"
                                select a.title).FirstOrDefault();
                    ws.CreateRow(r);
                    ws.GetRow(r).CreateCell(0).SetCellValue(pjname);
                    ws.GetRow(r).CreateCell(1).SetCellValue(item.model);
                    ws.GetRow(r).CreateCell(2).SetCellValue(item.price);
                    ws.GetRow(r).CreateCell(3).SetCellValue(item.pricediscount);
                    ws.GetRow(r).CreateCell(4).SetCellValue(item.rentyear);
                    ws.GetRow(r).CreateCell(5).SetCellValue(item.rentmonth);
                    ws.GetRow(r).CreateCell(6).SetCellValue(item.margin);
                    ws.GetRow(r).CreateCell(7).SetCellValue(item.rebuyprice);
                    ws.GetRow(r).CreateCell(8).SetCellValue(item.sortIndex.ToString());
                    switch (item.status)
                    {
                        case "Y":
                            ws.GetRow(r).CreateCell(9).SetCellValue("啟用");
                            break;
                        case "N":
                            ws.GetRow(r).CreateCell(9).SetCellValue("停用");
                            break;
                    }

                    //Style
                    ws.GetRow(r).GetCell(0).CellStyle = style;
                    ws.GetRow(r).GetCell(1).CellStyle = style;
                    ws.GetRow(r).GetCell(2).CellStyle = style;
                    ws.GetRow(r).GetCell(3).CellStyle = style;
                    ws.GetRow(r).GetCell(4).CellStyle = style;
                    ws.GetRow(r).GetCell(5).CellStyle = style;
                    ws.GetRow(r).GetCell(6).CellStyle = style;
                    ws.GetRow(r).GetCell(7).CellStyle = style;
                    ws.GetRow(r).GetCell(8).CellStyle = style;
                    ws.GetRow(r).GetCell(9).CellStyle = style;

                    //自動調寬度
                    ws.AutoSizeColumn(0);
                    ws.AutoSizeColumn(1);
                    ws.AutoSizeColumn(2);
                    ws.AutoSizeColumn(3);
                    ws.AutoSizeColumn(4);
                    ws.AutoSizeColumn(5);
                    ws.AutoSizeColumn(6);
                    ws.AutoSizeColumn(7);
                    ws.AutoSizeColumn(8);
                    ws.AutoSizeColumn(9);
                    r++;
                }

                MemoryStream ms = new MemoryStream();
                wb.Write(ms);

                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", "租金資料.xlsx"));
                Response.BinaryWrite(ms.ToArray());
                Response.Flush();
                Response.End();
            }
            else
            {
                ws.CreateRow(0); //第一行為欄位名稱
                ws.GetRow(0).CreateCell(0).SetCellValue("姓名");
                ws.GetRow(0).CreateCell(1).SetCellValue("Email");
                ws.GetRow(0).CreateCell(2).SetCellValue("行動電話");
                ws.GetRow(0).CreateCell(3).SetCellValue("優先聯絡方式");
                ws.GetRow(0).CreateCell(4).SetCellValue("縣市");
                ws.GetRow(0).CreateCell(5).SetCellValue("鄉鎮市區");
                ws.GetRow(0).CreateCell(6).SetCellValue("可搭配專案");
                ws.GetRow(0).CreateCell(7).SetCellValue("需求簡述");
                ws.GetRow(0).CreateCell(8).SetCellValue("建立時間");
                ws.GetRow(0).CreateCell(9).SetCellValue("狀態");

                ws.GetRow(0).GetCell(0).CellStyle = style;
                ws.GetRow(0).GetCell(1).CellStyle = style;
                ws.GetRow(0).GetCell(2).CellStyle = style;
                ws.GetRow(0).GetCell(3).CellStyle = style;
                ws.GetRow(0).GetCell(4).CellStyle = style;
                ws.GetRow(0).GetCell(5).CellStyle = style;
                ws.GetRow(0).GetCell(6).CellStyle = style;
                ws.GetRow(0).GetCell(7).CellStyle = style;
                ws.GetRow(0).GetCell(8).CellStyle = style;
                ws.GetRow(0).GetCell(9).CellStyle = style;

                int r = 1;
                foreach (var item in resultlist)
                {
                    ws.CreateRow(r);
                    ws.GetRow(r).CreateCell(0).SetCellValue(item.title);
                    ws.GetRow(r).CreateCell(1).SetCellValue(item.email);
                    ws.GetRow(r).CreateCell(2).SetCellValue(item.phone);
                    ws.GetRow(r).CreateCell(3).SetCellValue(item.contactmethod);
                    ws.GetRow(r).CreateCell(4).SetCellValue(item.country);
                    ws.GetRow(r).CreateCell(5).SetCellValue(item.city);
                    ws.GetRow(r).CreateCell(6).SetCellValue(item.project);
                    ws.GetRow(r).CreateCell(7).SetCellValue(item.requirenote);
                    ws.GetRow(r).CreateCell(8).SetCellValue(item.create_date.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                    switch (item.status)
                    {
                        case "Y":
                            ws.GetRow(r).CreateCell(9).SetCellValue("已回復");
                            break;
                        case "N":
                            ws.GetRow(r).CreateCell(9).SetCellValue("未回復");
                            break;
                    }

                    //Style
                    ws.GetRow(r).GetCell(0).CellStyle = style;
                    ws.GetRow(r).GetCell(1).CellStyle = style;
                    ws.GetRow(r).GetCell(2).CellStyle = style;
                    ws.GetRow(r).GetCell(3).CellStyle = style;
                    ws.GetRow(r).GetCell(4).CellStyle = style;
                    ws.GetRow(r).GetCell(5).CellStyle = style;
                    ws.GetRow(r).GetCell(6).CellStyle = style;
                    ws.GetRow(r).GetCell(7).CellStyle = style;
                    ws.GetRow(r).GetCell(8).CellStyle = style;
                    ws.GetRow(r).GetCell(9).CellStyle = style;

                    //自動調寬度
                    ws.AutoSizeColumn(0);
                    ws.AutoSizeColumn(1);
                    ws.AutoSizeColumn(2);
                    ws.AutoSizeColumn(3);
                    ws.AutoSizeColumn(4);
                    ws.AutoSizeColumn(5);
                    ws.AutoSizeColumn(6);
                    ws.AutoSizeColumn(7);
                    ws.AutoSizeColumn(8);
                    ws.AutoSizeColumn(9);
                    r++;
                }

                MemoryStream ms = new MemoryStream();
                wb.Write(ms);

                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", "洽詢資料.xlsx"));
                Response.BinaryWrite(ms.ToArray());
                Response.Flush();
                Response.End();
            }


            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}