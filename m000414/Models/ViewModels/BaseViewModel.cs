﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Web.Models.EntityModel;

namespace Web.Models.ViewModels
{
    public enum LanguageEnum
    {
        [Description("zh-TW")]
        Chinese = 1,

        [Description("en-US")]
        English = 2,
    }
    public class rentModel
    {
        public string guid { get; set; }
        public string title { get; set; }
        public string code { get; set; }
    }
    public class BaseViewModel
    {
       public List<string> Footer_Pic { get; set; }
        public List<footer_pic_alt> FooterPicAlt { get; set; }
    }

    public class footer_pic_alt
    {
        public string pic { get; set; }
        public string pic_alt { get; set; }
    }
    public class ajaxClass
    {
        private string val;

        public string GetVal()
        {
            return val;
        }

        public void SetVal(string value)
        {
            val = value;
        }

        private string func;

        public string GetFunc()
        {
            return func;
        }

        public void SetFunc(string value)
        {
            func = value;
        }
    }

    public class ajaxClassN
    {
        //private string val;
        //private string func;
        //public string Val
        //{
        //    get { return val; }
        //    set { val = value; }
        //}
        //public string Func
        //{
        //    get { return func; }
        //    set { func = value; }
        //}

        [Editable(false)]
        public string Val { get; set; }
        [Editable(false)]
        public string Func { get; set; }
    }
}