﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class GaModel
    {
        public string source { get; set; }
        public int? count { get; set; }

    }

    public class GaMonthModel
    {
        public string age { get; set; }
        public string visits { get; set; }

    }
}