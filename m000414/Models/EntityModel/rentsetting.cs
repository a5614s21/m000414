﻿namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class rentsetting
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }
        public string titlename { get; set; }
        public string model { get; set; }
        public string price { get; set; }
        public string pricediscount { get; set; }
        public string rentyear { get; set; }
        public string rentmonth { get; set; }
        public string margin { get; set; }
        public string rebuyprice { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        [StringLength(30)]
        public string lang { get; set; }
        public int? sortIndex { get; set; }
    }
}