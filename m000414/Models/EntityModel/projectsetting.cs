﻿namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class projectsetting
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }
        public string indoor { get; set; }
        public string sortNum { get; set; }
        public string carstatusname { get; set; }
        public string brandname { get; set; }
        public string statusname { get; set; }
        public string carstatus { get; set; }
        public string brand { get; set; }
        public string monthrent { get; set; }
        public string renttime { get; set; }
        public string roundservice { get; set; }
        public string roundservicefree { get; set; }
        public string mileage { get; set; }
        public string outline { get; set; }
        public string url { get; set; }
        public string feature { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        [StringLength(30)]
        public string lang { get; set; }
    }
}