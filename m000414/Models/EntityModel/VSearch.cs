﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Web.Models.EntityModel
{
    [Table("V_Search")]
    public class VSearch
    {
        [Key]      
        public int id { get; set; }

        public string guid { get; set; }

        public string title { get; set; }

        public string content { get; set; }

        public string status { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }
             
        public string lang { get; set; }

        public string tname { get; set; }

        public string url { get; set; }

        public string path { get; set; }

    }
}