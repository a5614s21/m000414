﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security;
using System.Web;

namespace Web.Models.EntityModel
{
    public class changepw
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string username { get; set; }

        public string thepr { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }
    }
}