namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class taiwan_city
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }


        public string city { get; set; }

        public string district { get; set; }

        public string zip { get; set; }

        public string lay { get; set; }

        public int? sortIndex { get; set; }


        [StringLength(30)]
        public string lang { get; set; }
    }
}
