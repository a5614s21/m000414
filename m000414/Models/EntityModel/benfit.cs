﻿namespace Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class benfit
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }
        public string titlecontent { get; set; }
        public string contentone { get; set; }
        public string contenttwo { get; set; }
        public string contentthree { get; set; }
        public string contentfour { get; set; }
        public string content { get; set; }
        public string videolinkone { get; set; }
        public string videolinktwo { get; set; }
        public string videolinkthree { get; set; }
        public string videolinkfour { get; set; }

        public DateTime? create_date { get; set; }

        public DateTime? modifydate { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        [StringLength(30)]
        public string lang { get; set; }
        public string pic { get; set; }

        public string pic_alt { get; set; }
        public string pictwo { get; set; }

        public string pictwo_alt { get; set; }
        public string picthree { get; set; }

        public string picthree_alt { get; set; }
        public string picfour { get; set; }

        public string picfour_alt { get; set; }
    }
}