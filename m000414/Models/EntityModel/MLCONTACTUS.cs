﻿namespace Web.Models.EntityModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    public class MLCONTACTUS
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SNO { get; set; }

        public string NAME { get; set; }
        public string EMLE { get; set; }
        public string MOBILEE { get; set; }
        public string QUESTION { get; set; }
        public string TOWN { get; set; }
        public string CITY { get; set; }
        public int? REPLAYTP { get; set; }
        public string CONTENT { get; set; }
        public string PROJECT { get; set; }

        public DateTime? UDATE { get; set; }

        public DateTime? CDATE { get; set; }
        public int? OVERYEAR { get; set; }
        public int? SOURCE { get; set; }
        public int? STATUS { get; set; }
    }
}