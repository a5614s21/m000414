﻿namespace Web.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using System.Linq;
    using System.Net;
    using Web.Models.EntityModel;

    public class Model : DbContext
    {
        // 您的內容已設定為使用應用程式組態檔 (App.config 或 Web.config)
        // 中的 'Model' 連接字串。根據預設，這個連接字串的目標是
        // 您的 LocalDb 執行個體上的 'Web.Models.Model' 資料庫。
        //
        // 如果您的目標是其他資料庫和 (或) 提供者，請修改
        // 應用程式組態檔中的 'Model' 連接字串。
        public Model()
            : base("name=Model")
        {
        }

        //最新消息類別
        public virtual DbSet<news_category> news_category { get; set; }

        //最新消息內容
        public virtual DbSet<news> news { get; set; }

        #region -- 控制台 --

        //網站基本資訊
        public virtual DbSet<web_data> web_data { get; set; }

        //SMTP設定
        public virtual DbSet<smtp_data> smtp_data { get; set; }

        //資源回收桶
        public virtual DbSet<ashcan> ashcan { get; set; }

        //防火牆設定
        public virtual DbSet<firewalls> firewalls { get; set; }

        //系統日誌
        public virtual DbSet<system_log> system_log { get; set; }

        #endregion

        #region -- 帳戶資訊 --

        //帳戶管理
        public virtual DbSet<user> user { get; set; }

        //群組管理
        public virtual DbSet<roles> roles { get; set; }

        //帳戶對應群組
        public virtual DbSet<user_role> user_role { get; set; }

        //群組對應權限
        public virtual DbSet<role_permissions> role_permissions { get; set; }

        #endregion

        #region -- 其他設定 --

        //語系
        public virtual DbSet<language> language { get; set; }

        //系統設定
        public virtual DbSet<system_data> system_data { get; set; }

        //台灣縣市區
        public virtual DbSet<taiwan_city> taiwan_city { get; set; }

        //系統列表
        public virtual DbSet<system_menu> system_menu { get; set; }

        public virtual DbSet<mail_contents> mail_contents { get; set; }
        public virtual DbSet<homebanner> homebanner { get; set; }
        public virtual DbSet<videocontent> videocontent { get; set; }
        public virtual DbSet<benfit> benfit { get; set; }
        public virtual DbSet<rentbenfit> rentbenfit { get; set; }
        public virtual DbSet<contact_service> contact_service { get; set; }
        public virtual DbSet<mulitpleselect> multipleselect { get; set; }
        public virtual DbSet<comparemoney> comparemoney { get; set; }
        public virtual DbSet<comparetime> comparetime { get; set; }
        public virtual DbSet<projectsetting> projectsetting { get; set; }
        public virtual DbSet<rentsetting> rentsetting { get; set; }
        public virtual DbSet<car_status> car_status { get; set; }
        public virtual DbSet<brand_category> brand_category { get; set; }
        public virtual DbSet<monthrent_category> monthrent_category { get; set; }
        public virtual DbSet<renttime> renttime { get; set; }
        public virtual DbSet<carmodel> carmodel { get; set; }
        public virtual DbSet<advanceform> advanceform { get; set; }
        public virtual DbSet<membershare> membershare { get; set; }
        public virtual DbSet<countrycity> countrycity { get; set; }
        public virtual DbSet<MLCONTACTUS> MLCONTACTUS { get; set; }

        public virtual DbSet<changepw> changepw { get; set; }

        #endregion

        public virtual DbSet<VSearch> Vsearch { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new FooViewConfiguration());
        }

        // 針對您要包含在模型中的每種實體類型新增 DbSet。如需有關設定和使用
        // Code First 模型的詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=390109。
    }

    public class FooViewConfiguration : EntityTypeConfiguration<VSearch>
    {
        public FooViewConfiguration()
        {
            this.HasKey(t => t.id);
            this.ToTable("VSearch");
        }
    }
}