﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;

namespace Web.Repository
{
    public class news__Repository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '標題','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("subtitle", "[{'subject': '描述','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("category", "[{'subject': '所屬分類','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'news_category'}]");
            main.Add("content", "[{'subject': '內容介紹','type': 'editor','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("link", "[{'subject': '連結網址','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            media.Add("pic", "[{'subject': '內頁圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高300 x 225 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'N','useLang':'Y'}]");
            //media.Add("video", "[{'subject': '影片上傳','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','filetype': 'video/mp4','multiple': 'N','useLang':'Y'}]");

            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("startdate", "[{'subject': '發佈日期','type': 'dates','defaultVal': '" + DateTime.Now.ToString("yyyy/MM/dd") + "','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果需要在未來期限到後自動上架刊登，請在此設定一個日期</small>','useLang':'N'}]");
            other.Add("enddate", "[{'subject': '下架日期','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果需要在未來期限到後自動上架刊登，請在此設定一個日期</small>','useLang':'N'}]");
            other.Add("sticky", "[{'subject': '置頂顯示','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">啟用此選項文章將被優先排序到最前面</small>','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("index_sticky", "[{'subject': '首頁置頂','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">啟用此選項文章將被優先排序到首頁</small>','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("seo_keywords", "[{'subject': 'SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            other.Add("seo_description", "[{'subject': 'SEO描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;
        }

        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("pic", "圖片");
            re.Add("info", "內容摘要");
            re.Add("category", "分類");
            re.Add("startdate", "發佈日期");
            re.Add("enddate", "下架日期");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "startdate");
            re.Add("orderByType", "desc");

            return re;
        }
    }

}