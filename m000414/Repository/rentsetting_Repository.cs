﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Models.EntityModel;

namespace Web.Repository
{
    public class rentsetting_Repository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            //main.Add("title", "[{'subject': '專案名稱','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'project_title'}]");
            main.Add("title", "[{'subject': '專案名稱','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'project_title_all'}]");
            main.Add("model", "[{'subject': '車型','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("price", "[{'subject': '車價','type': 'number','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            main.Add("pricediscount", "[{'subject': '車價折扣','type': 'number','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("rentyear", "[{'subject': '租期(年)','type': 'select','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("rentmonth", "[{'subject': '月租金','type': 'number','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("margin", "[{'subject': '保證金','type': 'number','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("rebuyprice", "[{'subject': '購回價','type': 'number','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();
            other.Add("start_date", "[{'subject': '生效日期','type': 'dates','defaultVal': '" + DateTime.Now.ToString("yyyy/MM/dd") + "','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如果需要在未來期限到後自動上架刊登，請在此設定一個日期</small>','useLang':'N'}]");

            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            other.Add("sortIndex", "[{'subject': '標題','type': 'sortIndex','defaultVal': '1','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");
            //other.Add("verify", "[{'subject': '審核狀態','type': 'verify','defaultVal': 'E','classVal': 'col-lg-10','required': '','notes': '','data':'尚未審核/通過/退件','Val':'E/Y/N','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("titlename", "專案名稱");
            re.Add("model", "車型");
            re.Add("price", "車價");
            re.Add("pricediscount", "車價折扣");
            //re.Add("setprice", "車價");
            //re.Add("setpricediscount", "車價折扣");
            re.Add("rentyear", "租期(年)");
            re.Add("rentmonth", "月租金");
            re.Add("margin", "保證金");
            re.Add("rebuyprice", "購回價");
            //re.Add("setrentmonth", "月租金");
            //re.Add("setmargin", "保證金");
            //re.Add("setrebuyprice", "購回價");
            re.Add("sortIndex", "排序");
            re.Add("status", "狀態");
            //re.Add("verify", "審核狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortIndex");
            re.Add("orderByType", "asc");

            return re;
        }
    }
}