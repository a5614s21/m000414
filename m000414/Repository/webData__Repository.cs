﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Models.EntityModel;

namespace Web.Repository
{
    public class webData__Repository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();
            main.Add("title", "[{'subject': '網站名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("url", "[{'subject': '網站網址','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            
            main.Add("phone", "[{'subject': '電話','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("address", "[{'subject': '地址','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("company", "[{'subject': '客服信箱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("servicephone1", "[{'subject': '負責人信箱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">如有多組信箱，請以逗號區隔</small>','useLang':'Y'}]");

            //main.Add("appealphone", "[{'subject': '免費申訴電話','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("service_date", "[{'subject': '服務時間','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("browsercontent", "[{'subject': '瀏覽器建議','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            //main.Add("servicephone1", "[{'subject': '客服專線1','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("servicephone2", "[{'subject': '客服專線2','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("line", "[{'subject': 'LINE','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("facebook", "[{'subject': 'FaceBook','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            //media.Add("pic", "[{'subject': '認證圖片','type': 'fileUpload','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">建議寬高155 x 55 (px)、43 x 55 (px)</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'Y'}]");
            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();
            other.Add("seo_keywords", "[{'subject': 'SEO關鍵字','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            other.Add("seo_description", "[{'subject': 'SEO描述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            other.Add("code", "[{'subject': '追蹤碼','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;
        }
    }

    public class VSearch__Service
    {
        private Model _context = new Model();


        public List<VSearch> GetAllDataBykeyword(string lang, string keyword)
        {

            try
            {
                if(!string.IsNullOrEmpty(keyword))
                {
                    var result = _context.Vsearch.Where(x => x.lang == lang && x.status == "Y" && (x.title.Contains(keyword) || x.content.Contains(keyword))).ToList();
                    return result;
                }

                return null;
                
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}