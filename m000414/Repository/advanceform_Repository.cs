﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Repository
{
    public class advanceform_Repository
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();
            main.Add("title", "[{'subject': '姓名','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("email", "[{'subject': 'E-mail','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("phone", "[{'subject': '行動電話','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("contactmethod", "[{'subject': '優先聯絡方式','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("country", "[{'subject': '縣/市','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("city", "[{'subject': '鄉/鎮/市區','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("project", "[{'subject': '可搭配專案','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'Y'}]");
            main.Add("requirenote", "[{'subject': '需求簡述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();
            other.Add("status", "[{'subject': 'Status','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '','data':'已回復/未回覆','Val':'Y/N','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);
            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("title", "姓名");
            re.Add("country", "縣市");
            re.Add("city", "區域");
            re.Add("phone", "手機");
            re.Add("email", "E-mail");
            re.Add("contactmethod", "優先聯絡方式");
            re.Add("project", "專案");
            re.Add("requirenote", "需求概述");
            re.Add("create_date", "留言時間");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }
        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "create_date");
            re.Add("orderByType", "desc");

            return re;
        }
    }
}