﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Models.EntityModel;

namespace Web.Repository
{
    public class user__Repository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "N";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("username", "[{'subject': '使用者名稱(登入帳號)','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\"></small>',}]");
            main.Add("thepr", "[{'subject': '登入密碼','type': 'password','defaultVal': '','class': 'col-lg-10','required': 'required','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請再次輸入密碼!</small>',}]");
            main.Add("role_guid", "[{'subject': '帳戶群組','type': 'select','defaultVal': '','classVal': 'col-lg-10','required': 'required','notes': '','inherit':'roles' }]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 帳戶詳細資料

            Dictionary<String, Object> userCol = new Dictionary<string, object>();

            userCol.Add("name", "[{'subject': '姓名 (暱稱)','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            userCol.Add("email", "[{'subject': 'Email','type': 'email','default': '','class': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            userCol.Add("phone", "[{'subject': '聯絡電話','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            userCol.Add("mobile", "[{'subject': '行動電話','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            userCol.Add("address", "[{'subject': '通訊住址','type': 'text','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            //other.Add("change_date", "[{'subject': '到期時間','type': 'dates','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">設定到期時間後將不可再登入</small>'}]");
            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N'}]");
            other.Add("first", "[{'subject': '第一次登入','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");
            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("user", userCol);
            return fromData;
        }

        public static Dictionary<String, Object> EditcolFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("username", "[{'subject': '使用者名稱','type': 'account','defaultVal': '','classVal': 'col-lg-10','required': 'required','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\"></small>',}]");
            main.Add("thepr", "[{'subject': '登入密碼','type': 'password','defaultVal': '','class': 'col-lg-10','required': 'required','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">請再次輸入密碼!</small>',}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 帳戶詳細資料

            Dictionary<String, Object> userCol = new Dictionary<string, object>();

            userCol.Add("name", "[{'subject': '姓名 (暱稱)','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            userCol.Add("email", "[{'subject': 'Email','type': 'email','default': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            userCol.Add("phone", "[{'subject': '聯絡電話','type': 'text','default': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            userCol.Add("mobile", "[{'subject': '行動電話','type': 'text','default': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            userCol.Add("address", "[{'subject': '通訊住址','type': 'text','default': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '',}]");
            userCol.Add("first", "[{'subject': '第一次登入','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','notes': '','useLang':'N'}]");
            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();
            
            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("role_guid", "[{'subject': 'lang','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            hidden.Add("lockout", "[{'subject': 'lang','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            hidden.Add("status", "[{'subject': 'lang','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("user", userCol);
            fromData.Add("hidden", hidden);
            return fromData;
        }

        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("username", "使用者帳號");
            re.Add("name", "姓名");
            re.Add("pluralCategory", "帳戶群組");
            re.Add("logindate", "上次登入");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }
    }


    public class User__Service
    {
        private Model _context = new Model();

        //鎖定帳號
        public void Lock(string username)
        {
            try
            {
                var user = _context.user.Where(x => x.username == username).FirstOrDefault();
                user.status = "N";

                _context.Entry(user).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();

            }
            catch
            {
                
            }
        }

        public string CheckFirstLog_Ninety(string username)
        {
            try
            {
                var data = _context.user.Where(x => x.username == username).SingleOrDefault();
                if(data != null)
                {
                    //檢查是否第一次登入
                    if(string.IsNullOrEmpty(data.first))
                    {
                        return "First";
                    }

                    //檢查是否超過90未變更密碼
                    var time = DateTime.Now.AddDays(90);
                    if(data.change_date >= time)
                    {
                        return "Ninety";
                    }

                    return "None";
                }
                else
                {
                    return "Error";
                }
            }
            catch(Exception ex)
            {
                return "Error";
            }
        }
        
    }

    public class changepw_Service
    {
        private Model _context = new Model();

        public void Insert(string username, string password)
        {
            try
            {
                var data = _context.changepw.Where(x => x.username == username).FirstOrDefault();
                if(data != null)
                {
                    SecureString securePwd_db = new SecureString();
                    foreach (var item in data.thepr)
                    {
                        securePwd_db.AppendChar(item);
                    }

                    var convertSecureString_db = Marshal.SecureStringToGlobalAllocUnicode(securePwd_db);
                    var resultSecureString_db = Marshal.PtrToStringUni(convertSecureString_db);

                    var pw_data = JsonConvert.DeserializeObject<List<string>>(resultSecureString_db);
                    pw_data.Add(password);

                    Dictionary<int, string> D = new Dictionary<int, string>();
                    var num = 1;
                    foreach(var item in pw_data)
                    {
                        D.Add(num, item);
                        num++;
                    }

                    var list = D.OrderByDescending(x => x.Key).Select(x => x.Value).Take(3).ToList();
                    var list_json = JsonConvert.SerializeObject(list);

                    SecureString securePwd = new SecureString();
                    foreach (var item in list_json)
                    {
                        securePwd.AppendChar(item);
                    }

                    var convertSecureString = Marshal.SecureStringToGlobalAllocUnicode(securePwd);
                    var resultSecureString = Marshal.PtrToStringUni(convertSecureString);

                    data.thepr = resultSecureString;
                    data.modifydate = DateTime.Now;

                    _context.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    _context.SaveChanges();
                }
                else
                {
                    List<string> pw = new List<string>();
                    pw.Add(password);
                    var pw_json = JsonConvert.SerializeObject(pw);

                    SecureString securePwd2 = new SecureString();
                    foreach (var item in pw_json)
                    {
                        securePwd2.AppendChar(item);
                    }

                    var convertSecureString = Marshal.SecureStringToGlobalAllocUnicode(securePwd2);
                    var resultSecureString = Marshal.PtrToStringUni(convertSecureString);

                    changepw cpw = new changepw();
                    cpw.guid = Guid.NewGuid().ToString();
                    cpw.username = username;
                    cpw.thepr = resultSecureString;
                    cpw.create_date = DateTime.Now;
                    cpw.modifydate = DateTime.Now;

                    _context.Entry(cpw).State = System.Data.Entity.EntityState.Added;
                    _context.SaveChanges();                    
                }

            }
            catch(Exception ex)
            {
                
            }
        }
    }
}