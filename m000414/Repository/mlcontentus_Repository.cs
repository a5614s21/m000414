﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Repository
{
    public class mlcontentus_Repository
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "N";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();
            main.Add("NAME", "[{'subject': '姓名','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("EMLE", "[{'subject': 'E-mail','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("MOBILEE", "[{'subject': '行動電話','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("QUESTION", "[{'subject': '優先聯絡方式','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("CITY", "[{'subject': '縣/市','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("TOWN", "[{'subject': '鄉/鎮/市區','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("PROJECT", "[{'subject': '可搭配專案','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("CONTENT", "[{'subject': '需求簡述','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'N'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();
            other.Add("STATUS", "[{'subject': 'Status','type': 'radio','defaultVal': '2','classVal': 'col-lg-10','required': '','notes': '','data':'待聯絡/聯絡中/已完成','Val':'0/1/2','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("SNO", "[{'subject': 'SNO','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);
            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("SNO", "SNO");
            re.Add("NAME", "姓名");
            re.Add("CITY", "縣市");
            re.Add("TOWN", "區域");
            re.Add("MOBILEE", "手機");
            re.Add("EMLE", "E-mail");
            //re.Add("REPLAYTP", "優先聯絡方式");
            re.Add("QUESTION", "優先聯絡方式");
            re.Add("PROJECT", "專案");
            re.Add("CONTENT", "需求概述");
            //re.Add("info", "需求概述");
            re.Add("CDATE", "留言時間");
            re.Add("STATUS", "狀態");
            re.Add("action", "動作");

            return re;
        }
        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "CDATE");
            re.Add("orderByType", "desc");

            return re;
        }
    }
}