﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Models.EntityModel;

namespace Web.Repository
{
    public class projectsetting_Repository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "Y";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '專案名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("indoor", "[{'subject': '上架/下架','type': 'radio','defaultVal': '下架','classVal': 'col-lg-10','required': 'required','notes': '','data':'上架/下架','Val':'上架/下架','useLang':'N'}]");
            main.Add("sortNum", "[{'subject': '排序','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("carstatus", "[{'subject': '車況','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'car_status'}]");
            main.Add("brand", "[{'subject': '廠牌','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'brand_category'}]");
            //main.Add("monthrent", "[{'subject': '月租金','type': 'select','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':'monthrent_category'}]");
            main.Add("renttime", "[{'subject': '租期(可複選)','type': 'checkbox','defaultVal': '','classVal': 'col-lg-10','required': 'required','notes': '','data':'1年/2年/3年/4年/5年','Val':'1/2/3/4/5','useLang':'N'}]");
            main.Add("roundservice", "[{'subject': '涵蓋服務','type': 'checkboxT','defaultVal': '','classVal': 'col-lg-10','required': 'required','notes': '','data':'領牌費用/牌照燃料稅/強制險/代步車/保養維修/任意險','Val':'領牌費用/牌照燃料稅/強制險/代步車/保養維修/任意險','useLang':'N'}]");
            //main.Add("mileage", "[{'subject': '限駛里程','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '(萬公里/年，包含定期保養及耗材更換)','useLang':'Y'}]");
            
            
            //main.Add("roundservicefree", "[{'subject': '','type': 'checkboxF','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','data':'乙式/丙式/乙式+丙式','Val':'乙式/丙式/乙式+丙式','useLang':'N'}]");
            main.Add("roundservicefree", "[{'subject': '','type': 'radioM','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','data':'乙式/丙式/乙式+丙式','Val':'乙式/丙式/乙式+丙式','useLang':'N'}]");
            main.Add("outline", "[{'subject': '到期處置(可複選)','type': 'checkbox','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','data':'歸還車輛/期滿購回','Val':'1/2','useLang':'N'}]");
            main.Add("url", "[{'subject': '專案連結','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            //main.Add("feature", "[{'subject': '專案特色','type': 'textarea','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");
            main.Add("feature", "[{'subject': '專案特色','type': 'editor','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'','notes': '','useLang':'Y'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");
            //other.Add("verify", "[{'subject': '審核狀態','type': 'verify','defaultVal': 'E','classVal': 'col-lg-10','required': '','notes': '','data':'尚未審核/通過/退件','Val':'E/Y/N','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("sortNum", "排序");
            re.Add("title", "專案名稱");
            //re.Add("carstatus_name", "車況");
            //re.Add("brand_name", "廠牌");
            //re.Add("setrent", "租金");
            re.Add("carstatusname", "車況");
            re.Add("brandname", "廠牌");
            re.Add("statusname", "租金");

            re.Add("renttime", "租期");
            //re.Add("feature", "專案特色");
            re.Add("info", "專案特色");
            re.Add("setrservice", "涵蓋服務");
            re.Add("outline", "到期處置");

            re.Add("indoor", "上下架");
            re.Add("modifydate", "更新時間");
            re.Add("status", "狀態");
            //re.Add("verify", "審核狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "sortNum");
            re.Add("orderByType", "asc");

            return re;
        }
    }
}