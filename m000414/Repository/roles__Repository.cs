﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Repository
{
    public class roles__Repository
    {
        // GET: message
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "N";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定
            Dictionary<String, Object> main = new Dictionary<string, object>();

            main.Add("title", "[{'subject': '群組名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '',}]");
            main.Add("rolenote", "[{'subject': '群組敘述','type': 'textarea','default': '','class': 'col-lg-10','required': '','readonly':'','notes': '',}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

               //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            #endregion

            #region 進階
            Dictionary<String, Object> other = new Dictionary<string, object>();

            //other.Add("forum_status", "[{'subject': '問題回覆檢視','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '','data':'全部顯示/僅分派之回覆','Val':'Y/N'}]");
            //other.Add("get_ask_mail", "[{'subject': '接收提問通知信件','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '','data':'接收/不接收','Val':'Y/N'}]");
            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N'}]");
            //other.Add("verify", "[{'subject': '審核權限','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N'}]");
            other.Add("permissions", "[{'subject': '權限設定','type': 'permissions','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '','data':'','Val':''}]");
            

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);

            return fromData;

        }


        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");    
            re.Add("info", "群組名稱");
            re.Add("rolenote", "群組敘述");
            re.Add("user_qty", "成員數");      
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }
    }
}