﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using Web.Models.EntityModel;
using Microsoft.Ajax.Utilities;
using System.Web.UI.WebControls;
using System.Xml.Schema;
using System.Runtime.CompilerServices;
using Web.Controllers;
using System.Security.Cryptography;
using System.Text;

namespace Web.Service
{
    public class TablesService : Controller
    {
        /// <summary>
        /// 取得列表資料提供給DataTable
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> getListData(string tables, NameValueCollection requests, string urlRoot, string guid = "")
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            Dictionary<String, Object> list = new Dictionary<String, Object>();
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            int skip = int.Parse(requests["start"].ToString());
            int take = int.Parse(requests["length"].ToString());
            if (take == -1)
            {
                take = 9999;
            }
            int iTotalDisplayRecords = 0;
            dynamic listData = null;
            dynamic sSearch = null;

            if (!string.IsNullOrEmpty(requests["search[value]"]))
            {
                sSearch = Newtonsoft.Json.Linq.JArray.Parse(requests["search[value]"].ToString());
                sSearch = sSearch[0];
            }

            //取得預設傳值
            Dictionary<string, string> defaultSearch = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(requests["sSearch"]))
            {
                try
                {
                    var defaultSearcStr = requests["sSearch"].ToString().Replace("\"", "").Split('=');
                    defaultSearch.Add(defaultSearcStr[0], defaultSearcStr[1]);
                }
                catch (Exception ex) { }
            }


            string columns = requests["order[0][column]"];

            string orderByKey = requests["columns[" + columns + "][data]"].ToString();
            string orderByType = requests["order[0][dir]"].ToString();
            string orderBy = formatOrderByKey(tables, orderByKey) + " " + orderByType.ToUpper();

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //首頁BANNER
                case "homebanner":
                    if (model != null)
                    {
                        var data = model.Repository<homebanner>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //影片介紹
                case "videocontent":
                    if (model != null)
                    {
                        var data = model.Repository<videocontent>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //個人長租好處
                case "benfit":
                    if (model != null)
                    {
                        var data = model.Repository<benfit>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //租賃服務好處
                case "rentbenfit":
                    if (model != null)
                    {
                        var data = model.Repository<rentbenfit>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //合約期間服務
                case "contact_service":
                    if (model != null)
                    {
                        var data = model.Repository<contact_service>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //合約期滿多重選擇
                case "multipleselect":
                    if (model != null)
                    {
                        var data = model.Repository<mulitpleselect>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //個人長租現金購車
                case "comparemoney":
                    if (model != null)
                    {
                        var data = model.Repository<comparemoney>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //長期租車短期租車
                case "comparetime":
                    if (model != null)
                    {
                        var data = model.Repository<comparetime>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //專案推薦
                case "projectsetting":
                    if (model != null)
                    {
                        var data = model.Repository<projectsetting>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //租金試算
                case "rentsetting":
                    if (model != null)
                    {
                        var data = model.Repository<rentsetting>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        List<rentsetting> resultm = new List<rentsetting>();
                        List<rentsetting> tempresultm = new List<rentsetting>();

                        foreach (var item in models)
                        {
                            resultm.Add(item);
                        }

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            //if (sSearch["keyword"].ToString() != "")
                            //{
                            //    string keywords = sSearch["keyword"].ToString();
                            //    models = models.Where(x => x.title.Contains(keywords));
                            //}
                            if (sSearch["re_status"].ToString() != "")
                            {
                                string status = sSearch["re_status"].ToString();
                                resultm = resultm.Where(x => x.title == status).ToList();
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                tempresultm = resultm;
                                resultm = new List<rentsetting>();
                                foreach (var item in tempresultm)
                                {
                                    var tempinout = (from a in DB.projectsetting
                                                     where a.guid == item.title
                                                     select a).FirstOrDefault();
                                    if (tempinout.indoor == status)
                                    {
                                        resultm.Add(item);
                                    }
                                }
                            }
                        }

                        iTotalDisplayRecords = resultm.ToList().Count;
                        listData = resultm.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //車型
                case "carmodel":
                    if (model != null)
                    {
                        var data = model.Repository<carmodel>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //近一步洽詢
                case "advanceform":
                    if (model != null)
                    {
                        var data = model.Repository<advanceform>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["emailkey"].ToString() != "")
                            {
                                string keywords = sSearch["emailkey"].ToString();
                                models = models.Where(x => x.email.Contains(keywords));
                            }
                            if (sSearch["telkey"].ToString() != "")
                            {
                                string keywords = sSearch["telkey"].ToString();
                                models = models.Where(x => x.phone.Contains(keywords));
                            }
                            if (sSearch["compkey"].ToString() != "")
                            {
                                DateTime keywords = DateTime.ParseExact(sSearch["compkey"].ToString(), "yyyy-MM-dd",System.Globalization.CultureInfo.InvariantCulture);
                                models = models.Where(x => x.create_date >= keywords);
                            }
                            if (sSearch["modelkey"].ToString() != "")
                            {
                                DateTime keywords = DateTime.ParseExact(sSearch["modelkey"].ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                                DateTime d2 = keywords.AddDays(1);
                                models = models.Where(x => x.create_date <= d2);
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                case "MLCONTACTUS":
                    if (model != null)
                    {
                        var data = model.Repository<MLCONTACTUS>();
                        var dataAdvance = model.Repository<MLCONTACTUS>();

                        var models = data.ReadsWhere(x => x.STATUS != 9);

                        List<MLCONTACTUS> mlcontactus = new List<MLCONTACTUS>();
                        var alladvance = dataAdvance.ReadsWhere(x => x.STATUS != 9).Where(x => x.SOURCE == 4).ToList();
                        string encrypt_key1 = "LEXUSADMSYS2020";

                        foreach (var item in alladvance)
                        {
                            //if(item.REPLAYTP == 0){
                            //    item.REPLAYTP = "電話";
                            //}

                            if (item.CONTENT.Length > 20)
                            {
                                item.CONTENT = item.CONTENT.Substring(0, 20) + "...";
                            }

                            //if (item.PROJECT == null)
                            //{

                            //}
                            //else
                            //{
                            //    if (item.PROJECT.Length > 6)
                            //    {
                            //        item.PROJECT = item.PROJECT.Substring(0, 6) + "...";
                            //    }
                            //}

                            if (item.REPLAYTP != null)
                            {
                                if (item.REPLAYTP == 1)
                                {
                                    item.QUESTION = "行動電話";
                                }
                                else if (item.REPLAYTP == 2)
                                {
                                    item.QUESTION = "E-MAIL";
                                }
                                else if (item.REPLAYTP == 3)
                                {
                                    item.QUESTION = "兩者皆可";
                                }
                            }
                            else
                            {
                                item.QUESTION = "";
                            }

                            string str1 = "";
                            string decrypt = "";
                            try
                            {
                                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
                                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                                SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();
                                byte[] key = sha256.ComputeHash(Encoding.UTF8.GetBytes(encrypt_key1));
                                byte[] iv = md5.ComputeHash(Encoding.UTF8.GetBytes(encrypt_key1));
                                aes.Key = key;
                                aes.IV = iv;

                                byte[] dataByteArray = Convert.FromBase64String(item.NAME);
                                byte[] dataByteArray2 = Convert.FromBase64String(item.EMLE);
                                byte[] dataByteArray3 = Convert.FromBase64String(item.MOBILEE);
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                                    {
                                        cs.Write(dataByteArray, 0, dataByteArray.Length);
                                        cs.FlushFinalBlock();
                                        str1 = Encoding.UTF8.GetString(ms.ToArray());
                                        item.NAME = str1;
                                    }
                                }
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                                    {
                                        cs.Write(dataByteArray2, 0, dataByteArray2.Length);
                                        cs.FlushFinalBlock();
                                        str1 = Encoding.UTF8.GetString(ms.ToArray());
                                        item.EMLE = str1;
                                    }
                                }
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                                    {
                                        cs.Write(dataByteArray3, 0, dataByteArray3.Length);
                                        cs.FlushFinalBlock();
                                        str1 = Encoding.UTF8.GetString(ms.ToArray());
                                        item.MOBILEE = str1;
                                    }
                                }
                                mlcontactus.Add(item);
                            }
                            catch (Exception exception1)
                            {
                                Exception exception = exception1;
                                str1 = "ERR";
                            }

                        }

                        //foreach (var reitem in mlcontactus)
                        //{
                        //    if (reitem.NAME.Length > 6)
                        //    {
                        //        reitem.NAME = reitem.NAME.Substring(0, 6) + "...";
                        //    }

                        //    if (reitem.MOBILEE.Length > 8)
                        //    {
                        //        reitem.MOBILEE = reitem.MOBILEE.Substring(0, 8) + "...";
                        //    }
                        //}


                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "" && sSearch["keyword"].ToString() != "undefined")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                mlcontactus = (from a in mlcontactus
                                               where a.NAME.Contains(keywords)
                                               select a).ToList();
                                //models = models.Where(x => x.NAME.Contains(keywords));
                            }
                            if (sSearch["emailkey"].ToString() != "" && sSearch["emailkey"].ToString() != "undefined")
                            {
                                string keywords = sSearch["emailkey"].ToString();
                                mlcontactus = (from a in mlcontactus
                                               where a.EMLE.Contains(keywords)
                                               select a).ToList();
                                //models = models.Where(x => x.EMLE.Contains(keywords));
                            }
                            if (sSearch["telkey"].ToString() != "" && sSearch["telkey"].ToString() != "undefined")
                            {
                                string keywords = sSearch["telkey"].ToString();
                                mlcontactus = (from a in mlcontactus
                                               where a.MOBILEE.Contains(keywords)
                                               select a).ToList();
                                //models = models.Where(x => x.MOBILEE.Contains(keywords));
                            }
                            if (sSearch["compkey"].ToString() != "" && sSearch["compkey"].ToString() != "undefined")
                            {
                                DateTime keywords = DateTime.ParseExact(sSearch["compkey"].ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                                mlcontactus = (from a in mlcontactus
                                               where a.CDATE >= keywords
                                               select a).ToList();
                                //models = models.Where(x => x.CDATE >= keywords);
                            }
                            if (sSearch["modelkey"].ToString() != "" && sSearch["modelkey"].ToString() != "undefined")
                            {
                                DateTime keywords = DateTime.ParseExact(sSearch["modelkey"].ToString(), "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                                DateTime d2 = keywords.AddDays(1);

                                mlcontactus = (from a in mlcontactus
                                               where a.CDATE <= d2
                                               select a).ToList();
                                //models = models.Where(x => x.CDATE <= d2);
                            }
                            if (sSearch["status"].ToString() != "" && sSearch["status"].ToString() != "undefined")
                            {
                                int status = Int32.Parse(sSearch["status"].ToString());
                                mlcontactus = (from a in mlcontactus
                                               where a.STATUS == status
                                               select a).ToList();
                                //models = models.Where(x => x.STATUS == status);
                            }
                        }

                        iTotalDisplayRecords = mlcontactus.ToList().Count;
                        listData = mlcontactus.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //用戶分享
                case "membershare":
                    if (model != null)
                    {
                        var data = model.Repository<membershare>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //最新消息分類
                case "news_category":
                    if (model != null)
                    {
                        var data = model.Repository<news_category>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //最新消息
                case "news":
                    if (model != null)
                    {
                        var data = model.Repository<Models.news>();
                        var models = data.ReadsWhere(x => x.status != "D" && x.lang == defLang);
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(x => x.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(x => x.status == status);
                            }
                            if (sSearch["re_status"].ToString() != "")
                            {
                                string restatus = sSearch["re_status"].ToString();
                                if (restatus == "stick")
                                {
                                    models = models.Where(x => x.sticky == "Y");
                                }
                                else if (restatus == "indexstick")
                                {
                                    models = models.Where(x => x.index_sticky == "Y");
                                }
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(x => x.category.Contains(category));
                            }
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //群組管理
                case "roles":
                    if (model != null)
                    {
                        var data = model.Repository<Models.roles>();
                        var models = data.ReadsWhere(m => m.status != "D");
                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //使用者
                case "user":
                    if (model != null)
                    {
                        var data = model.Repository<Models.user>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.username != "sysadmin");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.username.Contains(keywords) || m.name.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.role_guid.Contains(category));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //防火牆
                case "firewalls":
                    if (model != null)
                    {
                        var data = model.Repository<Models.firewalls>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //系統日誌
                case "system_log":
                    if (model != null)
                    {
                        var data = model.Repository<system_log>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords) || m.ip.Contains(keywords) || m.notes.Contains(keywords));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //資源回收桶
                case "ashcan":
                    if (model != null)
                    {
                        var data = model.Repository<Models.ashcan>();

                        var models = data.ReadsWhere(m => m.from_guid != "");

                        if (!string.IsNullOrEmpty(requests["search[value]"]))
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;
            }

            list.Add("data", dataTableListData(listData, tables, urlRoot, requests));
            list.Add("iTotalDisplayRecords", iTotalDisplayRecords);

            return list;
        }
        private static List<Object> dataTableListData(dynamic list, string tables, string urlRoot, NameValueCollection requests)
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            Model DB = new Model();

            List<Object> re = new List<Object>();

            Dictionary<String, Object> dataTableRow = dataTableTitle(tables);//資料欄位
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系
            string json = JsonConvert.SerializeObject(list, Formatting.Indented);//轉型(model to Json String)

            var Json1 = Newtonsoft.Json.Linq.JArray.Parse(json);//第一層列表轉json陣列
            string picurlRoot = urlRoot;
            if (urlRoot == "/")
            {
                urlRoot = "";
            }

            //取得選單一些基本資訊

            IQueryable<system_menu> system_menu_search = DB.system_menu.Where(m => m.tables == tables);
            if (requests["category"] != null && requests["category"].ToString() != "")
            {
                string category = requests["category"].ToString();
                string act_path = "list/" + category;
                system_menu_search = system_menu_search.Where(m => m.act_path == act_path);
            }

            System.Web.HttpContext contextAction = System.Web.HttpContext.Current;
            string currentUserGuid = "";
            if (context.Request.Cookies["sysLogin"] != null)
            {
                HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                string[] tempAccount = aCookie.Value.Split('&');

                currentUserGuid = context.Server.HtmlEncode(tempAccount[1].Replace("sysUserGuid=", "").ToString());
            }

            var system_menu = system_menu_search.FirstOrDefault();
            if (system_menu == null)
            {
                system_menu = DB.system_menu.Where(m => m.tables == tables).FirstOrDefault();
            }

            int i = 0;
            foreach (var dataList in Json1)
            {
                Dictionary<String, Object> rowData = new Dictionary<string, object>();
                string thisGuid = "";
                if (tables == "MLCONTACTUS")
                {
                    thisGuid = dataList["SNO"].ToString();
                }
                else
                {
                    thisGuid = dataList["guid"].ToString();
                }
                

                rowData.Add("DT_RowId", "row_" + thisGuid);//欄位ID

                foreach (KeyValuePair<string, object> dataItem in dataTableRow)
                {
                    switch (dataItem.Key.ToString())
                    {
                        #region 鍵值

                        case "guid":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/checkbox.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                readText = readText.Replace("{$value}", dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$title}", tableTitleKey(tables, dataList));
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 圖片

                        case "pic":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                var pic = dataList[dataItem.Key.ToString()].ToString();

                                if (!string.IsNullOrEmpty(pic))
                                {
                                    if (pic.Contains("/Content"))
                                    {
                                        //readText = readText.Replace("{$value}", dataList[dataItem.Key.ToString()].ToString());
                                        readText = readText.Replace("{$value}", FunctionService.getWebUrl() + dataList[dataItem.Key.ToString()].ToString());
                                    }
                                    else
                                    {
                                        //readText = readText.Replace("{$value}", picurlRoot + dataList[dataItem.Key.ToString()].ToString());
                                        readText = readText.Replace("{$value}", FunctionService.getWebUrl() + dataList[dataItem.Key.ToString()].ToString());
                                    }
                                }
                                else
                                {
                                    readText = readText.Replace("{$value}", picurlRoot + "styles/images/default.jpg");
                                }

                                readText = readText.Replace("{$backColor}", "");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 圖片

                        case "icon":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$value}", picurlRoot + dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$backColor}", "style=\"background-color:#333;\"");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 圖片

                        case "list_pic":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$value}", picurlRoot + dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$backColor}", "");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        case "setprice":

                            var rentdata = (from a in DB.rentsetting
                                          where a.guid == thisGuid
                                          select a.price).FirstOrDefault();
                            if (rentdata != null && rentdata != "")
                            {
                                if(rentdata == "0"){
                                    rowData.Add(dataItem.Key.ToString(), "0");
                                }
                                else
                                {
                                    //string readPriceText = "<div >" + rentdata + "</div>";
                                    string readPriceText = "<div >" + Int32.Parse(rentdata).ToString("###,###") + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readPriceText);
                                }
                            }
                            else
                            {
                                rowData.Add(dataItem.Key.ToString(), "0");
                            }

                            break;

                        case "setpricediscount":

                            var discount = (from a in DB.rentsetting
                                            where a.guid == thisGuid
                                            select a.pricediscount).FirstOrDefault();

                            if (discount != null && discount !="")
                            {
                                if (discount == "0")
                                {
                                    rowData.Add(dataItem.Key.ToString(), "0");
                                }
                                else
                                {
                                    //string readDiscountText = "<div >" + discount + "</div>";
                                    string readDiscountText = "<div >" + Int32.Parse(discount).ToString("###,###") + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readDiscountText);
                                }
                            }
                            else
                            {
                                rowData.Add(dataItem.Key.ToString(), "0");
                            }

                            break;

                        case "setrentmonth":

                            var rentmonth = (from a in DB.rentsetting
                                            where a.guid == thisGuid
                                            select a.rentmonth).FirstOrDefault();

                            if (rentmonth != null && rentmonth !="")
                            {
                                if (rentmonth == "0")
                                {
                                    rowData.Add(dataItem.Key.ToString(), "0");
                                }
                                else
                                {
                                    //string readRentMonthText = "<div >" + rentmonth + "</div>";
                                    string readRentMonthText = "<div >" + Int32.Parse(rentmonth).ToString("###,###") + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readRentMonthText);
                                }
                            }
                            else
                            {
                                rowData.Add(dataItem.Key.ToString(), "0");
                            }


                            break;

                        case "setmargin":

                            var margin = (from a in DB.rentsetting
                                            where a.guid == thisGuid
                                            select a.margin).FirstOrDefault();

                            if (margin != null &&  margin != "")
                            {
                                if (margin == "0")
                                {
                                    rowData.Add(dataItem.Key.ToString(), "0");
                                }
                                else
                                {
                                    //string readMarginText = "<div >" + margin + "</div>";
                                    string readMarginText = "<div >" + Int32.Parse(margin).ToString("###,###") + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readMarginText);
                                }
                            }
                            else
                            {
                                rowData.Add(dataItem.Key.ToString(), "0");
                            }

                            break;

                        case "setrebuyprice":

                            var rebuyprice = (from a in DB.rentsetting
                                            where a.guid == thisGuid
                                            select a.rebuyprice).FirstOrDefault();

                            if (rebuyprice != null && rebuyprice != "")
                            {
                                if (rebuyprice == "0")
                                {
                                    rowData.Add(dataItem.Key.ToString(), "0");
                                }
                                else
                                {
                                    //string readRebuypriceText = "<div >" + rebuyprice + "</div>";
                                    string readRebuypriceText = "<div >" + Int32.Parse(rebuyprice).ToString("###,###") + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readRebuypriceText);
                                }
                            }
                            else
                            {
                                rowData.Add(dataItem.Key.ToString(), "0");
                            }


                            break;

                        case "setrent":

                            var nonproj = thisGuid;
                            var isrent = (from a in DB.rentsetting
                                          where a.title == thisGuid && a.status == "Y" && a.lang == "tw"
                                          select a).ToList();

                            if (isrent.Count() != 0)
                            {
                                string readRentText = "<div >" + "已設定" + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readRentText);
                            }
                            else
                            {
                                string readRentText = "<div >" + "未設定" + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readRentText);
                            }

                            break;

                        case "setrservice":

                            var prjsingle = (from a in DB.projectsetting
                                          where a.guid == thisGuid
                                          select a).FirstOrDefault();

                            if (prjsingle != null)
                            {
                                string readprojnameText = "<div >" + prjsingle.roundservice + prjsingle.roundservicefree + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readprojnameText);
                            }
                            else
                            {
                                rowData.Add(dataItem.Key.ToString(), "");
                            }

                            break;

                        case "proj_title":
                            var projecttitle = (from a in DB.rentsetting
                                                  where a.guid == thisGuid
                                                  select a.title).FirstOrDefault();
                            if (projecttitle != null)
                            {
                                var tempname = (from a in DB.projectsetting
                                                where a.guid == projecttitle
                                                select a.title).FirstOrDefault();

                                string readprojnameText = "<div >" + tempname + "</div>";
                                rowData.Add(dataItem.Key.ToString(),readprojnameText);
                            }
                            else
                            {
                                rowData.Add(dataItem.Key.ToString(), "");
                            }

                            break;

                        case "carstatus_name":

                            var judgecarstatus = (from a in DB.projectsetting
                                             where a.guid == thisGuid
                                             select a.carstatus).FirstOrDefault();

                            var carstatus = (from a in DB.car_status
                                          where a.guid == judgecarstatus && a.status == "Y"
                                          select a).FirstOrDefault();

                            string readCarStatusText = "<div >" + carstatus.title + "</div>";
                            rowData.Add(dataItem.Key.ToString(), readCarStatusText);

                            break;

                        case "brand_name":

                            var judgebrandcategory = (from a in DB.projectsetting
                                                  where a.guid == thisGuid
                                                  select a.brand).FirstOrDefault();

                            var brandcategory = (from a in DB.brand_category
                                             where a.guid == judgebrandcategory && a.status == "Y"
                                             select a).FirstOrDefault();

                            string readBrandText = "<div >" + brandcategory.title + "</div>";
                            rowData.Add(dataItem.Key.ToString(), readBrandText);

                            break;

                        case "username":
                            if (tables == "user")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/info.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$value}", dataList["username"].ToString());
                                readText = readText.Replace("{$modifydate}", "");
                                readText = readText.Replace("{$sticky}", "");
                                readText = readText.Replace("{$index_sticky}", "");
                                readText = readText.Replace("{$content}", "");
                                readText = readText.Replace("{$notes}", "");

                                //連結
                                string addId = "";
                                //dataTableCategory
                                if (context.Session["dataTableCategory"] != null)
                                {
                                    addId = "?c=" + context.Session["dataTableCategory"].ToString();
                                }
                                readText = readText.Replace("{$url}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid + addId);
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            else
                            {
                                if (dataList[dataItem.Key.ToString()] != null)
                                {
                                    string readText = "<div >" + dataList[dataItem.Key.ToString()].ToString() + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }

                            break;

                        #region 內容摘要

                        case "info":
                            if (thisGuid != null && thisGuid != "")
                            {
                                if (tables != "shareholders")
                                {
                                    string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/info.cshtml");
                                    string readText = System.IO.File.ReadAllText(path);

                                    if (tables == "projectsetting")
                                    {
                                        readText = readText.Replace("{$value}", "");
                                    }
                                    else if (tables == "MLCONTACTUS")
                                    {
                                        readText = readText.Replace("{$value}", "");
                                    }
                                    else
                                    {
                                        readText = readText.Replace("{$value}", dataList["title"].ToString());
                                    }

                                    //置頂
                                    string sticky = "";
                                    if (dataList["sticky"] != null && dataList["sticky"].ToString() != "")
                                    {
                                        if (dataList["sticky"].ToString() == "Y")
                                        {
                                            sticky = "<span class=\"badge badge-pill badge-warning ml-2\">置頂</span>";
                                        }
                                    }
                                    readText = readText.Replace("{$sticky}", sticky);

                                    //首頁置頂
                                    string index_sticky = "";
                                    if (dataList["index_sticky"] != null && dataList["index_sticky"].ToString() != "")
                                    {
                                        if (dataList["index_sticky"].ToString() == "Y")
                                        {
                                            index_sticky = "<span class=\"badge badge-pill badge-warning ml-2\">首頁置頂</span>";
                                        }
                                    }
                                    readText = readText.Replace("{$index_sticky}", index_sticky);

                                    //今日異動
                                    string modifydate = "";
                                    if (dataList["modifydate"] != null && dataList["modifydate"].ToString() != "")
                                    {
                                        string date = DateTime.Parse(dataList["modifydate"].ToString()).ToString("yyyy-MM-dd");
                                        if (date == DateTime.Now.ToString("yyyy-MM-dd"))
                                        {
                                            modifydate = "<span class=\"badge badge-pill badge-info ml-2\">今日異動</span>";
                                        }
                                    }
                                    readText = readText.Replace("{$modifydate}", modifydate);

                                    //簡述
                                    string content = "";

                                    if (tables == "projectsetting")
                                    {
                                        if (!string.IsNullOrEmpty(dataList["feature"].ToString()))
                                        {
                                            content = Regex.Replace(dataList["feature"].ToString(), "<.*?>", String.Empty);

                                            if (content.Length > 25)
                                            {
                                                content = content.Substring(0, 25) + "...";
                                            }
                                            content = "<p class=\"m-0 p-0 text-pre-line\">" + content + "</p>";
                                        }
                                        readText = readText.Replace("{$content}", content);
                                    }
                                    else if (tables == "MLCONTACTUS")
                                    {
                                        if (!string.IsNullOrEmpty(dataList["CONTENT"].ToString()))
                                        {
                                            content = Regex.Replace(dataList["CONTENT"].ToString(), "<.*?>", String.Empty);

                                            if (content.Length > 12)
                                            {
                                                content = content.Substring(0, 12) + "...";
                                            }
                                            content = "<p class=\"m-0 p-0 text-pre-line\">" + content + "</p>";
                                        }
                                        readText = readText.Replace("{$content}", content);
                                    }
                                    else if (tables != "index_url" && tables != "insurance_content" && tables != "insurance_company_content" && tables != "abouts")
                                    {
                                        if (tables == "shared_url")
                                        {
                                            if (dataList["content"] != null && dataList["content"].ToString() != "")
                                            {
                                                content = Regex.Replace(dataList["content"].ToString(), "<.*?>", String.Empty);

                                                if (content.Length > 99)
                                                {
                                                    content = content.Substring(0, 99) + "...";
                                                }
                                                content = "<p class=\"m-0 p-0 text-pre-line\"><small>" + content + "</small></p>";
                                            }
                                            readText = readText.Replace("{$content}", content);
                                        }
                                        else
                                        {
                                            if (dataList["content"] != null && dataList["content"].ToString() != "")
                                            {
                                                content = Regex.Replace(dataList["content"].ToString(), "<.*?>", String.Empty);

                                                if (content.Length > 50)
                                                {
                                                    content = content.Substring(0, 50) + "...";
                                                }
                                                content = "<p class=\"m-0 p-0 text-pre-line\"><small>" + content + "</small></p>";
                                            }
                                            readText = readText.Replace("{$content}", content);
                                        }
                                    }
                                    else
                                    {
                                        readText = readText.Replace("{$content}", content);
                                    }

                                    string notes = "";//

                                    readText = readText.Replace("{$notes}", notes);

                                    //連結
                                    string addId = "";
                                    //dataTableCategory
                                    if (context.Session["dataTableCategory"] != null)
                                    {
                                        addId = "?c=" + context.Session["dataTableCategory"].ToString();
                                    }
                                    readText = readText.Replace("{$url}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid + addId);
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                                else
                                {
                                    string readText = "<div>" + dataList[dataItem.Key.ToString()].ToString() + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }
                            break;

                        #endregion

                        #region 分類或其他上層對應

                        case "category":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                                string categoryID = dataList["category"].ToString();
                                switch (tables)
                                {
                                    case "news":
                                        readText = DB.news_category.Where(m => m.guid == categoryID && m.lang == defLang).First().title;
                                        break;
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 複選分類

                        case "pluralCategory":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                                if (tables == "user")
                                {
                                    string[] categoryArr = dataList["role_guid"].ToString().Split(',');
                                    for (int s = 0; s < categoryArr.Length; s++)
                                    {
                                        string categoryID = categoryArr[s].ToString();
                                        var category = DB.roles.Where(m => m.guid == categoryID).FirstOrDefault();
                                        if (category != null)
                                        {
                                            readText += category.title + "<br>";
                                        }
                                    }
                                }

                                readText = "<div>" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 審核者

                        case "user":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";

                                if (dataList["user_guid"] != null && dataList["user_guid"].ToString() != "")
                                {
                                    string categoryID = dataList["user_guid"].ToString();
                                    var category = DB.user.Where(m => m.guid == categoryID).FirstOrDefault();
                                    if (category != null)
                                    {
                                        readText = category.name;
                                    }
                                    else
                                    {
                                        readText = "查無審核者!";
                                    }
                                }
                                else
                                {
                                    readText = "未指定";
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 瀏覽次數

                        case "view":

                            /*  if (thisGuid != null && thisGuid != "")
                              {
                                  int record_log_qty = DB.record_log.Where(m => m.from == tables).Where(m => m.from_guid == thisGuid).Where(m => m.types == "view").Count();
                                  string readText = "<div class=\"text-center\">"+ record_log_qty.ToString() + "</div>";
                                  rowData.Add(dataItem.Key.ToString(), readText);
                              }*/
                            break;

                        #endregion

                        #region 日期

                        case "date":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }


                            break;

                        #endregion

                        #region 發布日期
                        case "start_date":
                        case "startdate":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 發布日期2

                        case "create_date":

                            if (tables == "system_log")
                            {
                                if (dataList[dataItem.Key.ToString()] != null)
                                {
                                    string dates = "";
                                    if (dataList[dataItem.Key.ToString()].ToString() != "")
                                    {
                                        dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                                    }
                                    string readText = "<div class=\"text-center\">" + dates + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }
                            else if (tables == "advanceform")
                            {
                                if (dataList[dataItem.Key.ToString()] != null)
                                {
                                    string dates = "";
                                    if (dataList[dataItem.Key.ToString()].ToString() != "")
                                    {
                                        dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                                    }
                                    string readText = "<div class=\"text-center\">" + dates + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }
                            else
                            {
                                if (dataList[dataItem.Key.ToString()] != null)
                                {
                                    string dates = "";
                                    if (dataList[dataItem.Key.ToString()].ToString() != "")
                                    {
                                        dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                    }
                                    string readText = "<div class=\"text-center\">" + dates + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }

                            break;

                        case "CDATE":

                            if (tables == "system_log")
                            {
                                if (dataList[dataItem.Key.ToString()] != null)
                                {
                                    string dates = "";
                                    if (dataList[dataItem.Key.ToString()].ToString() != "")
                                    {
                                        dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                                    }
                                    string readText = "<div class=\"text-center\">" + dates + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }
                            else if (tables == "MLCONTACTUS")
                            {
                                if (dataList[dataItem.Key.ToString()] != null)
                                {
                                    string dates = "";
                                    if (dataList[dataItem.Key.ToString()].ToString() != "")
                                    {
                                        dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd HH:mm:ss");
                                    }
                                    string readText = "<div>" + dates + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }
                            else
                            {
                                if (dataList[dataItem.Key.ToString()] != null)
                                {
                                    string dates = "";
                                    if (dataList[dataItem.Key.ToString()].ToString() != "")
                                    {
                                        dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                    }
                                    string readText = "<div class=\"text-center\">" + dates + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }

                            break;

                        #endregion

                        #region 狀態

                        case "status":

                            dynamic permissionsVerifyStatus = context.Session["permissions"];//取得權限紀錄

                            if (dataList[dataItem.Key.ToString()] != null && dataList[dataItem.Key.ToString()].ToString() != "")
                            {
                                if(tables == "advanceform")
                                {
                                    string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/status.cshtml");
                                    string readText = System.IO.File.ReadAllText(path);

                                    var tempuserdata = (from a in DB.user
                                                        where a.guid == currentUserGuid
                                                        select a.role_guid).FirstOrDefault();
                                    var permissionsAction = (from a in DB.role_permissions
                                                             where a.role_guid == tempuserdata && a.permissions_guid == system_menu.guid
                                                             select a.permissions_status).FirstOrDefault();

                                    string style = "danger";
                                    string statusSubject = "已回覆";
                                    string status = dataList[dataItem.Key.ToString()].ToString();

                                    if (status == "N")
                                    {
                                        style = "secondary";
                                        statusSubject = "未回覆";
                                    }

                                    if (permissionsAction == "F")
                                    {
                                        readText = readText.Replace("{$displayStatus}", "");
                                    }
                                    else
                                    {
                                        readText = readText.Replace("{$displayStatus}", "pointer-events: none;");
                                    }
                                    readText = readText.Replace("{$style}", style);
                                    readText = readText.Replace("{$statusSubject}", statusSubject);
                                    readText = readText.Replace("{$status}", status);
                                    readText = readText.Replace("{$guid}", thisGuid);
                                    if (tables == "user" || tables == "project_users")
                                    {
                                        readText = readText.Replace("{$title}", dataList["name"].ToString());
                                    }
                                    else
                                    {
                                        readText = readText.Replace("{$title}", dataList["title"].ToString());
                                    }

                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                                else
                                {
                                    string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/status.cshtml");
                                    string readText = System.IO.File.ReadAllText(path);

                                    var tempuserdata = (from a in DB.user
                                                        where a.guid == currentUserGuid
                                                        select a.role_guid).FirstOrDefault();
                                    var permissionsAction = (from a in DB.role_permissions
                                                             where a.role_guid == tempuserdata && a.permissions_guid == system_menu.guid
                                                             select a.permissions_status).FirstOrDefault();

                                    string style = "danger";
                                    string statusSubject = "啟用";
                                    string status = dataList[dataItem.Key.ToString()].ToString();

                                    if (status == "N")
                                    {
                                        style = "secondary";
                                        statusSubject = "停用";
                                    }

                                    if (permissionsAction == "F")
                                    {
                                        readText = readText.Replace("{$displayStatus}", "");
                                    }
                                    else
                                    {
                                        readText = readText.Replace("{$displayStatus}", "pointer-events: none;");
                                    }
                                    readText = readText.Replace("{$style}", style);
                                    readText = readText.Replace("{$statusSubject}", statusSubject);
                                    readText = readText.Replace("{$status}", status);
                                    readText = readText.Replace("{$guid}", thisGuid);
                                    if (tables == "user" || tables == "project_users")
                                    {
                                        readText = readText.Replace("{$title}", dataList["name"].ToString());
                                    }
                                    else
                                    {
                                        readText = readText.Replace("{$title}", dataList["title"].ToString());
                                    }

                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }

                            break;

                        case "STATUS":

                            dynamic permissionsVerifyStatusn = context.Session["permissions"];//取得權限紀錄

                            if (dataList[dataItem.Key.ToString()] != null && dataList[dataItem.Key.ToString()].ToString() != "")
                            {
                                if (tables == "MLCONTACTUS")
                                {
                                    string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/status.cshtml");
                                    string readText = System.IO.File.ReadAllText(path);

                                    var tempuserdata = (from a in DB.user
                                                        where a.guid == currentUserGuid
                                                        select a.role_guid).FirstOrDefault();
                                    var permissionsAction = (from a in DB.role_permissions
                                                             where a.role_guid == tempuserdata && a.permissions_guid == system_menu.guid
                                                             select a.permissions_status).FirstOrDefault();

                                    string style = "danger";
                                    string statusSubject = "已完成";
                                    string status = dataList[dataItem.Key.ToString()].ToString();

                                    if (status == "0")
                                    {
                                        style = "secondary";
                                        statusSubject = "待聯絡";
                                    }
                                    else if (status == "1")
                                    {
                                        style = "secondary";
                                        statusSubject = "聯絡中";
                                    }

                                    readText = readText.Replace("{$displayStatus}", "pointer-events: none;");


                                    if (permissionsAction == "F")
                                    {
                                        readText = readText.Replace("{$displayStatus}", "");
                                    }
                                    else
                                    {
                                        readText = readText.Replace("{$displayStatus}", "pointer-events: none;");
                                    }
                                    readText = readText.Replace("{$style}", style);
                                    readText = readText.Replace("{$statusSubject}", statusSubject);
                                    readText = readText.Replace("{$status}", status);
                                    readText = readText.Replace("{$guid}", thisGuid);
                                    if (tables == "user" || tables == "project_users")
                                    {
                                        readText = readText.Replace("{$title}", dataList["name"].ToString());
                                    }
                                    else if (tables == "MLCONTACTUS")
                                    {
                                        readText = readText.Replace("{$title}", dataList["NAME"].ToString());
                                    }
                                    else
                                    {
                                        readText = readText.Replace("{$title}", dataList["title"].ToString());
                                    }

                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                                else
                                {
                                    string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/status.cshtml");
                                    string readText = System.IO.File.ReadAllText(path);

                                    var tempuserdata = (from a in DB.user
                                                        where a.guid == currentUserGuid
                                                        select a.role_guid).FirstOrDefault();
                                    var permissionsAction = (from a in DB.role_permissions
                                                             where a.role_guid == tempuserdata && a.permissions_guid == system_menu.guid
                                                             select a.permissions_status).FirstOrDefault();

                                    string style = "danger";
                                    string statusSubject = "啟用";
                                    string status = dataList[dataItem.Key.ToString()].ToString();

                                    if (status == "N")
                                    {
                                        style = "secondary";
                                        statusSubject = "停用";
                                    }

                                    if (permissionsAction == "F")
                                    {
                                        readText = readText.Replace("{$displayStatus}", "");
                                    }
                                    else
                                    {
                                        readText = readText.Replace("{$displayStatus}", "pointer-events: none;");
                                    }
                                    readText = readText.Replace("{$style}", style);
                                    readText = readText.Replace("{$statusSubject}", statusSubject);
                                    readText = readText.Replace("{$status}", status);
                                    readText = readText.Replace("{$guid}", thisGuid);
                                    if (tables == "user" || tables == "project_users")
                                    {
                                        readText = readText.Replace("{$title}", dataList["name"].ToString());
                                    }
                                    else
                                    {
                                        readText = readText.Replace("{$title}", dataList["title"].ToString());
                                    }

                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }

                            break;

                        #endregion

                        #region 審核狀態

                        case "verify":

                            dynamic permissionsVerify = context.Session["permissions"];//取得權限紀錄

                            if (dataList[dataItem.Key.ToString()] != null && dataList[dataItem.Key.ToString()].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/verify.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                var tempuserdata = (from a in DB.user
                                                    where a.guid == currentUserGuid
                                                    select a.role_guid).FirstOrDefault();
                                var permissionsAction = (from a in DB.role_permissions
                                                         where a.role_guid == tempuserdata && a.permissions_guid == system_menu.guid
                                                         select a.permissions_status).FirstOrDefault();

                                string style = "danger";
                                string statusSubject = "通過";
                                string status = dataList[dataItem.Key.ToString()].ToString();

                                if (status == "N")
                                {
                                    style = "secondary";
                                    statusSubject = "退件";
                                }
                                else if (status == "E")
                                {
                                    style = "secondary";
                                    statusSubject = "尚未審核";
                                }

                                if (permissionsAction == "F")
                                {
                                    readText = readText.Replace("{$display}", "");
                                }
                                else
                                {
                                    readText = readText.Replace("{$display}", "pointer-events: none;");
                                }
                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);
                                readText = readText.Replace("{$status}", status);
                                readText = readText.Replace("{$guid}", thisGuid);
                                readText = readText.Replace("{$title}", dataList["title"].ToString());

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 討論區狀態

                        case "forum_status":

                            if (dataList["status"] != null && dataList["status"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "待分派";
                                string status = dataList["status"].ToString();

                                if (status == "disable")
                                {
                                    style = "secondary";
                                    statusSubject = "停用";
                                }
                                if (status == "stagnate")
                                {
                                    style = "dark";
                                    statusSubject = "後續評估";
                                }
                                if (status == "success")
                                {
                                    style = "success";
                                    statusSubject = "已回覆";
                                }
                                if (status == "warning")
                                {
                                    style = "warning";
                                    statusSubject = "處理中";
                                }
                                if (status == "end")
                                {
                                    style = "dark";
                                    statusSubject = "已解決";
                                }
                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 回覆狀態

                        case "re_status":

                            if (dataList["re_status"] != null && dataList["re_status"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "尚未回覆";
                                string status = dataList["re_status"].ToString();

                                if (status == "Y")
                                {
                                    style = "success";
                                    statusSubject = "已回覆";
                                }

                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            else
                            {
                                rowData.Add(dataItem.Key.ToString(), "");
                            }
                            break;

                        #endregion

                        #region 角色

                        case "role":

                            if (dataList["role"] != null && dataList["role"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "primary";
                                string statusSubject = "回覆者";
                                string status = dataList["role"].ToString();

                                if (status == "Tongren")
                                {
                                    style = "secondary";
                                    statusSubject = "同仁";
                                }
                                if (status == "Asker")
                                {
                                    style = "danger";
                                    statusSubject = "提問者";
                                }

                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 屬性

                        case "type":

                            if (dataList[dataItem.Key.ToString()] != null && dataList[dataItem.Key.ToString()].ToString() != "")
                            {
                                string readText = "";
                                switch (tables)
                                {
                                    case "holiday":
                                        string type = "休假";
                                        if (dataList[dataItem.Key.ToString()].ToString() == "duty")
                                        {
                                            type = "補上班";
                                        }
                                        readText = type;
                                        break;

                                    case "index_url":
                                        string col = "";
                                        if (dataList[dataItem.Key.ToString()].ToString() == "6")
                                        {
                                            col = "小";
                                        }
                                        else if (dataList[dataItem.Key.ToString()].ToString() == "12")
                                        {
                                            col = "大";
                                        }
                                        readText = col;
                                        break;
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 排序

                        case "sortIndex":

                            dynamic permissionsSort = context.Session["permissions"];//取得權限紀錄

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/sortIndex.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                var tempuserdata = (from a in DB.user
                                                    where a.guid == currentUserGuid
                                                    select a.role_guid).FirstOrDefault();
                                var permissionsAction = (from a in DB.role_permissions
                                                         where a.role_guid == tempuserdata && a.permissions_guid == system_menu.guid
                                                         select a.permissions_status).FirstOrDefault();


                                if (permissionsAction == "F")
                                {
                                    readText = readText.Replace("{$displaySort}", "");
                                }
                                else
                                {
                                    readText = readText.Replace("{$displaySort}", "pointer-events: none;");
                                }

                                readText = readText.Replace("{$value}", dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$guid}", thisGuid);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 成員數

                        case "user_qty":
                            if (dataList["guid"] != null)
                            {
                                string role_guid = dataList["guid"].ToString();
                                var data = DB.user.Where(m => m.role_guid.Contains(role_guid)).Where(m => m.status == "Y").Where(m => m.username != "sysadmin").Count();
                                string readText = "<div>" + data + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 來源資料表名稱

                        case "tables":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string fromTable = dataList[dataItem.Key.ToString()].ToString();
                                var data = DB.system_menu.Where(m => m.tables == fromTable).Select(a => new { title = a.title }).FirstOrDefault();
                                rowData.Add(dataItem.Key.ToString(), data.title);
                            }
                            break;

                        #endregion

                        #region 動作

                        case "action":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/action.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$guid}", thisGuid);

                                dynamic permissions = context.Session["permissions"];//取得權限紀錄
                                var tempuserdata = (from a in DB.user
                                                    where a.guid == currentUserGuid
                                                    select a.role_guid).FirstOrDefault();
                                var permissionsAction = (from a in DB.role_permissions
                                                         where a.role_guid == tempuserdata && a.permissions_guid == system_menu.guid
                                                         select a.permissions_status).FirstOrDefault();

                                //是否修改
                                if (system_menu.can_edit == "Y" && permissionsAction == "F")
                                {
                                    readText = readText.Replace("{$editHide}", "");
                                }
                                else
                                {
                                    readText = readText.Replace("{$editHide}", "style=\"display:none;\"");
                                }
                                //是否可刪除

                                if (system_menu.can_del == "Y" && permissionsAction == "F")
                                {
                                    readText = readText.Replace("{$delHide}", "");
                                }
                                else
                                {
                                    readText = readText.Replace("{$delHide}", "style=\"display:none;\"");
                                }

                                string addId = "";
                                //dataTableCategory
                                if (context.Session["dataTableCategory"] != null)
                                {
                                    addId = "?c=" + context.Session["dataTableCategory"].ToString();
                                }

                                //連結
                                readText = readText.Replace("{$editUrl}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid + addId);

                                if (system_menu.index_view_url != null && system_menu.index_view_url.ToString() != "")
                                {
                                    readText = readText.Replace("{$index_view_url}", "<a href=\"" + urlRoot + system_menu.index_view_url.ToString() + thisGuid + "\" target=\"_blank\" class=\"btn btn-outline-default btn-sm\" role=\"button\" aria-pressed=\"true\" title=\"瀏覽前端\"><i class=\"icon-eye3\"></i><span class=\"text-hide\">瀏覽</span></a>");
                                }
                                else
                                {
                                    readText = readText.Replace("{$index_view_url}", "");
                                }

                                readText = readText.Replace("{$title}", tableTitleKey(tables, dataList));

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 按鈕

                        case "button":
                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/button.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                switch (tables)
                                {
                                    case "cases_city":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/cases_city_history/list/" + thisGuid);
                                        break;

                                    case "progress_cases":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/progress_info/list/" + thisGuid);
                                        break;
                                }

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        case "addButton":
                            if (!string.IsNullOrEmpty(thisGuid))
                            {

                                var type = dataList["type"].ToString();
                                if (tables == "insurance_company_content")
                                {
                                    if (type == "main")
                                    {
                                        string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/addbutton.cshtml");
                                        string readText = System.IO.File.ReadAllText(path);
                                        readText = readText.Replace("{$title}", "附加險");
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/insurance_company_content_add/list/" + thisGuid);
                                        rowData.Add(dataItem.Key.ToString(), readText);
                                    }
                                    else
                                    {
                                        string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/addbutton.cshtml");
                                        string readText = System.IO.File.ReadAllText(path);
                                        readText = readText.Replace("{$title}", "附加險");
                                        rowData.Add(dataItem.Key.ToString(), "");
                                    }
                                }
                                else
                                {
                                    if (type == "main" || type == "mainsdata")
                                    {
                                        string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/addbutton.cshtml");
                                        string readText = System.IO.File.ReadAllText(path);
                                        readText = readText.Replace("{$title}", "附加險");
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/insurance_content_add/list/" + thisGuid);
                                        rowData.Add(dataItem.Key.ToString(), readText);
                                    }
                                    else if (type == "mains")
                                    {
                                        string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/addbutton.cshtml");
                                        string readText = System.IO.File.ReadAllText(path);
                                        readText = readText.Replace("{$title}", "主險");
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/insurance_content_main/list/" + thisGuid);
                                        rowData.Add(dataItem.Key.ToString(), readText);
                                    }
                                    else if (type == "mainblock")
                                    {
                                        rowData.Add(dataItem.Key.ToString(), "");
                                    }
                                }
                            }
                            break;

                        #endregion

                        default:
                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string readText = "<div >" + dataList[dataItem.Key.ToString()].ToString() + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                    }
                }

                //if (tables == "claims_servicelocation")
                //{
                //    var judgeCl = (from a in DB.claims_servicelocation
                //                   where a.guid == thisGuid && a.lang == defLang
                //                   select a).FirstOrDefault();

                //    var judgeCg = (from a in DB.claims_servicelocation_category
                //                   where a.guid == judgeCl.label && a.lang == defLang
                //                   select a).FirstOrDefault();
                //    if (judgeCg != null && judgeCg.status != "D")
                //    {
                //        re.Add(rowData);
                //    }
                //}
                //else
                //{
                //    re.Add(rowData);
                //}
                re.Add(rowData);
            }


            return re;
        }

        /// <summary>
        /// 新增 修改
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="form"></param>
        /// <param name="Field"></param>
        /// <returns></returns>
        public static string saveData(string tables, string form, string Field, string guid, string actType, Dictionary<String, Object> FormObj = null)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            dynamic Model = null;//目前Model
            dynamic FromData = null;//表單資訊
            string lang = "";
            // string guid = "";
            system_log system_Log = new system_log();//系統紀錄用

            switch (tables)
            {
                //首頁BANNER
                case "homebanner":
                    FromData = JsonConvert.DeserializeObject<homebanner>(form);
                    Model = model.Repository<homebanner>();

                    if (FromData.pic != null && FromData.pic != "")
                    {
                        var splitindex_urlpic = FromData.pic.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "tw")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.pic = index_urltemp;
                        }
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.homebanner.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                //影片介紹
                case "videocontent":
                    FromData = JsonConvert.DeserializeObject<videocontent>(form);
                    Model = model.Repository<videocontent>();

                    if (FromData.pic != null && FromData.pic != "")
                    {
                        var splitindex_urlpic = FromData.pic.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "tw")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.pic = index_urltemp;
                        }
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.videocontent.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                //個人長租好處
                case "benfit":
                    FromData = JsonConvert.DeserializeObject<benfit>(form);
                    Model = model.Repository<benfit>();

                    if (FromData.pic != null && FromData.pic != "")
                    {
                        var splitindex_urlpic = FromData.pic.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "tw")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.pic = index_urltemp;
                        }
                    }

                    if (FromData.pictwo != null && FromData.pictwo != "")
                    {
                        var splitindex_urlpic = FromData.pictwo.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "tw")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.pictwo = index_urltemp;
                        }
                    }

                    if (FromData.picthree != null && FromData.picthree != "")
                    {
                        var splitindex_urlpic = FromData.picthree.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "tw")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.picthree = index_urltemp;
                        }
                    }

                    if (FromData.picfour != null && FromData.picfour != "")
                    {
                        var splitindex_urlpic = FromData.picfour.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "tw")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.picfour = index_urltemp;
                        }
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.benfit.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                //租賃服務好處
                case "rentbenfit":
                    FromData = JsonConvert.DeserializeObject<rentbenfit>(form);
                    Model = model.Repository<rentbenfit>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.rentbenfit.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                //合約期間服務
                case "contact_service":
                    FromData = JsonConvert.DeserializeObject<contact_service>(form);
                    Model = model.Repository<contact_service>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.contact_service.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                //合約期滿多重選擇
                case "multipleselect":
                    FromData = JsonConvert.DeserializeObject<mulitpleselect>(form);
                    Model = model.Repository<mulitpleselect>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.multipleselect.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                //個人長租現金購車
                case "comparemoney":
                    FromData = JsonConvert.DeserializeObject<comparemoney>(form);
                    Model = model.Repository<comparemoney>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.comparemoney.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                //長期租車短期租車
                case "comparetime":
                    FromData = JsonConvert.DeserializeObject<comparetime>(form);
                    Model = model.Repository<comparetime>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.comparetime.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                //專案推薦
                case "projectsetting":
                    FromData = JsonConvert.DeserializeObject<projectsetting>(form);

                    string carguid = FromData.carstatus;
                    var carname = (from a in DB.car_status
                                   where a.guid == carguid
                                   select a.title).FirstOrDefault();
                    if (carname != null)
                    {
                        FromData.carstatusname = carname;
                    }
                    else
                    {
                        FromData.carstatusname = "";
                    }

                    string brandguid = FromData.brand;
                    var brandname = (from a in DB.brand_category
                                   where a.guid == brandguid
                                     select a.title).FirstOrDefault();
                    if (brandname != null)
                    {
                        FromData.brandname = brandname;
                    }
                    else
                    {
                        FromData.brandname = "";
                    }

                    FromData.statusname = "未設定";

                     Model = model.Repository<projectsetting>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.projectsetting.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                //租金試算
                case "rentsetting":
                    FromData = JsonConvert.DeserializeObject<rentsetting>(form);
                    Model = model.Repository<rentsetting>();

                    if (Field == "")
                    {
                        FromData.rentyear = FormObj["rentyearchange"].ToString();
                    }

                    if (FromData.price != null && FromData.price != "")
                    {
                        if (FromData.price == "0")
                        {
                            FromData.price = "0";
                        }
                        else
                        {
                            FromData.price = Int32.Parse(FromData.price.Replace(",", "")).ToString("###,###");
                        }
                    }
                    else
                    {
                        FromData.price = "0";
                    }

                    if (FromData.pricediscount != null && FromData.pricediscount != "")
                    {
                        if (FromData.pricediscount == "0")
                        {
                            FromData.pricediscount = "0";
                        }
                        else
                        {
                            FromData.pricediscount = Int32.Parse(FromData.pricediscount.Replace(",", "")).ToString("###,###");
                        }
                    }
                    else
                    {
                        FromData.pricediscount = "0";
                    }

                    if (FromData.rentmonth != null && FromData.rentmonth != "")
                    {
                        if (FromData.rentmonth == "0")
                        {
                            FromData.rentmonth = "0";
                        }
                        else
                        {
                            FromData.rentmonth = Int32.Parse(FromData.rentmonth.Replace(",", "")).ToString("###,###");
                        }
                    }
                    else
                    {
                        FromData.rentmonth = "0";
                    }

                    if (FromData.margin != null && FromData.margin != "")
                    {
                        if (FromData.margin == "0")
                        {
                            FromData.margin = "0";
                        }
                        else
                        {
                            FromData.margin = Int32.Parse(FromData.margin.Replace(",", "")).ToString("###,###");
                        }
                    }
                    else
                    {
                        FromData.margin = "0";
                    }

                    if (FromData.rebuyprice != null && FromData.rebuyprice != "")
                    {
                        if (FromData.rebuyprice == "0")
                        {
                            FromData.rebuyprice = "0";
                        }
                        else
                        {
                            FromData.rebuyprice = Int32.Parse(FromData.rebuyprice.Replace(",", "")).ToString("###,###");
                        }
                    }
                    else
                    {
                        FromData.rebuyprice = "0";
                    }

                    string renttitle = FromData.title;
                    var tempname = (from a in DB.projectsetting
                                    where a.guid == renttitle
                                    select a).FirstOrDefault();
                    if (tempname != null)
                    {
                        FromData.titlename = tempname.title;
                    }
                    else
                    {
                        FromData.titlename = "";
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.rentsetting.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                //車型
                case "carmodel":
                    FromData = JsonConvert.DeserializeObject<carmodel>(form);
                    Model = model.Repository<carmodel>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.carmodel.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                //近一步洽詢
                case "advanceform":
                    FromData = JsonConvert.DeserializeObject<advanceform>(form);
                    Model = model.Repository<advanceform>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.advanceform.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                case "MLCONTACTUS":
                    FromData = JsonConvert.DeserializeObject<MLCONTACTUS>(form);
                    Model = model.Repository<MLCONTACTUS>();
                    if (Field != "")
                    {
                        int resno = FromData.SNO;
                        //guid = FromData.SNO;
                        //lang = FromData.lang;
                        Model = DB.MLCONTACTUS.Where(m => m.SNO == resno).FirstOrDefault();
                    }
                    break;

                //用戶分享
                case "membershare":
                    FromData = JsonConvert.DeserializeObject<membershare>(form);
                    Model = model.Repository<membershare>();

                    if (FromData.pic != null && FromData.pic != "")
                    {
                        var splitindex_urlpic = FromData.pic.Split('/');
                        string index_urltemp = "";
                        int deleteindex = 0;
                        if (FromData.lang == "tw")
                        {
                            for (var i = 0; i < splitindex_urlpic.Length; i++)
                            {
                                if (splitindex_urlpic[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                splitindex_urlpic[j] = "";
                            }

                            for (var k = 0; k < splitindex_urlpic.Length; k++)
                            {
                                if (splitindex_urlpic[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + splitindex_urlpic[k];
                                }
                            }

                            FromData.pic = index_urltemp;
                        }
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.membershare.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                #region -- 最新消息 --

                //最新消息分類
                case "news_category":
                    FromData = JsonConvert.DeserializeObject<news_category>(form);
                    Model = model.Repository<news_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.news_category.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                //最新消息
                case "news":
                    FromData = JsonConvert.DeserializeObject<news>(form);
                    Model = model.Repository<news>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.news.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;

                #endregion

                #region -- 控制台 --

                //網站基本資料
                case "web_data":
                    FromData = JsonConvert.DeserializeObject<web_data>(form);
                    Model = model.Repository<web_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.web_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //SMTP資料
                case "smtp_data":
                    FromData = JsonConvert.DeserializeObject<smtp_data>(form);
                    Model = model.Repository<smtp_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.smtp_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //資源回收桶
                case "ashcan":

                    FromData = JsonConvert.DeserializeObject<ashcan>(form);
                    Model = model.Repository<ashcan>();
                    if (Field != "")
                    {
                        Guid NewGuid = Guid.Parse(FromData.guid.ToString());
                        Model = DB.ashcan.Where(m => m.guid == NewGuid).FirstOrDefault();
                    }

                    break;

                //系統參數
                case "system_data":
                    FromData = JsonConvert.DeserializeObject<system_data>(form);
                    Model = model.Repository<system_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Guid newGuid = Guid.Parse(guid);
                        Model = DB.system_data.Where(m => m.guid == newGuid).FirstOrDefault();
                    }
                    break;

                //防火牆
                case "firewalls":
                    FromData = JsonConvert.DeserializeObject<firewalls>(form);
                    Model = model.Repository<firewalls>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.firewalls.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //系統日誌
                case "system_log":
                    FromData = JsonConvert.DeserializeObject<system_log>(form);
                    Model = model.Repository<system_log>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.system_log.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                #endregion

                #region -- 帳戶資訊 --

                //使用者
                case "user":
                    FromData = JsonConvert.DeserializeObject<user>(form);
                    Model = model.Repository<user>();

                    if (FromData.thepr != null && FromData.thepr != "")
                    {
                        FromData.thepr = FunctionService.md5(FromData.thepr);
                    }
                    //無輸入密碼進入修改情況下
                    if (FromData.thepr == "" && FromData.guid != "")
                    {
                        FromData.thepr = FormObj["defPassword"].ToString();
                    }
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.user.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //群組管理
                case "roles":
                    FromData = JsonConvert.DeserializeObject<roles>(form);
                    Model = model.Repository<roles>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.roles.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                    #endregion
            }
            if (tables == "MLCONTACTUS")
            {
                FromData.UDATE = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            else
            {
                FromData.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            

            if (actType == "add")
            {
                FromData.guid = guid;
                FromData.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                Model.Create(FromData);
                Model.SaveChanges();

                system_Log.title = FunctionService.reSysLogTitle(FromData, "新增"); //回傳log標題
                var table_title = DB.system_menu.Where(x => x.tables == tables).SingleOrDefault();
                system_Log.notes = "新增" + table_title.title + "資料";
            }
            else
            {
                if (Field == "")
                {
                    if (tables == "smtp_data")
                    {
                        Model.Update(FromData);
                        Model.SaveChanges();
                    }
                    else if (tables == "roles")
                    {
                        Model.Update(FromData);
                        Model.SaveChanges();
                    }
                    else if (tables == "user")
                    {
                        Model.Update(FromData);
                        Model.SaveChanges();
                    }
                    else
                    {
                        if (tables == "MLCONTACTUS")
                        {
                            int tempsno = FromData.SNO;
                            var odata = (from a in DB.MLCONTACTUS
                                         where a.SNO == tempsno
                                         select a).FirstOrDefault();

                            string encrypt_key1 = "LEXUSADMSYS2020";
                            string str1 = "";
                            string str2 = "";
                            string str3 = "";
                            try
                            {
                                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
                                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                                SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();
                                byte[] key = sha256.ComputeHash(Encoding.UTF8.GetBytes(encrypt_key1));
                                byte[] iv = md5.ComputeHash(Encoding.UTF8.GetBytes(encrypt_key1));
                                aes.Key = key;
                                aes.IV = iv;

                                byte[] dataByteArray1 = Encoding.UTF8.GetBytes(odata.NAME);
                                byte[] dataByteArray2 = Encoding.UTF8.GetBytes(odata.EMLE);
                                byte[] dataByteArray3 = Encoding.UTF8.GetBytes(odata.MOBILEE);
                                using (MemoryStream ms = new MemoryStream())
                                using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                                {
                                    cs.Write(dataByteArray1, 0, dataByteArray1.Length);
                                    cs.FlushFinalBlock();
                                    str1 = Convert.ToBase64String(ms.ToArray());
                                }

                                using (MemoryStream ms = new MemoryStream())
                                using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                                {
                                    cs.Write(dataByteArray2, 0, dataByteArray2.Length);
                                    cs.FlushFinalBlock();
                                    str2 = Convert.ToBase64String(ms.ToArray());
                                }

                                using (MemoryStream ms = new MemoryStream())
                                using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                                {
                                    cs.Write(dataByteArray3, 0, dataByteArray3.Length);
                                    cs.FlushFinalBlock();
                                    str3 = Convert.ToBase64String(ms.ToArray());
                                }
                            }
                            catch (Exception exception1)
                            {
                                Exception exception = exception1;
                                str1 = "ERR";
                                str2 = "ERR";
                                str3 = "ERR";
                            }

                            odata.CONTENT = FromData.CONTENT;
                            odata.STATUS = FromData.STATUS;
                            odata.UDATE = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                            DB.Entry(odata).State = EntityState.Modified;
                            DB.SaveChanges();

                            //FromData.NAME = str1;
                            //FromData.EMLE = str2;
                            //FromData.MOBILEE = str3;
                            //FromData.QUESTION = "99";
                            //FromData.OVERYEAR = 0;
                            //FromData.SOURCE = 4;

                            //Model.Update(FromData);
                            //Model.SaveChanges();
                        }
                        else
                        {
                            if (FromData.lang == "en")
                            {

                            }
                            else
                            {
                                Model.Update(FromData);
                                Model.SaveChanges();
                            }
                        }
                    }


                    if (tables != "MLCONTACTUS")
                    {
                        var table_title = DB.system_menu.Where(x => x.tables == tables).SingleOrDefault();
                        system_Log.title = FunctionService.mSystemLogTitle(FromData, "修改", table_title.title);
                    }

                }
                else
                {
                    //單一欄位修改
                    switch (Field)
                    {
                        case "status":
                            if (tables == "advanceform" && FromData.lang == "en")
                            {

                            }
                            else if (tables == "rentsetting" && FromData.lang == "en")
                            {

                            }
                            else if (tables == "MLCONTACTUS")
                            {
                                Model.STATUS = 9;
                            }
                            else if (tables == "projectsetting")
                            {
                                Model.status = FromData.status;
                                if (FromData.status == "Y")
                                {
                                    Model.indoor = "上架";
                                }
                                else if (FromData.status == "N")
                                {
                                    Model.indoor = "下架";
                                }
                                else
                                {
                                    Model.indoor = "下架";
                                }
                            }
                            else
                            {
                                Model.status = FromData.status;
                            }
                            
                            break;

                        case "sortIndex":
                            Model.sortIndex = FromData.sortIndex;
                            break;

                        case "logindate":
                            Model.logindate = FromData.logindate;
                            break;
                        case "modify":
                            Model.inquirytype = FromData.inquirytype;
                            break;
                    }


                    DB.SaveChanges();
                }
            }

            if (FormObj != null)
            {
                #region 權限

                if (FormObj.ContainsKey("permissions") && FormObj["permissions"] != null && FormObj["permissions"].ToString() != "")
                {
                    NameValueCollection permissions = FunctionService.reSubmitFormDataJson(FormObj["permissions"].ToString());

                    //role_permissions role_permissions_add = new role_permissions();

                    var delData = DB.role_permissions.Where(m => m.role_guid == guid).ToList();

                    foreach (var item in delData)
                    {
                        DB.role_permissions.Remove((role_permissions)item);
                    }

                    foreach (string key in permissions)
                    {
                        //role_permissions_add.guid = Guid.NewGuid();
                        //role_permissions_add.role_guid = guid;
                        //role_permissions_add.permissions_guid = key.ToString();
                        //role_permissions_add.permissions_status = permissions[key].ToString();
                        //DB.role_permissions.Add(role_permissions_add);
                        //DB.SaveChanges();
                        role_permissions role_permissions_add = new role_permissions();
                        role_permissions_add.guid = Guid.NewGuid();
                        role_permissions_add.role_guid = guid;
                        role_permissions_add.permissions_guid = key.ToString();
                        role_permissions_add.permissions_status = permissions[key].ToString();
                        DB.Entry(role_permissions_add).State = EntityState.Added;
                        DB.SaveChanges();
                    }
                }

                #endregion
            }

            if (tables != "MLCONTACTUS")
            {
                //系統紀錄
                try
                {
                    system_Log.guid = Guid.NewGuid().ToString();
                    system_Log.ip = FunctionService.GetIP();
                    system_Log.status = "Y";
                    system_Log.types = "";
                    system_Log.tables = tables;
                    system_Log.tables_guid = guid;
                    system_Log.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    system_Log.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    Dictionary<string, string> ReUserData = FunctionService.ReUserData();
                    system_Log.username = ReUserData["username"].ToString();
                    DB.system_log.Add(system_Log);
                    DB.SaveChanges();
                }
                catch (Exception ex) { }
            }
       

            return guid;
        }

        /// <summary>
        /// 是否使用語系
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static string useLang(string tables)
        {
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //首頁BANNER
                case "homebanner":
                    re = Repository.homebanner_Repository.useLang();
                    break;

                //影片介紹
                case "videocontent":
                    re = Repository.videocontent_Repository.useLang();
                    break;

                //個人長租好處
                case "benfit":
                    re = Repository.benfit_Repository.useLang();
                    break;

                //租賃服務好處
                case "rentbenfit":
                    re = Repository.rentbenfit_Repository.useLang();
                    break;

                //合約期間服務
                case "contact_service":
                    re = Repository.contactservice_Repository.useLang();
                    break;

                //合約期滿多重選擇
                case "multipleselect":
                    re = Repository.multipleselect_Repository.useLang();
                    break;

                //個人長租現金購車
                case "comparemoney":
                    re = Repository.comparemoney_Repository.useLang();
                    break;

                //長期租車短期租車
                case "comparetime":
                    re = Repository.comparetime_Repository.useLang();
                    break;

                //專案推薦
                case "projectsetting":
                    re = Repository.projectsetting_Repository.useLang();
                    break;

                //租金試算
                case "rentsetting":
                    re = Repository.rentsetting_Repository.useLang();
                    break;

                //車型
                case "carmodel":
                    re = Repository.carmodel_Repository.useLang();
                    break;

                //近一步洽詢
                case "advanceform":
                    re = Repository.advanceform_Repository.useLang();
                    break;

                case "MLCONTACTUS":
                    re = Repository.mlcontentus_Repository.useLang();
                    break;

                //用戶分享
                case "membershare":
                    re = Repository.membershare_Repository.useLang();
                    break;

                //最新消息分類
                case "news_category":
                    re = Repository.newsCategory__Repository.useLang();
                    break;

                //最新消息
                case "news":
                    re = Repository.news__Repository.useLang();
                    break;

                #region -- 控制台 --

                //網站基本資料
                case "web_data":
                    re = Repository.webData__Repository.useLang();
                    break;

                //SMTP資料
                case "smtp_data":
                    re = Repository.smtpData__Repository.useLang();
                    break;

                //資源回收桶
                case "ashcan":
                    re = Repository.ashcan__Repository.useLang();
                    break;

                //防火牆設定
                case "firewalls":
                    re = Repository.firewalls__Repository.useLang();
                    break;

                //系統日誌
                case "system_log":
                    re = Repository.systemLog__Repository.useLang();
                    break;

                #endregion

                #region -- 帳戶資訊 --

                //帳戶管理
                case "user":
                    re = Repository.user__Repository.useLang();
                    break;

                //群組管理
                case "roles":
                    re = Repository.roles__Repository.useLang();
                    break;

                    #endregion
            }

            return re;
        }

        /// <summary>
        /// 取得DataTable欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic dataTableTitle(string tables)
        {
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //首頁BANNER
                case "homebanner":
                    re = Repository.homebanner_Repository.dataTableTitle();
                    break;

                //個人長租好處
                case "benfit":
                    re = Repository.benfit_Repository.dataTableTitle();
                    break;

                //租賃服務好處
                case "rentbenfit":
                    re = Repository.rentbenfit_Repository.dataTableTitle();
                    break;

                //合約期間服務
                case "contact_service":
                    re = Repository.contactservice_Repository.dataTableTitle();
                    break;

                //合約期滿多重選擇
                case "multipleselect":
                    re = Repository.multipleselect_Repository.dataTableTitle();
                    break;

                //個人長租現金購車
                case "comparemoney":
                    re = Repository.comparemoney_Repository.dataTableTitle();
                    break;

                //長期租車短期租車
                case "comparetime":
                    re = Repository.comparetime_Repository.dataTableTitle();
                    break;

                //專案推薦
                case "projectsetting":
                    re = Repository.projectsetting_Repository.dataTableTitle();
                    break;

                //租金試算
                case "rentsetting":
                    re = Repository.rentsetting_Repository.dataTableTitle();
                    break;

                //車型
                case "carmodel":
                    re = Repository.carmodel_Repository.dataTableTitle();
                    break;

                //近一步洽詢
                case "advanceform":
                    re = Repository.advanceform_Repository.dataTableTitle();
                    break;

                case "MLCONTACTUS":
                    re = Repository.mlcontentus_Repository.dataTableTitle();
                    break;

                //用戶分享
                case "membershare":
                    re = Repository.membershare_Repository.dataTableTitle();
                    break;

                //影片介紹
                case "videocontent":
                    re = Repository.videocontent_Repository.dataTableTitle();
                    break;

                //最新消息分類
                case "news_category":
                    re = Repository.newsCategory__Repository.dataTableTitle();
                    break;

                //最新消息
                case "news":
                    re = Repository.news__Repository.dataTableTitle();
                    break;

                #region -- 控制台 --

                //資源回收桶
                case "ashcan":
                    re = Repository.ashcan__Repository.dataTableTitle();
                    break;

                //防火牆
                case "firewalls":
                    re = Repository.firewalls__Repository.dataTableTitle();
                    break;

                //系統日誌
                case "system_log":
                    re = Repository.systemLog__Repository.dataTableTitle();
                    break;

                #endregion

                #region -- 帳戶資訊 --

                //使用者
                case "user":
                    re = Repository.user__Repository.dataTableTitle();
                    break;

                //群組管理
                case "roles":
                    re = Repository.roles__Repository.dataTableTitle();
                    break;

                    #endregion
            }

            return re;
        }

        /// <summary>
        /// 取得列表說明
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic listMessage(string tables)
        {
            dynamic re = null;
            switch (tables)
            {
                //防火牆
                case "firewalls":
                    re = Repository.firewalls__Repository.listMessage();
                    break;

                default:
                    re = "";
                    break;
            }

            return re;
        }

        /// <summary>
        /// 欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getColData(string tables, dynamic data)
        {
            dynamic re = null;
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //首頁BANNER
                case "homebanner":
                    re = Repository.homebanner_Repository.colFrom();
                    break;

                //影片介紹
                case "videocontent":
                    re = Repository.videocontent_Repository.colFrom();
                    break;

                //個人長租好處
                case "benfit":
                    re = Repository.benfit_Repository.colFrom();
                    break;

                //租賃服務好處
                case "rentbenfit":
                    re = Repository.rentbenfit_Repository.colFrom();
                    break;

                //合約期間服務
                case "contact_service":
                    re = Repository.contactservice_Repository.colFrom();
                    break;

                //合約期滿多重選擇
                case "multipleselect":
                    re = Repository.multipleselect_Repository.colFrom();
                    break;

                //個人長租現金購車
                case "comparemoney":
                    re = Repository.comparemoney_Repository.colFrom();
                    break;

                //長期租車短期租車
                case "comparetime":
                    re = Repository.comparetime_Repository.colFrom();
                    break;

                //專案推薦
                case "projectsetting":
                    re = Repository.projectsetting_Repository.colFrom();
                    break;

                //租金試算
                case "rentsetting":
                    re = Repository.rentsetting_Repository.colFrom();
                    break;

                //車型
                case "carmodel":
                    re = Repository.carmodel_Repository.colFrom();
                    break;

                //近一步洽詢
                case "advanceform":
                    re = Repository.advanceform_Repository.colFrom();
                    break;

                case "MLCONTACTUS":
                    re = Repository.mlcontentus_Repository.colFrom();
                    break;

                //用戶分享
                case "membershare":
                    re = Repository.membershare_Repository.colFrom();
                    break;

                //最新消息分類
                case "news_category":
                    re = Repository.newsCategory__Repository.colFrom();
                    break;

                //最新消息
                case "news":
                    re = Repository.news__Repository.colFrom();
                    break;

                #region -- 控制台 --

                //網站基本資料
                case "web_data":
                    re = Repository.webData__Repository.colFrom();
                    break;

                //SMTP資料
                case "smtp_data":
                    re = Repository.smtpData__Repository.colFrom();
                    break;

                //資源回收桶
                case "ashcan":
                    re = Repository.ashcan__Repository.colFrom();
                    break;

                //防火牆
                case "firewalls":
                    re = Repository.firewalls__Repository.colFrom();
                    break;

                //系統日誌
                case "system_log":
                    re = Repository.systemLog__Repository.colFrom();
                    break;

                #endregion

                #region -- 帳戶資訊 --

                //帳戶管理
                case "user":
                    re = Repository.user__Repository.colFrom();
                    break;

                //群組管理
                case "roles":
                    re = Repository.roles__Repository.colFrom();
                    break;

                //後台個人資料
                case "edituser":
                    re = Repository.user__Repository.EditcolFrom();
                    break;

                #endregion

                #region -- Siteadmin --

                //系統參數
                case "system_data":
                    re = Repository.systemData__Repository.colFrom();
                    break;

                    #endregion
            }

            return re;
        }

        /// <summary>
        /// 取得資料庫資料
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getPresetData(string tables, string guid)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            string re = "";

            Dictionary<String, Object> reValData = new Dictionary<String, Object>();
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //首頁BANNER
                case "homebanner":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<homebanner>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //影片介紹
                case "videocontent":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<videocontent>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //個人長租好處
                case "benfit":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<benfit>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //租賃服務好處
                case "rentbenfit":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<rentbenfit>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //合約期間服務
                case "contact_service":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<contact_service>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //合約期滿多重選擇
                case "multipleselect":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<mulitpleselect>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //最新消息分類
                case "news_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<news_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //個人長租現金購車
                case "comparemoney":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<comparemoney>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //長期租車短期租車
                case "comparetime":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<comparetime>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //專案推薦
                case "projectsetting":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<projectsetting>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //租金試算
                case "rentsetting":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<rentsetting>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //車型
                case "carmodel":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<carmodel>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //近一步洽詢
                case "advanceform":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<advanceform>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                case "MLCONTACTUS":
                    if (guid != null && guid != "")
                    {
                        int resno = Int32.Parse(guid);
                        var data = model.Repository<MLCONTACTUS>();
                        var tempData = data.ReadsWhere(m => m.SNO == resno).FirstOrDefault();

                        List<MLCONTACTUS> mlcontactus = new List<MLCONTACTUS>();
                        string encrypt_key1 = "LEXUSADMSYS2020";
                        string str1 = "";
                        string decrypt = "";

                        if (tempData.REPLAYTP != null)
                        {
                            if (tempData.REPLAYTP == 1)
                            {
                                tempData.QUESTION = "行動電話";
                            }
                            else if (tempData.REPLAYTP == 2)
                            {
                                tempData.QUESTION = "E-MAIL";
                            }
                            else if (tempData.REPLAYTP == 3)
                            {
                                tempData.QUESTION = "兩者皆可";
                            }
                        }

                        try
                        {
                            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
                            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                            SHA256CryptoServiceProvider sha256 = new SHA256CryptoServiceProvider();
                            byte[] key = sha256.ComputeHash(Encoding.UTF8.GetBytes(encrypt_key1));
                            byte[] iv = md5.ComputeHash(Encoding.UTF8.GetBytes(encrypt_key1));
                            aes.Key = key;
                            aes.IV = iv;

                            byte[] dataByteArray = Convert.FromBase64String(tempData.NAME);
                            byte[] dataByteArray2 = Convert.FromBase64String(tempData.EMLE);
                            byte[] dataByteArray3 = Convert.FromBase64String(tempData.MOBILEE);
                            using (MemoryStream ms = new MemoryStream())
                            {
                                using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                                {
                                    cs.Write(dataByteArray, 0, dataByteArray.Length);
                                    cs.FlushFinalBlock();
                                    str1 = Encoding.UTF8.GetString(ms.ToArray());
                                    tempData.NAME = str1;
                                }
                            }
                            using (MemoryStream ms = new MemoryStream())
                            {
                                using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                                {
                                    cs.Write(dataByteArray2, 0, dataByteArray2.Length);
                                    cs.FlushFinalBlock();
                                    str1 = Encoding.UTF8.GetString(ms.ToArray());
                                    tempData.EMLE = str1;
                                }
                            }
                            using (MemoryStream ms = new MemoryStream())
                            {
                                using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                                {
                                    cs.Write(dataByteArray3, 0, dataByteArray3.Length);
                                    cs.FlushFinalBlock();
                                    str1 = Encoding.UTF8.GetString(ms.ToArray());
                                    tempData.MOBILEE = str1;
                                }
                            }
                            mlcontactus.Add(tempData);
                        }
                        catch (Exception exception1)
                        {
                            Exception exception = exception1;
                            str1 = "ERR";
                        }


                        re = JsonConvert.SerializeObject(mlcontactus, Formatting.Indented);
                    }
                    break;

                //用戶分享
                case "membershare":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<membershare>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //最新消息
                case "news":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<Models.news>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                #region -- 控制台 --

                //網站基本資料
                case "web_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<web_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //SMTP資料
                case "smtp_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<smtp_data>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //防火牆
                case "firewalls":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<Models.firewalls>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //系統日誌
                case "system_log":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_log>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                #endregion

                #region -- 帳戶資訊 --

                //使用者
                case "user":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<Models.user>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //群組管理
                case "roles":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<Models.roles>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                #endregion

                #region -- Siteadmin --

                //系統參數
                case "system_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_data>();
                        Guid newGuid = Guid.Parse(guid);
                        var tempData = data.ReadsWhere(m => m.guid == newGuid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                    #endregion
            }

            return re;
        }

        /// <summary>
        /// 取得分類
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getCategory(string tables, dynamic data = null)
        {
            Model DB = new Model();
            dynamic re = null;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            switch (tables)
            {
                //專案名稱
                case "project_title_all":
                    if (tables != null && tables != "")
                    {
                        re = DB.projectsetting.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                    }
                    break;

                //專案名稱
                case "project_title":
                    if (tables != null && tables != "")
                    {
                        List<projectsetting> resultProj = new List<projectsetting>();
                        var tempallproj = (from a in DB.projectsetting
                                           where a.status == "Y" && a.lang == "tw"
                                           select a).ToList();
                        foreach (var item in tempallproj)
                        {
                            var tempisrent = (from a in DB.rentsetting
                                              where a.title == item.guid && a.status == "Y" && a.lang == "tw"
                                              select a).ToList();
                            if (tempisrent.Count() != 0)
                            {
                                resultProj.Add(item);
                            }
                        }

                        //re = DB.projectsetting.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                        re = resultProj;
                    }
                    break;

                //車況分類
                case "car_status":
                    if (tables != null && tables != "")
                    {
                        re = DB.car_status.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                    }
                    break;

                //廠牌
                case "brand_category":
                    if (tables != null && tables != "")
                    {
                        re = DB.brand_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                    }
                    break;

                //月租金
                case "monthrent_category":
                    if (tables != null && tables != "")
                    {
                        re = DB.monthrent_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                    }
                    break;

                //最新消息分類
                case "news_category":
                    if (tables != null && tables != "")
                    {
                        re = DB.news_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                    }
                    break;

                //群組
                case "roles":
                    if (tables != null && tables != "")
                    {
                        re = DB.roles.Where(m => m.status == "Y").ToList();
                    }
                    break;

                //使用者
                case "user":
                    if (tables != null && tables != "")
                    {
                        re = DB.user.Where(m => m.status == "Y").ToList();
                    }
                    break;
            }

            return re;
        }

        /// <summary>
        /// 回傳標題
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getDataTitle(string tables, string guid)
        {
            Model DB = new Model();
            string re = "";
            

            return re;
        }

        /// <summary>
        /// 複選選單選取判斷用
        /// </summary>
        /// <param name="tabels"></param>
        /// <param name="user_guid"></param>
        /// <param name="forum_guid"></param>
        /// <returns></returns>
        public static string selectMultipleSelected(string tabels, string user_guid, string forum_guid)
        {
            Model DB = new Model();

            string selected = "";
            /*
            switch(tabels)
            {
                case "forum_message":

                    int data = DB.forum_message.Where(m=>m.user_guid == user_guid).Where(m=>m.forum_guid == forum_guid).Select(a => new { guid = a.guid }).Count();
                    if(data > 0)
                    {
                        selected = " selected";
                    }
                    break;
            }
            */

            return selected;
        }

        /// <summary>
        /// 取得預設排序
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy(string tables)
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                //首頁BANNER
                case "homebanner":
                    re.Clear();
                    re = Repository.homebanner_Repository.defaultOrderBy();
                    break;

                //影片介紹
                case "videocontent":
                    re.Clear();
                    re = Repository.videocontent_Repository.defaultOrderBy();
                    break;

                //個人長租好處
                case "benfit":
                    re.Clear();
                    re = Repository.benfit_Repository.defaultOrderBy();
                    break;

                //租賃服務好處
                case "rentbenfit":
                    re = Repository.rentbenfit_Repository.defaultOrderBy();
                    break;

                //進一步洽詢
                case "advanceform":
                    re = Repository.advanceform_Repository.defaultOrderBy();
                    break;

                case "MLCONTACTUS":
                    re = Repository.mlcontentus_Repository.defaultOrderBy();
                    break;

                //合約期間服務
                case "contact_service":
                    re = Repository.contactservice_Repository.defaultOrderBy();
                    break;

                //合約期滿多重選擇
                case "multipleselect":
                    re = Repository.multipleselect_Repository.defaultOrderBy();
                    break;

                //個人長租現金購車
                case "comparemoney":
                    re = Repository.comparemoney_Repository.defaultOrderBy();
                    break;

                //長期租車短期租車
                case "comparetime":
                    re = Repository.comparetime_Repository.defaultOrderBy();
                    break;

                //專案推薦
                case "projectsetting":
                    re = Repository.projectsetting_Repository.defaultOrderBy();
                    break;

                //租金試算
                case "rentsetting":
                    re = Repository.rentsetting_Repository.defaultOrderBy();
                    break;

                //車型
                case "carmodel":
                    re = Repository.carmodel_Repository.defaultOrderBy();
                    break;

                ////近一步洽詢
                //case "advanceform":
                //    re = Repository.advanceform_Repository.useLang();
                //    break;

                //用戶分享
                case "membershare":
                    re = Repository.membershare_Repository.defaultOrderBy();
                    break;

                //最新消息類別
                case "news_category":
                    re.Clear();
                    re = Repository.newsCategory__Repository.defaultOrderBy();
                    break;

                //最新消息內容
                case "news":
                    re.Clear();
                    re = Repository.news__Repository.defaultOrderBy();
                    break;

                #region -- 控制台 --

                //防火牆
                case "firewalls":
                    re.Clear();
                    re = Repository.firewalls__Repository.defaultOrderBy();
                    break;

                //系統日誌
                case "system_log":
                    re.Clear();
                    re = Repository.systemLog__Repository.defaultOrderBy();
                    break;

                    #endregion
            }
            return re;
        }

        /// <summary>
        /// 格式化排序欄位
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="orderByKey"></param>
        /// <returns></returns>
        public static string formatOrderByKey(string tables, string orderByKey)
        {
            string re = "";
            switch (orderByKey)
            {
                case "info":
                    re = "title";
                    break;

                case "view_info":
                    re = "nums";
                    break;

                case "forum_status":
                    re = "status";
                    break;

                case "action":
                    re = "guid";
                    break;

                case "user":
                    re = "user_guid";
                    break;

                case "category":
                    re = "category";
                    if (tables == "package_content")
                    {
                        re = "package_guid";
                    }
                    break;

                default:
                    re = orderByKey;
                    break;
            }
            return HttpUtility.HtmlEncode(re);
        }

        /// <summary>
        /// 回傳資料表的標題或姓名
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static string tableTitleKey(string tables, dynamic dataList)
        {
            Model DB = new Model();
            string re = "";
            switch (tables)
            {
                case "user":
                    re = dataList["name"];
                    break;

                default:
                    re = dataList["title"];
                    break;
            }
            return HttpUtility.HtmlEncode(re);
        }
    }
}