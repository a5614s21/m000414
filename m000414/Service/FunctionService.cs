﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.Resources;
using System.Reflection;
using System.Globalization;
using System.Collections;
using System.Net.Mail;
using System.Web.Caching;
using System.Text.RegularExpressions;
using System.IO;

namespace Web.Service
{
    public class FunctionService : Controller
    {
        // GET: FunctionService


        /// <summary>
        /// 儲存LOG
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="files"></param>
        //public static void saveLog(dynamic ex, string files)
        //{



        //    string lines = "";
        //    string fileName = System.Web.Hosting.HostingEnvironment.MapPath("~") + "/Log/" + files;

        //    if (!System.IO.File.Exists(fileName))
        //    {
        //        System.IO.File.Create(fileName);
        //    }


        //    if (System.IO.File.Exists(fileName))
        //    {
        //        StreamReader str = new StreamReader(fileName);
        //        str.ReadLine();
        //        lines = str.ReadToEnd();
        //        str.Close();
        //    }

        //    lines += DateTime.Now.ToString() + "：Message => " + ex.Message + "，";
        //    lines += $"Main exception occurs {ex}.";
        //    lines += "\r\n";
        //    System.IO.File.WriteAllText(fileName, lines);
        //}

        public static string getWebUrl()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string webURL = "http://" + context.Request.ServerVariables["Server_Name"];
            if (context.Request.ServerVariables["Server_Name"] == "localhost")
            {
                webURL = webURL + $":{context.Request.Url.Port}";
            }
            else if (context.Request.ServerVariables["Server_Name"].IndexOf("iis.24241872.tw") != -1)
            {
                webURL = "http://iis.24241872.tw/projects/public/m000414/test";
            }
            else if (context.Request.ServerVariables["Server_Name"].IndexOf("south-china.com.tw") != -1)
            {
                webURL = "https://www.south-china.com.tw";
            }
            return webURL;
        }

        /// <summary>
        /// 儲存LOG
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="files"></param>
        public static void saveLog(dynamic ex, string files)
        {
            try
            {
                string lines = "";
                string fileName = System.Web.Hosting.HostingEnvironment.MapPath("~") + "/Log/" + files;

                if (!System.IO.File.Exists(fileName))
                {
                    System.IO.File.Create(fileName);
                }

                if (System.IO.File.Exists(fileName))
                {
                    StreamReader str = new StreamReader(fileName);
                    str.ReadLine();
                    lines = str.ReadToEnd();
                    str.Close();
                }

                lines += DateTime.Now.ToString() + "：Message => " + ex.Message + "，";
                lines += $"Main exception occurs {ex}.";
                lines += "\r\n";
                System.IO.File.WriteAllText(fileName, lines);
            }
            catch
            {
            }
        }

        /// <summary>
        /// 縣市區域
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
        public static List<taiwan_city> TaiwanCitys(string city)
        {
            Model DB = new Model();
            List<taiwan_city> re = new List<taiwan_city>();

            if (city == "")
            {
                re = DB.taiwan_city.Where(m => m.lang == "tw").Where(m => m.lay == "1").ToList();
            }
            else
            {
                re = DB.taiwan_city.Where(m => m.lang == "tw").Where(m => m.city == city).Where(m => m.lay == "2").ToList();
            }


            return re;
        }

        /// <summary>
        /// 多圖處理
        /// </summary>
        /// <param name="pic"></param>
        /// <param name="pic_alt"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> PluralImage(string pic, string pic_alt)
        {
            Dictionary<String, Object> reData = new Dictionary<String, Object>();
            List<string> pics = pic.Split(',').ToList();

            List<string> pics_alt = new List<string>();

            List<string> pics_alt_temp = new List<string>();

            if (pic_alt != null && pic_alt != "")
            {
                pics_alt_temp = pic_alt.Split('§').ToList();
            }
            int i = 0;
            foreach (var item in pics)
            {
                if (pic_alt != null && pic_alt != "" && pics_alt_temp[i] != null)
                {
                    pics_alt.Add(pics_alt_temp[i]);
                }
                else
                {
                    pics_alt.Add("　");
                }

                i++;
            }

            reData.Add("pic", pics);
            reData.Add("pic_alt", pics_alt);

            return reData;
        }

        /// <summary>
        /// SEO資料格式化
        /// </summary>
        /// <param name="web_Data"></param>
        /// <param name="title"></param>
        /// <param name="seo_keywords"></param>
        /// <param name="seo_description"></param>
        /// <returns></returns>
        public static Dictionary<String, String> SEO(Models.web_data web_Data, string title, string seo_keywords, string seo_description)
        {
            Dictionary<String, String> reData = new Dictionary<String, String>();

            reData.Add("title", web_Data.title);
            reData.Add("seo_keywords", web_Data.seo_keywords);
            reData.Add("seo_description", web_Data.seo_description);

            if (title != "")
            {
                reData["title"] = title + "|" + reData["title"].ToString();
            }

            if (seo_keywords != "")
            {
                reData["seo_keywords"] = seo_keywords;
            }

            if (seo_description != "")
            {
                reData["seo_description"] = seo_description;
            }

            return reData;
        }

        /// <summary>
        /// 取得語系黨
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public static NameValueCollection getLangData(string lang)
        {
            NameValueCollection langData = new NameValueCollection();
            if (lang != "")
            {
                lang = "_" + lang;
            }
            ResourceManager rm = new ResourceManager("Web.App_GlobalResources.Resource" + lang, Assembly.GetExecutingAssembly());
            ResourceSet resourceSet = rm.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            foreach (DictionaryEntry entry in resourceSet)
            {
                langData.Add(entry.Key.ToString(), entry.Value.ToString());
            }

            return langData;
        }

        /// <summary>
        /// 格式化檔案更新日期
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        public static string FileModifyTime(string files)
        {
            
            DateTime now = DateTime.Now;
            var y = now.Year;
            var m = now.Month;
            var d = now.Day;
            var h = now.Hour;
            var min = now.Minute;
            var s = now.Second;
            return y.ToString()+ m.ToString() + d.ToString() + h.ToString() + min.ToString() + s.ToString();
        }

        /// <summary>
        /// GUID樣式
        /// </summary>
        /// <returns></returns>
        public static string getGuid()
        {
            string GuidType = ConfigurationManager.ConnectionStrings["GuidType"].ConnectionString;

            if (GuidType == "System")
            {
                return Guid.NewGuid().ToString();
            }
            else
            {
                return DateTime.Now.ToString("yyMMddHHmmssff");
            }

        }

        /// <summary>
        /// 表單JSON轉換
        /// </summary>
        /// <param name="jsonText"></param>
        /// <returns></returns>
        public static NameValueCollection reSubmitFormDataJson(string jsonText)
        {

            NameValueCollection reData = new NameValueCollection();


            dynamic dynObj = JsonConvert.DeserializeObject(HttpUtility.UrlDecode(jsonText));
            foreach (var item in dynObj)
            {
                if ((string)item.name != "")
                {
                    if (reData[(string)item.name] == null)
                    {
                        string val = item.value;
                        reData.Add((string)item.name, val);
                    }
                    else
                    {
                        string val = reData[(string)item.name] + "," + item.value;
                        reData.Remove((string)item.name);
                        reData.Add((string)item.name, val);
                    }

                }

            }


            return reData;

        }

        /// <summary>
        /// md5
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string md5(string str)
        {
            string reStr = "";


            byte[] Original = Encoding.Default.GetBytes(str); //將字串來源轉為Byte[] 
            //MD5 s1 = MD5.Create(); //使用MD5 
            //byte[] Change = s1.ComputeHash(Original);//進行加密 
            //reStr = Convert.ToBase64String(Change);//將加密後的字串從byte[]轉回string

            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] change = sha1.ComputeHash(Original);
            sha1.Dispose();
            reStr = BitConverter.ToString(change);
            reStr = reStr.Replace("-","");

            return HttpUtility.HtmlEncode(reStr);
        }

        /// <summary>
        /// 後台登入判斷
        /// </summary>
        /// <param name="sysUsername"></param>
        /// <param name="adID"></param>
        /// <returns></returns>
        public static bool systemUserCheck()
        {
            Model DB = new Model();

            bool re = false;
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string adminCathType = ConfigurationManager.ConnectionStrings["adminCathType"].ConnectionString; //後台登入記錄方式 Session or Cookie
            string username = "";


            if (context.Session["sysUsername"] != null || context.Request.Cookies["sysLogin"] != null)
            {
                if (adminCathType == "Session")
                {
                    if (context.Session["sysUsername"] != null && context.Session["sysUsername"].ToString() != "")
                    {
                        re = true;
                        username = System.Web.HttpContext.Current.Session["sysUsername"].ToString();
                    }
                }
                else
                {

                    if (context.Request.Cookies["sysLogin"] != null)
                    {
                        re = true;
                        HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                        string[] tempAccount = aCookie.Value.Split('&');
                        username = context.Server.HtmlEncode(tempAccount[0].Replace("sysUsername=", ""));
                    }
                }

                if (username != "")
                {
                    //更新登入日期
                    try
                    {
                        var saveData = DB.user.Where(m => m.username == username).Where(m => m.status == "Y").FirstOrDefault();
                        saveData.logindate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        DB.SaveChanges();

                        getAdminPermissions(username);//權限
                    }
                    catch { }

                }

            }
            //IP判斷
            if (re == true && username != "sysadmin")
            {
                /*
                SOGOEntities DB = new SOGOEntities();
                var ipLock = DB.sogo_ip_lock.Where(m => m.status == "Y").ToList();

                if (ipLock.Count > 0)
                {
                    string ip = GetIP();
                    var ipLock2 = DB.sogo_ip_lock.Where(m => m.status == "Y").Where(m => m.ip == ip).ToList();
                    if (ipLock2.Count > 0)
                    {
                        re = true;
                    }
                    else
                    {
                        re = false;
                    }

                }*/

            }
            return re;

        }

        /// <summary>
        /// 後台登入判斷 (是否第一次登入 OR 90天未變更密碼)
        /// </summary>
        /// <returns></returns>
        public static string checkfirstlogin_ninety()
        {
            Model DB = new Model();
            User__Service userService = new User__Service();

            string re = "";
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string adminCathType = ConfigurationManager.ConnectionStrings["adminCathType"].ConnectionString; //後台登入記錄方式 Session or Cookie
            string username = "";

            if (context.Session["sysUsername"] != null || context.Request.Cookies["sysLogin"] != null)
            {
                if (adminCathType == "Session")
                {
                    if (context.Session["sysUsername"] != null && context.Session["sysUsername"].ToString() != "")
                    {
                        username = System.Web.HttpContext.Current.Session["sysUsername"].ToString();
                        re = userService.CheckFirstLog_Ninety(username);
                    }
                }
                else
                {
                    if (context.Request.Cookies["sysLogin"] != null)
                    {
                        HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                        string[] tempAccount = aCookie.Value.Split('&');
                        username = context.Server.HtmlEncode(tempAccount[0].Replace("sysUsername=", ""));
                        re = userService.CheckFirstLog_Ninety(username);
                    }
                }
            }

            return re;
        }

        /// <summary>
        /// 回傳登入者帳號
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, String> ReUserData()
        {

            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string adminCathType = ConfigurationManager.ConnectionStrings["adminCathType"].ConnectionString; //後台登入記錄方式 Session or Cookie


            Dictionary<String, String> uData = new Dictionary<string, string>();

            if (adminCathType == "Session")
            {
                if (System.Web.HttpContext.Current.Session["sysUsername"] != null && System.Web.HttpContext.Current.Session["sysUsername"].ToString() != "")
                {

                    uData.Add(HttpUtility.HtmlEncode("username"), HttpUtility.HtmlEncode(System.Web.HttpContext.Current.Session["sysUsername"].ToString()));
                    uData.Add(HttpUtility.HtmlEncode("guid"), HttpUtility.HtmlEncode(System.Web.HttpContext.Current.Session["sysUserGuid"].ToString()));
                }
            }
            else
            {

                if (context.Request.Cookies["sysLogin"] != null)
                {
                    HttpCookie aCookie = context.Request.Cookies["sysLogin"];
                    string[] tempct = aCookie.Value.Split('&');


                    uData.Add(HttpUtility.HtmlEncode("username"), HttpUtility.HtmlEncode(tempct[0].Replace("sysUsername=", "").ToString()));
                    uData.Add(HttpUtility.HtmlEncode("guid"), HttpUtility.HtmlEncode(tempct[1].Replace("sysUserGuid=", "").ToString()));
                }
            }



            return uData;
        }

        /// <summary>
        /// 角色權限
        /// </summary>
        /// <param name="role_guid"></param>
        /// <returns></returns>
        public static NameValueCollection role_permissions(string role_guid)
        {
            Model DB = new Model();
            string site = ConfigurationManager.ConnectionStrings["site"].ConnectionString;
            string changerole_guid = HttpUtility.HtmlEncode(role_guid);
            var data = DB.role_permissions.Where(m => m.role_guid == changerole_guid).ToList();

            NameValueCollection re = new NameValueCollection();
            foreach (var item in data)
            {
                re.Add(HttpUtility.HtmlEncode(item.permissions_guid), HttpUtility.HtmlEncode(item.permissions_status));
            }
            //return HttpUtility.ParseQueryString(HttpUtility.HtmlEncode(re));
            return re;
        }

        /// <summary>
        /// 設定權限群組
        /// </summary>
        /// <param name="username"></param>
        public static void getAdminPermissions(string username)
        {
            Model DB = new Model();
            System.Web.HttpContext context = System.Web.HttpContext.Current;

            Dictionary<String, Object> permissions = new Dictionary<String, Object>();
            List<String> permissionsTop = new List<String>();

            context.Session.Remove("verify");

            //非系統權限
            if (username != "sysadmin")
            {
                //取得權限
                var tempUser = DB.user.Where(m => m.username == username).Select(a => new { role_guid = a.role_guid }).FirstOrDefault();
                if (tempUser != null)
                {
                    string role_guid = tempUser.role_guid;
                    var roleData = DB.roles.Where(m => m.guid == role_guid.ToString()).FirstOrDefault();
                    var perData = DB.role_permissions.Where(m => m.role_guid == role_guid.ToString()).ToList();

                    //審核權限

                    context.Session.Add("verify", roleData.verify);

                    if (perData.Count > 0)
                    {
                        foreach (var item in perData)
                        {
                            string systemMenuGuid = item.permissions_guid;

                            var tempData = DB.system_menu.Where(m => m.guid == systemMenuGuid).Select(a => new { category = a.category }).FirstOrDefault();
                            if (tempData != null)
                            {
                                permissions.Add(item.permissions_guid, item.permissions_status);
                                if (permissionsTop.IndexOf(tempData.category) == -1)
                                {
                                    permissionsTop.Add(tempData.category);
                                }
                            }
                        }

                        context.Session.Remove("permissions");
                        context.Session.Remove("permissionsTop");

                        context.Session.Add("permissions", permissions);
                        context.Session.Add("permissionsTop", permissionsTop);
                    }
                }
            }
            else
            {
                context.Session.Add("verify", "N");

                var menuData = DB.system_menu.Where(m => m.category != "0").Select(a => new { category = a.category, guid = a.guid }).ToList();

                if (menuData.Count > 0)
                {
                    foreach (var item in menuData)
                    {
                        permissions.Add(item.guid, "F");
                        if (permissionsTop.IndexOf(item.category) == -1)
                        {
                            permissionsTop.Add(item.category);
                        }
                    }

                    context.Session.Remove("permissions");
                    context.Session.Remove("permissionsTop");

                    context.Session.Add("permissions", permissions);
                    context.Session.Add("permissionsTop", permissionsTop);
                }
            }
        }

        /// <summary>
        /// 取得頁碼
        /// </summary>
        /// <param name="URL"></param>
        /// <param name="Allpage"></param>
        /// <param name="page"></param>
        /// <param name="inPage"></param>
        /// <param name="totalNum"></param>
        /// <param name="addpach"></param>
        /// <returns></returns>
        public static string getPageNum(string URL, int Allpage, int page, int totalNum, string addpach)
        {
            if (Allpage != 0)
            {
                string PageList = "";

                int countPage = Allpage;
                int startPage = 1;
                int pageSec = Convert.ToInt16(Math.Ceiling((double)page / 5));

                System.Web.HttpContext context = System.Web.HttpContext.Current;
                if (context.Request.ServerVariables["Server_Name"].IndexOf("iis.24241872.tw") != -1)
                {
                    //第一頁
                    PageList += "<li class=\"arrow prev-prev disabled\"><a class=\"d-block\" title=\"第一頁\" href=\"" + HttpUtility.HtmlEncode(URL) + "&page=1" + "\" tabindex=\"0\" aria-disabled=\"true\"><img class=\"svg\" src=\"http://iis.24241872.tw/projects/public/m000272/test/styles/images/common/arrow-left-left.svg\" alt=\"第一頁\"><span class=\"sr-only\">第一頁</span></a></li>";

                    //上一頁
                    if (page == 1)
                    {
                        PageList += "<li class=\"arrow prev disabled\"><a class=\"d-block\" title=\"上一頁\" href=\"" + HttpUtility.HtmlEncode(URL) + "&page=1" + "\" tabindex=\"0\" aria-disabled=\"true\"><img class=\"svg\" src=\"http://iis.24241872.tw/projects/public/m000272/test/styles/images/common/arrow-left.svg\" alt=\"上一頁\"><span class=\"sr-only\">上一頁</span></a></li>";
                    }
                    else
                    {
                        PageList += "<li class=\"arrow prev disabled\"><a class=\"d-block\" title=\"第"+ (page -1).ToString() +"頁\" href=\"" + HttpUtility.HtmlEncode(URL) + "&page=" + (page - 1).ToString() + "\" tabindex=\"0\" aria-disabled=\"true\"><img class=\"svg\" src=\"http://iis.24241872.tw/projects/public/m000272/test/styles/images/common/arrow-left.svg\" alt=\"上一頁\"><span class=\"sr-only\">上一頁</span></a></li>";
                    }
                }
                else
                {
                    //第一頁
                    PageList += "<li class=\"arrow prev-prev disabled\"><a class=\"d-block\" title=\"第一頁\" href=\"" + HttpUtility.HtmlEncode(URL) + "&page=1" + "\" tabindex=\"0\" aria-disabled=\"true\"><img class=\"svg\" src=\"/styles/images/common/arrow-left-left.svg\" alt=\"第一頁\"><span class=\"sr-only\">第一頁</span></a></li>";

                    //上一頁
                    if (page == 1)
                    {
                        PageList += "<li class=\"arrow prev disabled\"><a class=\"d-block\" title=\"上一頁\" href=\"" + HttpUtility.HtmlEncode(URL) + "&page=1" + "\" tabindex=\"0\" aria-disabled=\"true\"><img class=\"svg\" src=\"/styles/images/common/arrow-left.svg\" alt=\"上一頁\"><span class=\"sr-only\">上一頁</span></a></li>";
                    }
                    else
                    {
                        PageList += "<li class=\"arrow prev disabled\"><a class=\"d-block\" title=\"第" + (page - 1).ToString() + "頁\" href=\"" + HttpUtility.HtmlEncode(URL) + "&page=" + (page - 1).ToString() + "\" tabindex=\"0\" aria-disabled=\"true\"><img class=\"svg\" src=\"/styles/images/common/arrow-left.svg\" alt=\"上一頁\"><span class=\"sr-only\">上一頁</span></a></li>";
                    }
                }



                   

                //頁碼
                var min = page - 2;
                var max = page + 2;

                if (max >= Allpage)
                {
                    max = Allpage;

                    if (min <= 0)
                    {
                        min = 1;
                    }
                }
                else
                {
                    if (min < 0)
                    {
                        min = 1;
                        if ((max += 2) > Allpage)
                        {
                            max = Allpage;
                        }
                        else
                        {
                            max += 2;
                            if (max > Allpage)
                            {
                                max = Allpage;
                            }
                        }
                    }
                    else if (min == 0)
                    {
                        min = 1;
                        max += 1;
                    }
                }

                var num = 1;
                for (int p = min; p <= max && num <= 5; p++)
                {
                    if (p == page)
                    {
                        PageList += "<li class=\"active\" aria-current=\"page\"><a class=\"d-block rounded-circle\" title=\"第" + p.ToString() + "頁\" href=\"javascript:void(0)\">" + p.ToString() + "<span class=\"sr-only\">(current)</span></a></li>";
                    }
                    else
                    {
                        PageList += "<li><a class=\"d-block rounded-circle\" title=\"第" + p.ToString() + "頁\" href=\"" + HttpUtility.HtmlEncode(URL) + "&page=" + p.ToString() + "\">" + p.ToString() + "</a></li>";
                    }
                    num++;
                }


                if (context.Request.ServerVariables["Server_Name"].IndexOf("iis.24241872.tw") != -1)
                {
                    //下一頁
                    if (page == Allpage)
                    {
                        PageList += "<li class=\"arrow next\"><a class=\"d-block\" title=\"下一頁\" href=\"" + HttpUtility.HtmlEncode(URL) + "&page=" + Allpage.ToString() + "\" tabindex=\"0\" aria-disabled=\"true\"><img class=\"svg\" src=\"http://iis.24241872.tw/projects/public/m000272/test/styles/images/common/arrow-right.svg\" alt=\"\"><span class=\"sr-only\">下一頁</span></a></li>";
                    }
                    else
                    {
                        PageList += "<li class=\"arrow next\"><a class=\"d-block\" title=\"下一頁\" href=\"" + HttpUtility.HtmlEncode(URL) + "&page=" + (page + 1).ToString() + "\" tabindex=\"0\" aria-disabled=\"true\"><img class=\"svg\" src=\"http://iis.24241872.tw/projects/public/m000272/test/styles/images/common/arrow-right.svg\" alt=\"\"><span class=\"sr-only\">下一頁</span></a></li>";
                    }

                    //最後一頁
                    PageList += "<li class=\"arrow next-next\"><a class=\"d-block\" title=\"最後一頁\" href=\"" + HttpUtility.HtmlEncode(URL) + "&page=" + Allpage.ToString() + "\" tabindex=\"0\" aria-disabled=\"true\"><img class=\"svg\" src=\"http://iis.24241872.tw/projects/public/m000272/test/styles/images/common/arrow-right-right.svg\" alt=\"\"><span class=\"sr-only\">最後一頁</span></a></li>";
                }
                else
                {
                    //下一頁
                    if (page == Allpage)
                    {
                        PageList += "<li class=\"arrow next\"><a class=\"d-block\" title=\"下一頁\" href=\"" + HttpUtility.HtmlEncode(URL) + "&page=" + Allpage.ToString() + "\" tabindex=\"0\" aria-disabled=\"true\"><img class=\"svg\" src=\"/styles/images/common/arrow-right.svg\" alt=\"\"><span class=\"sr-only\">下一頁</span></a></li>";
                    }
                    else
                    {
                        PageList += "<li class=\"arrow next\"><a class=\"d-block\" title=\"下一頁\" href=\"" + HttpUtility.HtmlEncode(URL) + "&page=" + (page + 1).ToString() + "\" tabindex=\"0\" aria-disabled=\"true\"><img class=\"svg\" src=\"/styles/images/common/arrow-right.svg\" alt=\"\"><span class=\"sr-only\">下一頁</span></a></li>";
                    }

                    //最後一頁
                    PageList += "<li class=\"arrow next-next\"><a class=\"d-block\" title=\"最後一頁\" href=\"" + HttpUtility.HtmlEncode(URL) + "&page=" + Allpage.ToString() + "\" tabindex=\"0\" aria-disabled=\"true\"><img class=\"svg\" src=\"/styles/images/common/arrow-right-right.svg\" alt=\"\"><span class=\"sr-only\">最後一頁</span></a></li>";
                }

                ////下一頁
                //if (page == Allpage)
                //{
                //    PageList += "<li class=\"arrow next\"><a class=\"d-block\" href=\"" + URL + "&page=" + Allpage.ToString() + "\" tabindex=\"-1\" aria-disabled=\"true\"><img class=\"svg\" src=\"/styles/images/common/arrow-right.svg\" alt=\"\"></a></li>";
                //}
                //else
                //{
                //    PageList += "<li class=\"arrow next\"><a class=\"d-block\" href=\"" + URL + "&page=" + (page + 1).ToString() + "\" tabindex=\"-1\" aria-disabled=\"true\"><img class=\"svg\" src=\"/styles/images/common/arrow-right.svg\" alt=\"\"></a></li>";
                //}

                ////最後一頁
                //PageList += "<li class=\"arrow next-next\"><a class=\"d-block\" href=\"" + URL + "&page=" + Allpage.ToString() + "\" tabindex=\"-1\" aria-disabled=\"true\"><img class=\"svg\" src=\"/styles/images/common/arrow-right-right.svg\" alt=\"\"></a></li>";

                return PageList;
            }

            return "";
        }

        /// <summary>
        /// 發送信件
        /// </summary>
        /// <param name="MailList"></param>
        /// <param name="content"></param>
        /// <param name="defLang"></param>
        public static void sendMail(List<string> MailList, string contentID, string defLang, string webURL, NameValueCollection form)
        {
            Model DB = new Model();

            web_data web_Data = DB.web_data.Where(model => model.lang == defLang).FirstOrDefault();//取得網站基本資訊
            Guid sysGuid = Guid.Parse("4795DABF-18DE-490E-9BB2-D57B4D99C127");

            system_data system_data = DB.system_data.Where(model => model.guid == sysGuid).FirstOrDefault();//取得網站基本資訊
            mail_contents mail_contents = DB.mail_contents.Where(m => m.guid == contentID).FirstOrDefault();

            string title = mail_contents.title;
            string content = mail_contents.content;

            content = content.Replace("{[title]}", title);
            content = content.Replace("{[websiteName]}", web_Data.title);
            content = content.Replace("{[sysName]}", "管理者");
            content = content.Replace("{[websiteUrl]}", webURL);
            //content = content.Replace("{[logo]}", webURL + "/" + system_data.logo);
            content = content.Replace("{[logo]}", "http://iis.24241872.tw/projects/public/m000272/test/styles/images/logo_red.png");
            content = content.Replace("{[date]}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));

            foreach (var key in form.AllKeys)
            {
                content = content.Replace("{[" + key + "]}", form[key].ToString());
            }


            var smtpData = DB.smtp_data.Where(m => m.guid == "b9732bfe-238d-4e4e-9114-8e6d30c34022").FirstOrDefault();



            string re = "";
            MailMessage msg = new MailMessage();
            //收件者，以逗號分隔不同收件者 ex "test@gmail.com,test2@gmail.com"
            msg.To.Add(string.Join(",", MailList.ToArray()));
            msg.From = new MailAddress(smtpData.from_email.ToString(), web_Data.title, System.Text.Encoding.UTF8);
            //郵件標題 
            msg.Subject = title.ToString();
            //郵件標題編碼  
            msg.SubjectEncoding = System.Text.Encoding.UTF8;
            //郵件內容
            msg.Body = content;
            msg.IsBodyHtml = true;
            msg.BodyEncoding = System.Text.Encoding.UTF8;//郵件內容編碼 
            msg.Priority = MailPriority.Normal;//郵件優先級 
                                               //建立 SmtpClient 物件 並設定 Gmail的smtp主機及Port 

            SmtpClient MySmtp = new SmtpClient(smtpData.host.ToString(), int.Parse(smtpData.port.ToString()));
            //設定你的帳號密碼
            MySmtp.Credentials = new System.Net.NetworkCredential(smtpData.username.ToString(), smtpData.thepr.ToString());
            //Gmial 的 smtp 使用 SSL
            if (smtpData.smtp_auth.ToString() == "Y")
            {
                MySmtp.EnableSsl = true;
            }
            else
            {
                MySmtp.EnableSsl = false;

            }
            try
            {
                MySmtp.Send(msg);
            }
            catch (SmtpFailedRecipientsException ex)
            {
                Console.WriteLine("Exception caught in RetryIfBusy(): {0}", ex.ToString());
            }



        }

        public static string FormatImagePath(string content)
        {
            var result = "";
            HttpContext context = System.Web.HttpContext.Current;

            if (!string.IsNullOrEmpty(content))
            {
                if (context.Request.ServerVariables["Server_Name"] == "localhost" || context.Request.ServerVariables["Server_Name"].IndexOf("iis.24241872.tw") != -1 || context.Request.ServerVariables["Server_Name"].IndexOf("south-china.com.tw") != -1)
                {
                    result = content.Replace("../", "")
                                    .Replace("Content/", "" + getWebUrl() + "/Content/")
                                    .Replace("content/", "" + getWebUrl() + "/content/")
                                    .Replace("Styles/", "" + getWebUrl() + "/Styles/")
                                    .Replace("styles/", "" + getWebUrl() + "/styles/");
                    //.Replace("Upload/", "" + getWebUrl() + "/Upload/")
                    //.Replace("upload/", "" + getWebUrl() + "/upload/");
                }
            }

            //return HttpUtility.HtmlEncode(result);
            return result;
        }

        public static string FormatNewsImagePath(string content)
        {
            var result = "";
            HttpContext context = System.Web.HttpContext.Current;

            if (!string.IsNullOrEmpty(content))
            {
                if (context.Request.ServerVariables["Server_Name"] == "localhost" || context.Request.ServerVariables["Server_Name"].IndexOf("iis.24241872.tw") != -1 || context.Request.ServerVariables["Server_Name"].IndexOf("south-china.com.tw") != -1)
                {
                    result = content.Replace("../", "")
                                    //.Replace("Content/", "" + getWebUrl() + "/Content/")
                                    //.Replace("content/", "" + getWebUrl() + "/content/")
                                    .Replace("Styles/", "" + getWebUrl() + "/Styles/")
                                    .Replace("styles/", "" + getWebUrl() + "/styles/");
                    //.Replace("Upload/", "" + getWebUrl() + "/Upload/")
                    //.Replace("upload/", "" + getWebUrl() + "/upload/");
                }
            }

            return result;
        }

        public static string FormatResueImagePath(string content)
        {
            var result = "";
            HttpContext context = System.Web.HttpContext.Current;

            if (!string.IsNullOrEmpty(content))
            {
                if (context.Request.ServerVariables["Server_Name"] == "localhost" || context.Request.ServerVariables["Server_Name"].IndexOf("iis.24241872.tw") != -1 || context.Request.ServerVariables["Server_Name"].IndexOf("south-china.com.tw") != -1)
                {
                    result = content.Replace("../", "")
                                    //.Replace("Content/", "" + getWebUrl() + "/Content/")
                                    //.Replace("content/", "" + getWebUrl() + "/content/")
                                    .Replace("/projects/public/m000272/test", "")
                                    .Replace("Styles/", "" + getWebUrl() + "/Styles/")
                                    .Replace("styles/", "" + getWebUrl() + "/styles/");
                    //.Replace("Upload/", "" + getWebUrl() + "/Upload/")
                    //.Replace("upload/", "" + getWebUrl() + "/upload/");
                }
            }

            return HttpUtility.HtmlEncode(result);
        }

        public static string reSysLogTitle(dynamic FromData, string typeTitle)
        {
            try
            {
                return HttpUtility.HtmlEncode(typeTitle) + "：" + HttpUtility.HtmlEncode(FromData.title.ToString());
            }
            catch
            {
                try
                {
                    return HttpUtility.HtmlEncode(typeTitle) + "：" + HttpUtility.HtmlEncode(FromData.name.ToString());
                }
                catch
                {
                    return HttpUtility.HtmlEncode(typeTitle) + "：" + HttpUtility.HtmlEncode(FromData.username.ToString());
                }
            }
        }

        public static string mSystemLogTitle(dynamic FromData, string typeTitle,string systemmenu)
        {
            try
            {
                return HttpUtility.HtmlEncode(typeTitle) + "：" + HttpUtility.HtmlEncode(systemmenu) + "_" + HttpUtility.HtmlEncode(FromData.title.ToString());
            }
            catch
            {
                try
                {
                    return HttpUtility.HtmlEncode(typeTitle) + "：" + HttpUtility.HtmlEncode(systemmenu) + "_" + HttpUtility.HtmlEncode(FromData.name.ToString());
                }
                catch
                {
                    return HttpUtility.HtmlEncode(typeTitle) + "：" + HttpUtility.HtmlEncode(systemmenu) + "_" + HttpUtility.HtmlEncode(FromData.username.ToString());
                }
            }
        }


        /// <summary>
        /// 發問問題列表參數格式化
        /// </summary>
        public class forumData
        {
            public string guid { get; set; } // or whatever
            public string title { get; set; }  // or whatever

            public string forum_categoryguid { get; set; }  // or whatever
            public string forum_subcategoryguid { get; set; }  // or whatever
            public string ad_id { get; set; }  // or whatever

            public string status { get; set; }  // or whatever

            public DateTime create_date { get; set; }  // or whatever

            public string permission { get; set; }  // or whatever

            public string look { get; set; }  // or whatever

            public string thumbsup { get; set; }  // or whatever

            public string ad_name { get; set; }  // or whatever
            public string ad_mail { get; set; }  // or whatever
            public string comment { get; set; }  // or whatever

            public string ad_imgurl { get; set; }  // or whatever
            public string ad_department { get; set; }  // or whatever

            public string ad_ext_num { get; set; }  // or whatever
            public string top_guid { get; set; }  // or whatever
        }


        /// <summary>
        /// 取得真實IP
        /// </summary>
        /// <returns></returns>
        public static string GetIP()
        {
            string ip;
            string trueIP = string.Empty;

            System.Web.HttpContext httpcontext = System.Web.HttpContext.Current;
            string ipAddress = httpcontext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            //先取得是否有經過代理伺服器
            ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ip))
            {
                //將取得的 IP 字串存入陣列
                string[] ipRange = ip.Split(',');

                //比對陣列中的每個 IP
                for (int i = 0; i < ipRange.Length; i++)
                {
                    //剔除內部 IP 及不合法的 IP 後，取出第一個合法 IP
                    if (ipRange[i].Trim().Substring(0, 3) != "10." &&
                        ipRange[i].Trim().Substring(0, 7) != "192.168" &&
                        ipRange[i].Trim().Substring(0, 7) != "172.16." &&
                        CheckIP(ipRange[i].Trim()))
                    {
                        trueIP = ipRange[i].Trim();
                        break;
                    }
                }

            }
            else
            {
                //trueIP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                
                //沒經過代理伺服器，直接使用 ServerVariables["REMOTE_ADDR"]
                //並經過 CheckIP( ) 的驗證

                trueIP = CheckIP(System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]) ?
                     System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] : "";
            }

            return trueIP;
        }

        public static bool CheckIP(string strPattern)
        {
            // 繼承自：System.Text.RegularExpressions
            // regular: ^\d{1,3}[\.]\d{1,3}[\.]\d{1,3}[\.]\d{1,3}$
            Regex regex = new Regex("^\\d{1,3}[\\.]\\d{1,3}[\\.]\\d{1,3}[\\.]\\d{1,3}$");
            Match m = regex.Match(strPattern);

            return m.Success;
        }

        /// <summary>
        /// 發送審核信件
        /// </summary>
        /// <param name="data"></param>
        public static void SendVerifyEmail(Dictionary<string, string> data, string username)
        {
            // string tables, string actType , string guid , string subject
            Model DB = new Model();
            //取得是否為審核資料表
            List<string> verifyUseTab = Web.Controllers.SiteadminController.verifyTables();//取得有審核的資料表
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            //審核通知信件
            if (verifyUseTab.IndexOf(data["tables"].ToString()) != -1)
            {
                web_data webData = DB.web_data.Where(m => m.lang == "tw").FirstOrDefault();//取得網站基本資訊
                List<string> MailList = new List<string>();
                NameValueCollection form = new NameValueCollection();

                //有審核權限下的使用者mail
                List<roles> roles = DB.roles.Where(m => m.verify == "Y").ToList();
                string sendstatus = "N";
                foreach (roles item in roles)
                {
                    List<user> user = DB.user.Where(m => m.role_guid == item.guid).ToList();
                    if(!user.Where(x=> x.username == username).Any())
                    {
                        sendstatus = "Y";
                        foreach (user subItem in user)
                        {
                            if (!string.IsNullOrEmpty(subItem.email) && MailList.IndexOf(subItem.email) == -1)
                            {
                                MailList.Add(subItem.email);
                            }
                        }
                    }
                }

                if(sendstatus == "Y")
                {
                    //通知審核者
                    if (data["actType"].ToString() == "add")
                    {
                        form.Add("sysName", "審核人員");
                        form.Add("info", "您有一則【" + data["subject"].ToString() + "】待審核，請登入後台後進行審核處理，謝謝!");
                        sendMail(MailList, "1", "tw", getWebUrl(), form);
                    }
                    else
                    {
                        form.Add("sysName", "審核人員");
                        form.Add("info", "您有一則【" + data["subject"].ToString() + "】待審核，請登入後台後進行審核處理，謝謝!");
                        sendMail(MailList, "1", "tw", getWebUrl(), form);
                    }
                }
            }
        }
    }

}