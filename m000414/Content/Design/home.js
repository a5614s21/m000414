﻿$(document).ready(function () {


    //$("body").delegate(".changeCity", "change", function () {
    $("body").on("change", ".changeCity", function () {

        var valsTemp = new Array();

        var arr = new Object();
        arr.name = "city";
        arr.value = $(this).val();
        valsTemp.push(arr);

        var arr = new Object();
        arr.name = "category";
        arr.value = $(this).attr('data-category');
        valsTemp.push(arr);

        var arr = new Object();
        arr.name = "type";
        arr.value = $(this).attr('data-type');
        valsTemp.push(arr);

        var vals = JSON.stringify(valsTemp);
        useAjax('changeCity', vals);
        return false;

    });

    //$("body").delegate(".openUrl", "click", function () {
    $("body").on("click", ".openUrl", function () {
        
        window.open($(this).attr('data-url'), '_blank');

    });


    //$("body").delegate(".removeService", "click", function () {
    $("body").on("click", ".removeService", function () {

        var valsTemp = new Array();

        var arr = new Object();
        arr.name = "services_guid";
        arr.value = $(this).attr('data-guid');
        valsTemp.push(arr);

        var arr = new Object();
        arr.name = "ad_id";
        arr.value = $(this).attr('data-adid');
        valsTemp.push(arr);

        var arr = new Object();
        arr.name = "type";
        arr.value = 'del';
        valsTemp.push(arr);

        var vals = JSON.stringify(valsTemp);

        useAjax('myFavorite', vals);



    });

    //$("body").delegate(".myFavorite", "click", function () {
    $("body").on("click",".myFavorite",  function () {
 


        var valsTemp = new Array();

        var arr = new Object();
        arr.name = "services_guid";
        arr.value = $(this).attr('data-guid');
        valsTemp.push(arr);

        var arr = new Object();
        arr.name = "ad_id";
        arr.value = $(this).attr('data-adid');
        valsTemp.push(arr);

        var arr = new Object();
        arr.name = "type";
        arr.value = $(this).attr('data-type');
        valsTemp.push(arr);


        if ($(this).attr('data-type') == "add") {
            $(this).attr('data-type', 'del')
        }
        else {
            $(this).attr('data-type', 'add');
        }

        var vals = JSON.stringify(valsTemp);
        useAjax('myFavorite', vals);


  
    });


    $("#contactForm").each(function () {


        $("#contactForm").validate({


            submitHandler: function (form) {


                $('.submitBut').attr("disabled", true);

                return true;
            },
            errorPlacement: function (error, element) {
                element.attr('style', 'border:#FF0000 1px solid;');

                element.next("._formErrorMsg").html('<div style="color: #FF0000; padding-bottom: 10px; padding-left: 10px;">' + error.text() + '</div>');


            },
            success: function (error) {

                var findID = $('#' + error[0].id.replace('-error', ''));

                //console.log(error[0].id);

                $(findID).attr('style', '');
                findID.next(".formNotice").html('');
                return false;

            }
        });

    });
});




/**
 * AJAX動作
 * @param ACT
 * @param needVal
 */
function useAjax(ACT, needVal) {



    $.ajax({
        type: 'POST',
        url: $('#ajaxRoot').val(),
        data: {
            Func: ACT,
            Val: encodeURI(needVal)
        },
        //cache: false, 
        dataType: 'json',
        beforeSend: function () {



        },
        success: function (json) {


            //console.log(json);

            //setLoadPlayer("none");


            switch (json.Func) {
                case "changeCity":

                    //var district = $.parseJSON(json.district);
                    var district = JSON.parse(json.district);
                    console.log(district);
                    $select = $("#district");
                    $select.html("");
                    $select.append('<option value="" selected>行政區</option>');
                    for (var i = 0; i < district.length; i++) {

                       /* var selected = "";
                        if (i == 0) {
                            selected = " selected";
                        }*/
                        $select.append('<option value="' + district[i] + '" >' + district[i] + '</option>');
                    }                
                       
                
                    return false;

                    break;




                case "myFavorite":

                    //myFavoriteList
                   
                    if (json.type == "add") {
                        if (json.reText != "") {
                            $('#myFavoriteList').append(json.reText);

                            $(".page-index ul>li .heart").on("click", function () {
                                $(this).toggleClass("red");
                              
                            });

                        }

                    }
                    else {
                        $('#service_' + json.services_guid).remove();

                        $('.myFavorite').each(function () {

                            if ($(this).attr('data-guid') == json.services_guid) {

                                $(this).removeClass('red');
                            }


                        });


                    }

                    if ($('#myFavoriteList').find('.addList').html() != null) {

                        $('#noAddFavorite').addClass('hidden');
                    }
                    else {
                        $('#noAddFavorite').removeClass('hidden');
                    }


                    
                    break;





            }


        },
        complete: function () { //生成分頁條

        },
        error: function () {
            //alert("讀取錯誤!");
        }
    });

}


function setLoadPlayer(view) {
    if (view == 'none') {
        $.unblockUI();
    }
    else {
        $.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
    }

}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}


var keyStr = "ABCDEFGHIJKLMNOP" +
    "QRSTUVWXYZabcdef" +
    "ghijklmnopqrstuv" +
    "wxyz0123456789+/" +
    "=";