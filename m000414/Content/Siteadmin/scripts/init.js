"use strict";
//console.log(1);
var $wrapper = $(".wrapper");
var $pageHeader = $(".page-header");
var $pageContent = $(".page-content");
var $contentBody = $(".content-body");
var $contentHeader = $(".content-header");
var $contentH = $(".contentH");
var $pageFooter = $(".page-footer");
var $panel = $(".panel");
var width = $(window).width();


/*--------------------------------------------
       		卷軸
---------------------------------------------*/
$('.nicescroll-bar').each(function() {
    $(this).slimscroll({
        height: '100%',
        color: '#878787',
        disableFadeOut: true,
        borderRadius: 0,
        size: '4px',
        alwaysVisible: false
    });
});

/* 行動版本預設選單關閉 */
if (width < 1620) {
  //  $wrapper.addClass('slide-nav-close');
}

/*****Ready function start*****/
$(document).ready(function() {

    /*--------------------------------------------
                專案專用
    ---------------------------------------------*/
    
  /*  $('pre code').each(function(i, block) {
        hljs.highlightBlock(block);
     });

    minmax();*/
});

var minmax = function() {

    /*下拉滑動特效*/
    /* var $dropdown = $('.dropdown');
    $dropdown.on('show.bs.dropdown', function(e) {
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
    });
    $dropdown.on('hide.bs.dropdown', function(e) {
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(150);
    });
    */

    /*--------------------------------------------
       		內容最小高度
     ---------------------------------------------*/
    var contentBodyH = function() {
        var h = $(window).outerHeight();
        var contentH = $contentHeader.outerHeight();
        var pageF = $pageFooter.outerHeight();
        var pageH = $pageHeader.height();
        var contentHeight = h - contentH - pageH - 30;
        var panelLength = $panel.length;

        if (panelLength == 1) {
            if (width < 576) {
                $panel.css({
                    minHeight: contentHeight + pageH,
                });
            } else {
                $panel.css({
                    minHeight: contentHeight,
                });
            }
        }
    }
    contentBodyH();

    /*--------------------------------------------
       		左側主選單開閉
     ---------------------------------------------*/
    $(document).on('click', '#toggle_nav_btn', function(e) {
        $wrapper.toggleClass('slide-nav-close');
        return false;
    });

    /*--------------------------------------------
       		選單螢幕自動關閉開啟
     ---------------------------------------------*/
    var navCloseAuto = function() {
        var width = $(window).width();
        if (width < 1620) {
            if ($(".slide-nav-close").length > 0) {
                return;
            } else {
                setTimeout(function() {
                    $wrapper.addClass('slide-nav-close');
                }, 200)
            }
        } else {
            if ($(".slide-nav-close").length > 0) {
                setTimeout(function() {
                    $wrapper.removeClass('slide-nav-close');
                }, 200)
            } else {
                return;
            }
        }
    }

    $(window).resize(function() {
        contentBodyH();
        navCloseAuto();
    });

    /*--------------------------------------------
       		側選單自動打開
     ---------------------------------------------*/
    $(document).on('click', '.fixed-sidebar-left a[data-toggle="collapse"]', function(e) {
        if ($(".slide-nav-close").length > 0) {
            $wrapper.removeClass('slide-nav-close');
        } else {
            return;
        }
    });
    /*--------------------------------------------
       		imgLiquid.js  圖片縮圖
     ---------------------------------------------*/
    $(".imgFill").each(function() {
        $(this).imgLiquid();
    });
    /*--------------------------------------------
       		拖曳排序
     ---------------------------------------------*/
     $(".file-img-list").each(function() {
        $(this).sortable();
        $(this).disableSelection();
    });
    /*--------------------------------------------
       		圖片輪播
     ---------------------------------------------*/
    
    $(".swiper-fade").each(function() {
        var swiper = new Swiper('.swiper-fade', {
            autoplay: {
                delay: 3000,
              },
              effect: 'fade',
              speed:1000
          });
    });
    /*--------------------------------------------
       		代碼顏色
    ---------------------------------------------*/
    hljs.initHighlightingOnLoad();
    /*--------------------------------------------
       		bootstrap-Tooltips 提示
    ---------------------------------------------*/
    $('[data-toggle="tooltip"]').each(function () {
        $(this).tooltip();
    });
    
    /*--------------------------------------------
            bootstrap-popover 提示
    ---------------------------------------------*/
    $('[data-toggle="popover"]').each(function () {
        $(this).popover();
    });

    /*--------------------------------------------
       		page [holiday]日曆
     ---------------------------------------------*/
     
     $("#calendar").each(function() {
        var currentYear = new Date().getFullYear();
        var currentMonth = new Date().getMonth();
        var currentDate = new Date().getDate();
        var today = new Date(currentYear, currentMonth, currentDate).getTime();
        function editEvent(event) {
            
            $('#event-modal input[name="event-index"]').val(event ? event.id : '');
            $('#event-modal input[name="event-name"]').val(event ? event.name : '');
            
            $('#event-modal input[name="event-start-date"]').datepicker('update', event ? event.startDate : '');
            $('#event-modal input[name="event-end-date"]').datepicker('update', event ? event.endDate : '');
            
            $('#event-modal').modal();
        }
        
        function deleteEvent(event) {
            var dataSource = $('#calendar').data('calendar').getDataSource();
        
            for(var i in dataSource) {
                if(dataSource[i].id == event.id) {
                    dataSource.splice(i, 1);
                    break;
                }
            }
            
            $('#calendar').data('calendar').setDataSource(dataSource);
        }
        
        function saveEvent() {
            var event = {
                id: $('#event-modal input[name="event-index"]').val(),
                name: $('#event-modal input[name="event-name"]').val(),
                startDate: $('#event-modal input[name="event-start-date"]').datepicker('getDate'),
                endDate: $('#event-modal input[name="event-end-date"]').datepicker('getDate')
            }
            
            var dataSource = $('#calendar').data('calendar').getDataSource();
        
            if(event.id) {
                for(var i in dataSource) {
                    if(dataSource[i].id == event.id) {
                        dataSource[i].name = event.name;
                        dataSource[i].startDate = event.startDate;
                        dataSource[i].endDate = event.endDate;
                    }
                }
            }
            else
            {
                var newId = 0;
                for(var i in dataSource) {
                    if(dataSource[i].id > newId) {
                        newId = dataSource[i].id;
                    }
                }
                
                newId++;
                event.id = newId;
            
                dataSource.push(event);
            }
            
            $('#calendar').data('calendar').setDataSource(dataSource);
            $('#event-modal').modal('hide');
        }
        
        $(function() {
            var currentYear = new Date().getFullYear();
        
            $('#calendar').calendar({ 
                enableContextMenu: true,
                enableRangeSelection: true,
                customDayRenderer: function(element, date) {
                    if(date.getTime() == today) {
                        $(element).css('color', 'white');
                        $(element).css('font-weight', 'bold');
                        $(element).css('background-color', 'red');
                    }
                },
                language: 'tw',
                contextMenuItems:[
                    {
                        text: 'Update',
                        click: editEvent
                    },
                    {
                        text: 'Delete',
                        click: deleteEvent
                    }
                ],
                selectRange: function(e) {
                    
                    editEvent({ startDate: e.startDate, endDate: e.endDate });
                    
                },
                mouseOnDay: function(e) {
                    if(e.events.length > 0) {
                        var content = '';
                        
                        for(var i in e.events) {
                            content += '<div class="event-tooltip-content">'
                                            + '<div class="event-name" style="color:red">' + e.events[i].name + '</div>'
                                        + '</div>';
                        }
                    
                        $(e.element).popover({ 
                            trigger: 'manual',
                            container: 'body',
                            html:true,
                            content: content
                        });
                        
                        $(e.element).popover('show');
                    }
                    
                },
                mouseOutDay: function(e) {
                    if(e.events.length > 0) {
                        $(e.element).popover('hide');
                    }
                },
                dayContextMenu: function(e) {
                    $(e.element).popover('hide');
                },
                dataSource: [
                    {
                        id: 0,
                        name: 'Google I/O',
                        startDate: new Date(currentYear, 4, 28),
                        endDate: new Date(currentYear, 4, 29)
                    },
                    {
                        id: 1,
                        name: 'Microsoft Convergence',
                        startDate: new Date(currentYear, 2, 16),
                        endDate: new Date(currentYear, 2, 19)
                    },
                    {
                        id: 2,
                        name: 'Microsoft Build Developer Conference',
                        startDate: new Date(currentYear, 3, 29),
                        endDate: new Date(currentYear, 4, 1)
                    },
                    {
                        id: 3,
                        name: 'Apple Special Event',
                        startDate: new Date(currentYear, 8, 1),
                        endDate: new Date(currentYear, 8, 1)
                    },
                    {
                        id: 4,
                        name: 'Apple Keynote',
                        startDate: new Date(currentYear, 8, 9),
                        endDate: new Date(currentYear, 8, 9)
                    },
                    {
                        id: 5,
                        name: 'Chrome Developer Summit',
                        startDate: new Date(currentYear, 10, 17),
                        endDate: new Date(currentYear, 10, 18)
                    },
                    {
                        id: 6,
                        name: 'F8 2015',
                        startDate: new Date(currentYear, 2, 25),
                        endDate: new Date(currentYear, 2, 26)
                    },
                    {
                        id: 7,
                        name: 'Yahoo Mobile Developer Conference',
                        startDate: new Date(currentYear, 7, 25),
                        endDate: new Date(currentYear, 7, 26)
                    },
                    {
                        id: 8,
                        name: 'Android Developer Conference',
                        startDate: new Date(currentYear, 11, 1),
                        endDate: new Date(currentYear, 11, 4)
                    },
                    {
                        id: 9,
                        name: 'LA Tech Summit',
                        startDate: new Date(currentYear, 10, 17),
                        endDate: new Date(currentYear, 10, 17)
                    }
                ]
            });
            
            $('#save-event').click(function() {
                
                saveEvent();
                
            });
        });
    });
   
};