﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMemberShare : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.membershares",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(maxLength: 64),
                        inoutline = c.String(),
                        content = c.String(),
                        startdate = c.DateTime(),
                        enddate = c.DateTime(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        sticky = c.String(maxLength: 1),
                        status = c.String(maxLength: 1),
                        pic = c.String(),
                        pic_alt = c.String(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.membershares");
        }
    }
}
