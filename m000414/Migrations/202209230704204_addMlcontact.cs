﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMlcontact : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MLCONTACTUS",
                c => new
                    {
                        SNO = c.Int(nullable: false, identity: true),
                        NAME = c.String(),
                        EMAIL = c.String(),
                        MOBILE = c.String(),
                        QUESTION = c.String(),
                        TOWN = c.String(),
                        CITY = c.String(),
                        REPLAYTP = c.Int(nullable: false),
                        CONTENT = c.String(),
                        UPDATE = c.DateTime(),
                        CDATE = c.DateTime(),
                        OVERYEAR = c.Int(nullable: false),
                        SOURCE = c.Int(nullable: false),
                        STATUS = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SNO);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MLCONTACTUS");
        }
    }
}
