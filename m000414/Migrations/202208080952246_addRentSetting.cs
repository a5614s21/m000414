﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addRentSetting : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.rentsettings",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        model = c.String(),
                        price = c.String(),
                        pricediscount = c.String(),
                        rentyear = c.String(),
                        rentmonth = c.String(),
                        margin = c.String(),
                        rebuyprice = c.String(),
                        start_date = c.DateTime(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.rentsettings");
        }
    }
}
