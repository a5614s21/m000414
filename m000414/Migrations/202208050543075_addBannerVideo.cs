﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addBannerVideo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.homebanners",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        pic = c.String(),
                        pic_alt = c.String(),
                        url = c.String(),
                        startdate = c.DateTime(),
                        enddate = c.DateTime(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                        sortIndex = c.Int(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.videocontents",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        url = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.videocontents");
            DropTable("dbo.homebanners");
        }
    }
}
