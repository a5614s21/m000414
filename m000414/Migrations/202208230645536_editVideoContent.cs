﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editVideoContent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.videocontents", "titlecontent", c => c.String());
            AddColumn("dbo.videocontents", "pic", c => c.String());
            AddColumn("dbo.videocontents", "pic_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.videocontents", "pic_alt");
            DropColumn("dbo.videocontents", "pic");
            DropColumn("dbo.videocontents", "titlecontent");
        }
    }
}
