﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editRoundServiceFree : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.projectsettings", "roundservicefree", c => c.String());
            DropColumn("dbo.projectsettings", "roundservice2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.projectsettings", "roundservice2", c => c.String());
            DropColumn("dbo.projectsettings", "roundservicefree");
        }
    }
}
