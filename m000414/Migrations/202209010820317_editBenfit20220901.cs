﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editBenfit20220901 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.benfits", "contenttwo", c => c.String());
            AddColumn("dbo.benfits", "contentthree", c => c.String());
            AddColumn("dbo.benfits", "contentfour", c => c.String());
            AddColumn("dbo.benfits", "videolinkone", c => c.String());
            AddColumn("dbo.benfits", "videolinktwo", c => c.String());
            AddColumn("dbo.benfits", "videolinkthree", c => c.String());
            AddColumn("dbo.benfits", "videolinkfour", c => c.String());
            AddColumn("dbo.benfits", "pictwo", c => c.String());
            AddColumn("dbo.benfits", "pictwo_alt", c => c.String());
            AddColumn("dbo.benfits", "picthree", c => c.String());
            AddColumn("dbo.benfits", "picthree_alt", c => c.String());
            AddColumn("dbo.benfits", "picfour", c => c.String());
            AddColumn("dbo.benfits", "picfour_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.benfits", "picfour_alt");
            DropColumn("dbo.benfits", "picfour");
            DropColumn("dbo.benfits", "picthree_alt");
            DropColumn("dbo.benfits", "picthree");
            DropColumn("dbo.benfits", "pictwo_alt");
            DropColumn("dbo.benfits", "pictwo");
            DropColumn("dbo.benfits", "videolinkfour");
            DropColumn("dbo.benfits", "videolinkthree");
            DropColumn("dbo.benfits", "videolinktwo");
            DropColumn("dbo.benfits", "videolinkone");
            DropColumn("dbo.benfits", "contentfour");
            DropColumn("dbo.benfits", "contentthree");
            DropColumn("dbo.benfits", "contenttwo");
        }
    }
}
