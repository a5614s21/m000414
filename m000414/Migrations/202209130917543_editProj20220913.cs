﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editProj20220913 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.projectsettings", "carstatusname", c => c.String());
            AddColumn("dbo.projectsettings", "brandname", c => c.String());
            AddColumn("dbo.projectsettings", "statusname", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.projectsettings", "statusname");
            DropColumn("dbo.projectsettings", "brandname");
            DropColumn("dbo.projectsettings", "carstatusname");
        }
    }
}
