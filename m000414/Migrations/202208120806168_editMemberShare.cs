﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editMemberShare : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.membershares", "sortIndex", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.membershares", "sortIndex");
        }
    }
}
