﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editMoneyCompare : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.conparemoneys", newName: "comparemoneys");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.comparemoneys", newName: "conparemoneys");
        }
    }
}
