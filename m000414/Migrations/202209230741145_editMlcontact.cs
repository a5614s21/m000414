﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editMlcontact : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MLCONTACTUS", "PROJECT", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MLCONTACTUS", "PROJECT");
        }
    }
}
