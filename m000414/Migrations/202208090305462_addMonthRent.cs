﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMonthRent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.projectsettings", "monthrent", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.projectsettings", "monthrent");
        }
    }
}
