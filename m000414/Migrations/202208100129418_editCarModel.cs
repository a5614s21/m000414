﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editCarModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.carmodels", "sortIndex", c => c.Int());
            DropColumn("dbo.carmodels", "create_name");
            DropColumn("dbo.carmodels", "modify_name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.carmodels", "modify_name", c => c.String());
            AddColumn("dbo.carmodels", "create_name", c => c.String());
            DropColumn("dbo.carmodels", "sortIndex");
        }
    }
}
