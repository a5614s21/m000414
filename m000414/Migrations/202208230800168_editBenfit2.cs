﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editBenfit2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.benfits", "contentone", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.benfits", "contentone");
        }
    }
}
