﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editRentSetting20220912 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.rentsettings", "titlename", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.rentsettings", "titlename");
        }
    }
}
