﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editMlcontact20220926 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MLCONTACTUS", "EMLE", c => c.String());
            AddColumn("dbo.MLCONTACTUS", "MOBILEE", c => c.String());
            AddColumn("dbo.MLCONTACTUS", "UDATE", c => c.DateTime());
            DropColumn("dbo.MLCONTACTUS", "EMAIL");
            DropColumn("dbo.MLCONTACTUS", "MOBILE");
            DropColumn("dbo.MLCONTACTUS", "UPDATE");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MLCONTACTUS", "UPDATE", c => c.DateTime());
            AddColumn("dbo.MLCONTACTUS", "MOBILE", c => c.String());
            AddColumn("dbo.MLCONTACTUS", "EMAIL", c => c.String());
            DropColumn("dbo.MLCONTACTUS", "UDATE");
            DropColumn("dbo.MLCONTACTUS", "MOBILEE");
            DropColumn("dbo.MLCONTACTUS", "EMLE");
        }
    }
}
