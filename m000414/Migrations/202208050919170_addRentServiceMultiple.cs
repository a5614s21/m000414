﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addRentServiceMultiple : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.contact_service",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.mulitpleselects",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.rentbenfits",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.rentbenfits");
            DropTable("dbo.mulitpleselects");
            DropTable("dbo.contact_service");
        }
    }
}
