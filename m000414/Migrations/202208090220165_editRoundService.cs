﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editRoundService : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.projectsettings", "roundservice2", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.projectsettings", "roundservice2");
        }
    }
}
