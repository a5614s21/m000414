﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editMemberShare20220907 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.membershares", "articleone", c => c.String());
            AddColumn("dbo.membershares", "articletwo", c => c.String());
            AddColumn("dbo.membershares", "articlethree", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.membershares", "articlethree");
            DropColumn("dbo.membershares", "articletwo");
            DropColumn("dbo.membershares", "articleone");
        }
    }
}
