﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCarModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.carmodels",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        create_name = c.String(),
                        modify_name = c.String(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.carmodels");
        }
    }
}
