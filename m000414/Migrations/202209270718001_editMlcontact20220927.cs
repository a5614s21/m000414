﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editMlcontact20220927 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MLCONTACTUS", "REPLAYTP", c => c.Int());
            AlterColumn("dbo.MLCONTACTUS", "OVERYEAR", c => c.Int());
            AlterColumn("dbo.MLCONTACTUS", "SOURCE", c => c.Int());
            AlterColumn("dbo.MLCONTACTUS", "STATUS", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.MLCONTACTUS", "STATUS", c => c.Int(nullable: false));
            AlterColumn("dbo.MLCONTACTUS", "SOURCE", c => c.Int(nullable: false));
            AlterColumn("dbo.MLCONTACTUS", "OVERYEAR", c => c.Int(nullable: false));
            AlterColumn("dbo.MLCONTACTUS", "REPLAYTP", c => c.Int(nullable: false));
        }
    }
}
