﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editBenfit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.benfits", "titlecontent", c => c.String());
            AddColumn("dbo.benfits", "pic", c => c.String());
            AddColumn("dbo.benfits", "pic_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.benfits", "pic_alt");
            DropColumn("dbo.benfits", "pic");
            DropColumn("dbo.benfits", "titlecontent");
        }
    }
}
