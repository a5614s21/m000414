namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class add_infinity_to_news_table : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.news", "infinity", c => c.String());
            Sql("execute sp_addextendedproperty 'MS_Description', N'�L���s�W' ,'SCHEMA', N'dbo','TABLE', N'news', 'COLUMN', N'infinity'");
        }
        
        public override void Down()
        {
            DropColumn("dbo.news", "infinity");
        }
    }
}
