﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addProjectSetting : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.brand_category",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        code = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.car_status",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        code = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
            CreateTable(
                "dbo.projectsettings",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        indoor = c.String(),
                        sortNum = c.String(),
                        carstatus = c.String(),
                        brand = c.String(),
                        renttime = c.String(),
                        roundservice = c.String(),
                        mileage = c.String(),
                        outline = c.String(),
                        url = c.String(),
                        feature = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.projectsettings");
            DropTable("dbo.car_status");
            DropTable("dbo.brand_category");
        }
    }
}
