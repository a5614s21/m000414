﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAdvanceform : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.advanceforms",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        email = c.String(),
                        phone = c.String(),
                        contactmethod = c.String(),
                        country = c.String(),
                        city = c.String(),
                        project = c.String(),
                        requirenote = c.String(),
                        create_date = c.DateTime(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.advanceforms");
        }
    }
}
